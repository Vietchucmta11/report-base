package service;

import model.Authorities;

public interface AuthoritiesService {
	public int insertAnthorities(Authorities authorities);
	
	public Authorities getDetailAuthorities(String username);
	
	public int updateAuthorities(Authorities authorities);
	
}
