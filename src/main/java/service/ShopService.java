package service;

import java.util.List;

import model.Shop;

public interface ShopService {
	public Shop getDetailShopById(int shop_id);

	public List<Shop> getListShop();

	public int insertShop(Shop shop);
	
	public int updateShop(Shop shop) ;
}
