package service;

import java.util.List;

import model.Customer;

public interface CustomerService {
	public int insertCustomer(Customer customer);

	public Customer getCustomerById(int id);

	public int updateCustomer(int id, Customer customer);

	public List<Customer> getListCustomerByShop(int shop_id);

	public int updateAnalytic(Customer customer);

	public List<Customer> getListCustomeByShopDate(int Shop_id, String strDate);

	public List<Customer> getListCustomerByShopCameraDate(int shop_id, int camera_id, String strDate);

	public boolean updateListAnalytic(Customer customer);

	public Customer getCustomerByTracking_New(int shop_id, String tracking_new);

	public List<Customer> getListCustomerDateToDate(int shop_id, String startDate, String endDate);

	public List<Customer> getListCustomerDate(int shop_id, String strDate);

	public boolean deleteCustomerById(int id);

	public boolean undoCustomer(Customer customer);
}
