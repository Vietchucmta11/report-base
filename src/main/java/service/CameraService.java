package service;

import java.util.List;

import model.Camera;

public interface CameraService {
	public Camera getDetailCameraById(int camera_id);

	public List<Camera> getListCameraByShopId(int shop_id);

	public boolean updateStatusCamera(Camera camera);
	
	public int insertCamera(Camera camera);
}
