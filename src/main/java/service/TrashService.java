package service;

import java.util.List;

import model.Trash;

public interface TrashService {
	public List<Trash> getListTrashByShop(int shop_id);

	public boolean deleteTrash(int id);

	public boolean insertTrash(Trash trash);
	
	public Trash getTrashById(int id);
}
