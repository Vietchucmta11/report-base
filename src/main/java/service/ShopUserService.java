package service;

import model.ShopUser;

public interface ShopUserService {
	public ShopUser getShopUserByUsername(String username);

	// ShopUser getShopUserByShopId(int shop_id);

	public int insertShopUser(ShopUser shopUser);

	public int updateShopUser(ShopUser shopUser);
}
