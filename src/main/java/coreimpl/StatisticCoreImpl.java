package coreimpl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import core.StatisticCore;
import model.Customer;
import model.Line;
import model.Statistic;
import model.StatisticLine;

@Component
@Transactional
public class StatisticCoreImpl implements StatisticCore {

	final static Logger logger = Logger.getLogger(StatisticCoreImpl.class);

	@Override
	public List<Statistic> statisticByEmotion(List<Customer> customers) {
		List<Statistic> statistics = new ArrayList<Statistic>();
		String[] name = { "Angry", "Fearful", "Sad", "Surprise", "Happy", "Disgust", "Neutral" };
		for (int i = 0; i < name.length; i++) {
			Statistic statistic = new Statistic();
			statistic.setName(name[i]);
			int quatity = 0;
			String strname = name[i].toLowerCase();
			for (Customer customer : customers) {
				if (strname.equals(customer.getEmotion().toLowerCase()) == true) {
					quatity += 1;
				}
			}
			statistic.setQuatity(quatity);
			statistics.add(statistic);
		}
		logger.debug("Get data statistic by emotion: " + statistics.size() + "emotion");
		return statistics;
	}

	@Override
	public List<Statistic> statisticByAge(List<Customer> customers) {
		List<Statistic> statistics = new ArrayList<Statistic>();
		String[] name = { "0-10", "10-15", "15-20", "20-25", "25-30", "30-35", "35-40", "40-45", "45-50", "50-55",
				"55-60", ">60" };
		for (int i = 0; i < name.length; i++) {
			Statistic statistic = new Statistic();
			List<String> quatity = new ArrayList<String>();
			statistic.setName(name[i]);
			if (i == 0) {
				for (Customer c : customers) {
					if (c.getAge() < 10) {
						quatity.add(c.getTracking_new());
					}
				}
			} else if (i == (name.length - 1)) {
				for (Customer c : customers) {
					if (c.getAge() >= 60) {
						quatity.add(c.getTracking_new());
					}
				}
			} else {
				int a = Integer.parseInt(name[i].split("\\-")[0]);
				for (Customer c : customers) {
					if (c.getAge() == a) {
						quatity.add(c.getTracking_new());
					}
				}
			}
			statistic.setQuatity(quatity.size());
			statistics.add(statistic);
		}
		logger.debug("Get statistic by age:" + statistics.size() + "");
		return statistics;
	}

	@Override
	public List<Statistic> statisticByGender(List<Customer> customers) {
		List<Statistic> statistics = new ArrayList<Statistic>();
		String[] name = { "Male", "Female" };
		for (int i = 0; i < name.length; i++) {
			Statistic statistic = new Statistic();
			statistic.setName(name[i]);
			List<String> quatity = new ArrayList<String>();
			String strname = name[i].toLowerCase();
			for (Customer customer : customers) {
				if (strname.equals(customer.getGender().toLowerCase()) == true) {
					quatity.add(customer.getTracking_new());
				}
			}
			statistic.setQuatity(quatity.size());
			statistics.add(statistic);
		}
		logger.debug("Get data statistic by gender: " + statistics.size() + "gender");
		return statistics;
	}

	//// thong ke so luot nguoi trong cac ngay
	@Override
	public StatisticLine statisticLineDateToDate(List<Customer> customers, Date startDate, Date endDate) {
		StatisticLine statisticLine = new StatisticLine();
		List<Line> lines = new ArrayList<Line>();
		List<String> strDates = new ArrayList<String>();
		int _date = minusDate(startDate, endDate);
		Line line = new Line();
		line.setName("Số ảnh");
		List<Integer> quatity = new ArrayList<>();
		Date d = startDate;
		for (int j = 0; j < (_date + 1); j++) {
			int dem = 0;
			for (Customer c : customers) {
				if (compareDate(d, c.getDatetime_new()) == 1) {
					dem += 1;
				}
			}
			quatity.add(dem);
			d = plusDate(d);
		}
		line.setData(quatity);
		lines.add(line);
		
		for (int i = 0; i < (_date + 1); i++) {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			strDates.add(df.format(startDate));
			startDate = plusDate(startDate);
		}
		statisticLine.setDataLine(lines);
		statisticLine.setStrDate(strDates);
		logger.debug("get list statistic line date: " + startDate + " to " + endDate);
		return statisticLine;
	}

	// thon ke gioi tinh theo ngay
	@Override
	public StatisticLine statisticLineByGender(List<Customer> customers, Date startDate, Date endDate) {
		StatisticLine statisticLine = new StatisticLine();
		List<Line> lines = new ArrayList<Line>();
		List<String> strDates = new ArrayList<String>();
		String[] name = { "Male", "Female" };
		int _date = minusDate(startDate, endDate);
		for (int i = 0; i < name.length; i++) {
			Line line = new Line();
			line.setName(name[i]);
			List<Integer> quatity = new ArrayList<>();
			String strName = name[i].toLowerCase();
			Date d = startDate;
			for (int j = 0; j < (_date + 1); j++) {
				int dem = 0;
				for (Customer c : customers) {
					if (compareDate(d, c.getDatetime_new()) == 1
							&& strName.equals(c.getGender().toLowerCase()) == true) {
						dem += 1;
					}
				}
				quatity.add(dem);
				d = plusDate(d);
			}
			line.setData(quatity);
			lines.add(line);
		}

		for (int i = 0; i < (_date + 1); i++) {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			strDates.add(df.format(startDate));
			startDate = plusDate(startDate);
		}
		statisticLine.setDataLine(lines);
		statisticLine.setStrDate(strDates);
		logger.debug("get list statistic line gender: " + startDate + " to " + endDate);
		return statisticLine;
	}

	// thong ke cam xuc theo ngay
	@Override
	public StatisticLine statisticLineEmotion(List<Customer> customers, Date startDate, Date endDate) {
		StatisticLine statisticLine = new StatisticLine();
		String[] name = { "Angry", "Fearful", "Sad", "Surprise", "Happy", "Disgust", "Neutral" };
		List<Line> lines = new ArrayList<Line>();
		List<String> strDates = new ArrayList<String>();
		int _date = minusDate(startDate, endDate);
		for (int i = 0; i < name.length; i++) {
			Line line = new Line();
			line.setName(name[i]);
			String strName = name[i].toLowerCase();
			Date d = startDate;
			List<Integer> quatity = new ArrayList<>();
			for (int j = 0; j < (_date + 1); j++) {
				int dem = 0;
				for (Customer c : customers) {
					if (compareDate(d, c.getDatetime_new()) == 1
							&& strName.equals(c.getEmotion().toLowerCase()) == true) {
						dem += 1;
					}
				}
				d = plusDate(d);
				quatity.add(dem);
			}
			line.setData(quatity);
			lines.add(line);
		}
		for (int i = 0; i < (_date + 1); i++) {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			strDates.add(df.format(startDate));
			startDate = plusDate(startDate);
		}
		statisticLine.setStrDate(strDates);
		statisticLine.setDataLine(lines);
		logger.debug("get list statistic line emotion: " + startDate + " to " + endDate);
		return statisticLine;
	}

	// thống kê độ tuổi trong khoảng ngày
	@Override
	public StatisticLine statisticLineAge(List<Customer> customers, Date startDate, Date endDate) {
		StatisticLine statisticLine = new StatisticLine();
		String[] name = { "0-10", "10-15", "15-20", "20-25", "25-30", "30-35", "35-40", "40-45", "45-50", "50-55",
				"55-60", ">60" };
		List<Line> lines = new ArrayList<Line>();
		List<String> strDates = new ArrayList<String>();
		int _date = minusDate(startDate, endDate);
		for (int i = 0; i < name.length; i++) {
			Line line = new Line();
			List<Integer> quatity = new ArrayList<>();
			line.setName(name[i]);
			Date d = startDate;
			for (int j = 0; j < (_date + 1); j++) {
				int dem = 0;
				if (i == 0) {
					for (Customer c : customers) {
						if (compareDate(d, c.getDatetime_new()) == 1 && c.getAge() < 10) {
							dem += 1;
						}
					}
				} else if (i == (name.length - 1)) {
					for (Customer c : customers) {
						if (compareDate(d, c.getDatetime_new()) == 1 && c.getAge() >= 60) {
							dem += 1;
						}
					}
				} else {
					int a = Integer.parseInt(name[i].split("\\-")[0]);
					for (Customer c : customers) {
						if (compareDate(d, c.getDatetime_new()) == 1 && c.getAge() == a) {
							dem += 1;
						}
					}
				}
				quatity.add(dem);
				d = plusDate(d);
			}
			line.setData(quatity);
			lines.add(line);
		}
		for (int i = 0; i < (_date + 1); i++) {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			strDates.add(df.format(startDate));
			startDate = plusDate(startDate);
		}
		statisticLine.setStrDate(strDates);
		statisticLine.setDataLine(lines);
		logger.debug("get list statistic line age: " + startDate + " to " + endDate);
		return statisticLine;
	}

	// cong them 1 ngay
	public Date plusDate(Date date) {
		LocalDateTime localDateTime = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		localDateTime = localDateTime.plusDays(1);
		Date currentDatePlusOneDay = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
		return currentDatePlusOneDay;
	}

	// so sanh 1 ngay
	public int compareDate(Date date1, Date date2) {
		LocalDate ld1 = date1.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		LocalDate ld2 = date2.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		if (ld1.getYear() == ld2.getYear()) {
			if (ld1.getDayOfYear() == ld2.getDayOfYear()) {
				return 1;
			} else {
				return 0;
			}
		} else {
			return 0;
		}
	}

	// hieu giua 2 ngay
	public int minusDate(Date date1, Date date2) {
		long i = (date2.getTime() - date1.getTime()) / (3600 * 24 * 1000);
		return (int) i;
	}

	@Override
	public List<Statistic> statisticLinedate(List<Customer> customers) {
		List<Statistic> statistics = new ArrayList<Statistic>();
		int hour = 0;
		try {
			for (int i = 0; i < 24; i++) {
				Statistic statistic = new Statistic();
				int quatity = 0;
				for (Customer c : customers) {
					if (hour == getHour(c.getDatetime_new())) {
						quatity += 1;
					}
				}
				statistic.setName(new StringBuffer().append(hour).toString() + "-"
						+ new StringBuffer().append(hour + 1).toString());
				statistic.setQuatity(quatity);
				statistics.add(statistic);
				hour += 1;
			}
			return statistics;
		} catch (Exception e) {
			return null;
		}
	}

	public static int getHour(Date date) {
		LocalDateTime ld = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		return ld.getHour();
	}

	@Override
	public StatisticLine statisticLineGenderDate(List<Customer> customers) {
		StatisticLine statisticLine = new StatisticLine();
		List<Line> lines = new ArrayList<Line>();
		List<String> strDates = new ArrayList<String>();
		String[] name = { "Male", "Female" };
		for (int i = 0; i < name.length; i++) {
			Line line = new Line();
			line.setName(name[i]);
			List<Integer> quatity = new ArrayList<>();
			String strName = name[i].toLowerCase();
			int h = 0;
			for (int j = 0; j < 24; j++) {
				int dem = 0;
				for (Customer c : customers) {
					if (h == getHour(c.getDatetime_new()) && strName.equals(c.getGender().toLowerCase()) == true) {
						dem += 1;
					}
				}
				quatity.add(dem);
				h += 1;
			}
			line.setData(quatity);
			lines.add(line);
		}
		int hour = 0;
		for (int i = 0; i < 24; i++) {
			strDates.add(
					new StringBuffer().append(hour).toString() + "-" + new StringBuffer().append(hour + 1).toString());
			hour += 1;
		}
		statisticLine.setDataLine(lines);
		statisticLine.setStrDate(strDates);
		return statisticLine;
	}

	@Override
	public StatisticLine statisticLineAgeDate(List<Customer> customers) {
		StatisticLine statisticLine = new StatisticLine();
		List<Line> lines = new ArrayList<Line>();
		List<String> strDates = new ArrayList<String>();
		String[] name = { "0-10", "10-15", "15-20", "20-25", "25-30", "30-35", "35-40", "40-45", "45-50", "50-55",
				"55-60", ">60" };
		for (int i = 0; i < name.length; i++) {
			Line line = new Line();
			line.setName(name[i]);
			List<Integer> quatity = new ArrayList<>();
			int h = 0;
			for (int j = 0; j < 24; j++) {
				int dem = 0;
				if (i == 0) {
					for (Customer c : customers) {
						if (h == getHour(c.getDatetime_new()) && c.getAge() < 10) {
							dem += 1;
						}
					}
				} else if (i == (name.length - 1)) {
					for (Customer c : customers) {
						if (h == getHour(c.getDatetime_new()) && c.getAge() >= 60) {
							dem += 1;
						}
					}
				} else {
					int a = Integer.parseInt(name[i].split("\\-")[0]);
					for (Customer c : customers) {
						if (h == getHour(c.getDatetime_new()) && c.getAge() == a) {
							dem += 1;
						}
					}
				}
				quatity.add(dem);
				h += 1;
			}
			line.setData(quatity);
			lines.add(line);
		}
		int hour = 0;
		for (int i = 0; i < 24; i++) {
			strDates.add(
					new StringBuffer().append(hour).toString() + "-" + new StringBuffer().append(hour + 1).toString());
			hour += 1;
		}
		statisticLine.setDataLine(lines);
		statisticLine.setStrDate(strDates);
		return statisticLine;
	}

	@Override
	public StatisticLine statisticLineEmtionDate(List<Customer> customers) {
		StatisticLine statisticLine = new StatisticLine();
		List<Line> lines = new ArrayList<Line>();
		List<String> strDates = new ArrayList<String>();
		String[] name = { "Angry", "Fearful", "Sad", "Surprise", "Happy", "Disgust", "Neutral" };
		for (int i = 0; i < name.length; i++) {
			Line line = new Line();
			line.setName(name[i]);
			List<Integer> quatity = new ArrayList<>();
			String strName = name[i].toLowerCase();
			int h = 0;
			for (int j = 0; j < 24; j++) {
				int dem = 0;
				for (Customer c : customers) {
					if (h == getHour(c.getDatetime_new()) && strName.equals(c.getEmotion().toLowerCase()) == true) {
						dem += 1;
					}
				}
				quatity.add(dem);
				h += 1;
			}
			line.setData(quatity);
			lines.add(line);
		}
		int hour = 0;
		for (int i = 0; i < 24; i++) {
			strDates.add(
					new StringBuffer().append(hour).toString() + "-" + new StringBuffer().append(hour + 1).toString());
			hour += 1;
		}
		statisticLine.setDataLine(lines);
		statisticLine.setStrDate(strDates);
		return statisticLine;
	}

}
