package admin;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import model.Camera;
import model.Capture;
import service.CameraService;

@Controller
@ComponentScan("serviceimpl.*")
@PropertySource("classpath:application.properties")
public class CameraAdmin {
	final static Logger logger = Logger.getLogger(CameraAdmin.class);

	@Autowired
	private Environment env;

	@Autowired
	private CameraService cameraService;

	@RequestMapping(value = "/admin/camera", method = RequestMethod.GET)
	public String viewcamera(ModelMap mm) {
		logger.info("admin seen view quan ly camera");
		return "admin/camera";
	}

	@RequestMapping(value = "/admin/create/camera", method = RequestMethod.POST, produces = "application/json")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public int cratecamera(@RequestBody Camera camera, Principal principal) {
		Date date = new Date();
		try {
			// camera.setBios(null);
			camera.setLast_time(date);
			// camera.setStatus(0);
			int x = this.cameraService.insertCamera(camera);
			logger.info(principal.getName() + " create camera of shop_id: " + camera.getShop_id());
			return x;

		} catch (Exception e) {
			logger.error(principal.getName() + " create false camera of shop_id: " + camera.getShop_id());
			return 0;
		}
	}

	@RequestMapping(value = "/admin/camera/shop={shop_id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<Camera> listCameraByShopId(@PathVariable(value = "shop_id") int shop_id, Principal principal) {
		List<Camera> cameras = new ArrayList<Camera>();
		try {
			cameras = this.cameraService.getListCameraByShopId(shop_id);
			logger.info(String.format("%s get list camera by shop: %d, info list camera: %s", principal.getName(),
					shop_id, cameras.toString()));
			return cameras;
		} catch (Exception e) {
			logger.error(principal.getName() + " get false list camera by shop: " + shop_id, e);
			return null;
		}
	}

	@RequestMapping(value = "/admin/camera/{camera_id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Camera deatailCamera(Principal principal, @PathVariable(value = "camera_id") int camera_id) {
		Camera camera = new Camera();
		try {
			camera = this.cameraService.getDetailCameraById(camera_id);
			logger.info(principal.getName() + " get detail camera: " + camera_id);
			return camera;
		} catch (Exception e) {
			logger.info(principal.getName() + " get false detail camera: " + camera_id, e);
			return null;
		}
	}

	@RequestMapping(value = "admin/update/camera={camera_id}", method = RequestMethod.POST, produces = "application/json")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public int updateCamera(@RequestBody Camera camera, Principal principal) {
		Camera c = new Camera();
		try {
			c = this.cameraService.getDetailCameraById(camera.getCamera_id());
			camera.setLast_time(c.getLast_time());
			boolean check = this.cameraService.updateStatusCamera(camera);
			if (check == true) {
				return 200;
			} else {
				return 500;
			}
		} catch (Exception e) {
			return 500;
		}
	}

	// public String parseKeySHA(int shop_id, Date date) {
	// String strDate = Long.toString(date.getTime());
	// String key = Integer.toString(shop_id);
	// for (int i = 0; i < (5 - Integer.toString(shop_id).length()); i++) {
	// key = "0" + key;
	// }
	// key += strDate;
	// try {
	// MessageDigest md = MessageDigest.getInstance("SHA-256");
	// md.update(key.getBytes());
	// byte[] byteData = md.digest();
	// StringBuffer hexString = new StringBuffer();
	// for (int i = 0; i < byteData.length; i++) {
	// String hex = Integer.toHexString(0xff & byteData[i]);
	// if (hex.length() == 1)
	// hexString.append('0');
	// hexString.append(hex);
	// }
	// logger.info("///"+hexString.toString());
	// return hexString.toString();
	// } catch (NoSuchAlgorithmException e) {
	// logger.error("false parse key sha-256");
	// return "";
	// }
	// }
	// read file image in folder
	@RequestMapping(value = "/admin/listcapture/camera={camera_id}/date={strdate}", method = RequestMethod.GET, produces = "application/json")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<Capture> getListCapture(@PathVariable(value = "camera_id") int camera_id,
			@PathVariable(value = "strdate") String strdate) {
		List<Capture> captures = new ArrayList<Capture>();
		String fileUrl = env.getProperty("fileUrl");
		String linkCapture = env.getProperty("linkUrlCapture");
		Camera camera;
		try {
			camera = this.cameraService.getDetailCameraById(camera_id);
			String strShop = Integer.toString(camera.getShop_id());
			for (int i = 0; i < (5 - Integer.toString(camera.getShop_id()).length()); i++) {
				strShop = "0" + strShop;
			}
			String strCam = Integer.toString(camera.getCamera_id());
			for (int i = 0; i < (5 - Integer.toString(camera.getCamera_id()).length()); i++) {
				strCam = "0" + strCam;
			}
			fileUrl = fileUrl + "/" + strShop + "/" + strCam + "/" + strdate;
			fileUrl = fileUrl.replaceAll("/+", "/");
			linkCapture = linkCapture+"/" + strShop + "/" + strCam + "/" + strdate;
			linkCapture=linkCapture.replaceAll("/+", "/").replaceAll(":/", "://");
			captures = listFilesForFolder(new File(fileUrl), linkCapture);
			logger.info(String.format("Get list cature of camera:%s on date: %s have total capture:%d",
					camera.toString(), strdate, captures.size()));
			return captures;
		} catch (Exception e) {
			logger.error(String.format("Get list cature  false of camera:%d on date: %s. Exception: %s", camera_id,
					strdate, e));
			return null;
		}
	}

	// array of supported extensions (use a List if you prefer)
	static final String[] EXTENSIONS = new String[] { "gif", "png", "bmp", "jpeg", "jpg" // and other formats you need
	};
	// filter to identify images based on their extensions
	static final FilenameFilter IMAGE_FILTER = new FilenameFilter() {
		@Override
		public boolean accept(final File dir, final String name) {
			for (final String ext : EXTENSIONS) {
				if (name.endsWith("." + ext)) {
					return (true);
				}
			}
			return (false);
		}
	};

	public List<Capture> listFilesForFolder(final File folderRead, String linkCapture) {
		List<Capture> captures = new ArrayList<Capture>();
		if (folderRead.isDirectory()) { // make sure it's a directory
			for (final File f : folderRead.listFiles(IMAGE_FILTER)) {
				@SuppressWarnings("unused")
				BufferedImage img = null;
				try {
					img = ImageIO.read(f);
					// you probably want something more involved here
					// to display in your UI
					Capture capture = new Capture();
					String s = linkCapture + "/" + f.getName();
					capture.setUrl(s);
					capture.setDate(parseTimeUnix(f.getName()));
					logger.debug(String.format("show image capture:%s", capture.toString()));
					captures.add(capture);
				} catch (Exception e) {
					logger.error(String.format("shown't image capture: %s ", f.toString()));
				}
			}
			logger.info(String.format("show list capture have size: %d", captures.size()));
		}
		return captures;
	}

	public Date parseTimeUnix(String str) {
		try {
			String[] chuoi = str.split("\\.");
			long fDate = Long.parseLong(chuoi[0]);
			Date date = new Date(fDate);
			return date;
		} catch (Exception e) {
			return null;
		}
	}

	@RequestMapping(value = "/admin/capture/camera={camera_id}", method = RequestMethod.GET)
	public String viewCapture(Principal principal, @PathVariable(value = "camera_id") int camera_id, ModelMap mm) {
		mm.put("cameraId", camera_id);
		return "admin/capture";
	}
}
