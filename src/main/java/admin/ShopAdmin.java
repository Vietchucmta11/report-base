package admin;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import service.ShopService;
import model.Shop;
@Controller
@ComponentScan("serviceimpl.*")
public class ShopAdmin {
	final static Logger logger= Logger.getLogger(ShopAdmin.class);
	
	@Autowired
	private ShopService shopService;
	
	@RequestMapping(value="/admin/shop", method=RequestMethod.GET)
	public String listShop(ModelMap mm) {
		mm.put("idTableShop", "listShop");
		return "admin/shop";
	}
	
	@RequestMapping(value="/admin/getListShop", method=RequestMethod.GET, produces= MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<Shop> getListShop(Principal principal){
		List<Shop> shops= new ArrayList<Shop>();
		try {
			shops=this.shopService.getListShop();
			logger.info(String.format("%s get info %d shop", principal.getName(),shops.size()));
			return shops;
		} catch (Exception e) {
			logger.error(principal.getName()+" get list shop false: ",e);
			return null;
		}
	}
	
//	@RequestMapping(value="/admin/createShop", method=RequestMethod.GET)
//	public String viewcreateShop() {
//		return "admin/createShop";
//	}
	
	@RequestMapping(value="/admin/createshop", method= RequestMethod.POST, produces="application/json")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public int createShop(@RequestBody Shop shop, Principal principal) {
		try {
			this.shopService.insertShop(shop);
			return 200;
		} catch (Exception e) {
			logger.error(principal.getName()+" create false new shop ", e);
			
			return 500;
		}
	}
	
	@RequestMapping( value="/admin/shop/{shop_id}", method= RequestMethod.GET, produces= "application/json")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Shop detailShop(@PathVariable( value="shop_id") int shop_id, Principal principal) {
		Shop shop= new Shop();
		try {
			shop=this.shopService.getDetailShopById(shop_id);
			return shop;
		} catch (Exception e) {
			return null;
		}
	}
	
	@RequestMapping(value="/admin/update/shop={shop_id}",method=RequestMethod.POST, produces= "application/json")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public int updateShop(@RequestBody Shop shop, Principal principal) {
		try {
			int x=shopService.updateShop(shop);
			logger.info(principal.getName()+ " update shop: "+ shop.getShop_id());
			return x;
		} catch (Exception e) {
			logger.error(principal.getName()+ "update false shop: "+ shop.getShop_id(), e);
			return 500;
		}
	}
	
}
