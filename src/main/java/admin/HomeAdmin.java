package admin;

import java.security.Principal;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@ComponentScan("serviceimpl.*")
public class HomeAdmin {
	// show header admin
	@RequestMapping(value = "/admin/header", method = RequestMethod.GET)
	public String headerAdmin(ModelMap model, Principal principal) {
		model.put("message", principal.getName());
		return "admin/header";
	}

	// show footer admin
	@RequestMapping(value = "/admin/footer", method = RequestMethod.GET)
	public String footerAdmin(ModelMap mm) {
		return "admin/footer";
	}

	// Show home admin
	@RequestMapping(value = "/admin/home", method = RequestMethod.GET)
	public String homeAdmin(ModelMap mm) {
		return "admin/home";
	}

}
