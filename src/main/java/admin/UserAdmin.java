package admin;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import model.Authorities;
import model.ShopUser;
import model.ShowUser;
import model.User;
import service.AuthoritiesService;
import service.ShopUserService;
import service.UserService;

@Controller
@ComponentScan("serviceimpl.*")
@EnableWebSecurity
public class UserAdmin {
	final static Logger logger = Logger.getLogger(UserAdmin.class);

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	};

	@Autowired
	private UserService userService;

	@Autowired
	private ShopUserService shopUserService;

	@RequestMapping(value = "/admin/listAccount", method = RequestMethod.GET)
	public String viewListUser(ModelMap mm) {
		mm.put("idTableUser", "listAccount");
		return "admin/user";
	}

	@Autowired
	private AuthoritiesService authoritiesService;

	// show data Json
	@RequestMapping(value = "/admin/listUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<ShowUser> listAnalyticByDate(Principal principal) {
		List<ShowUser> showUsers = new ArrayList<ShowUser>();
		List<User> users = new ArrayList<User>();
		try {
			users = this.userService.getListUsers();
			for (User u : users) {
				ShowUser showUser = new ShowUser();
				showUser.setUserName(u.getUsername());
				showUser.setPassWord(u.getPassword());
				ShopUser shopUser = new ShopUser();
				try {
					shopUser = this.shopUserService.getShopUserByUsername(u.getUsername());
					showUser.setShop_id(shopUser.getShop_id());
				} catch (Exception e) {
					showUser.setShop_id(-1);
				}
				showUser.setAuthority(authoritiesService.getDetailAuthorities(u.getUsername()).getAuthority());
				showUser.setEnabled(u.isEnabled());
				showUsers.add(showUser);
			}
			logger.info("Admin " + principal.getName() + " seen page list user");
			return showUsers;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Admin " + principal.getName() + " not seen list user: ", e);
			return null;
		}

	}

	@RequestMapping(value = "/admin/newAccount", method = RequestMethod.GET)
	public String viewNewAccount(ModelMap mm, Principal principal) {
		logger.info("Admin" + principal.getName() + " seen view create account");
		return "admin/newAccount";
	}

	@RequestMapping(value = "/admin/createUser", method = RequestMethod.POST, produces = "application/json")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public int createUser(@RequestBody ShowUser showUser, Principal principal) {
		List<User> users = new ArrayList<User>();
		User user = new User();
		ShopUser shopUser = new ShopUser();
		Authorities authorities = new Authorities();
		try {
			users = this.userService.getListUsers();
			int x = 0;
			for (User u : users) {
				if (showUser.getUserName().equals(u.getUsername()) == true) {
					x += 1;
				}
			}
			if (x == 0) {
				user.setUsername(showUser.getUserName());
				String encoded = new BCryptPasswordEncoder().encode(showUser.getPassWord());
				user.setPassword(encoded);
				user.setEnabled(showUser.isEnabled());
				this.userService.insertUser(user);
				logger.info("Admin " + principal.getName() + "create user: " + user.getUsername());
				///
				authorities.setUsername(showUser.getUserName());
				authorities.setAuthority(showUser.getAuthority());
				this.authoritiesService.insertAnthorities(authorities);
				logger.info("admin " + principal.getName() + "authority: " + authorities.getAuthority() + " for"
						+ user.getUsername());
				///
				if (showUser.getAuthority().equals("ROLE_USER") == true) {
					shopUser.setShop_id(showUser.getShop_id());
					shopUser.setUsername(showUser.getUserName());
					this.shopUserService.insertShopUser(shopUser);
					logger.info("Admin" + principal.getName() + "create shopUser: " + shopUser.getShop_id() + " for "
							+ user.getUsername());
				}
				return 200;
			} else {
				return 400;
			}
		} catch (Exception e) {
			logger.error("Admin " + principal.getName() + "not create account", e);
			return 500;
		}
	}

	// @RequestMapping(vaulue ="/admin/updateUser",
	// method=RequestMethod.POST,produces="application/json")

	@RequestMapping(value = "/admin/detail/user={username}", method = RequestMethod.GET, produces = "application/json")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public ShowUser showDetailAcc(@PathVariable(value = "username") String username, Principal principal) {
		ShowUser showUser = new ShowUser();
		User user = new User();
		ShopUser shopUser = new ShopUser();
		Authorities authorities = new Authorities();
		try {
			user = this.userService.findUserByUsername(username);
			authorities = this.authoritiesService.getDetailAuthorities(username);
			if (authorities.getAuthority().equals("ROLE_ADMIN") == true) {
				shopUser.setUsername(username);
				shopUser.setShop_id(-1);
			} else {
				shopUser = this.shopUserService.getShopUserByUsername(username);
			}
			showUser.setUserName(username);
			showUser.setPassWord(user.getPassword());
			showUser.setEnabled(user.isEnabled());
			showUser.setShop_id(shopUser.getShop_id());
			showUser.setAuthority(authorities.getAuthority());
			logger.info(principal.getName() + " get detail user: " + username);
			return showUser;
		} catch (Exception e) {
			logger.error(principal.getName() + " get fasle user" + username, e);
			return null;
		}
	}

	@RequestMapping(value = "/admin/update/user={username}", method = RequestMethod.POST, produces = "application/json")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public int updateAcc(@RequestBody ShowUser showUser,@PathVariable(value="username") String username ,Principal principal) {
		ShopUser shopUser = new ShopUser();
		User user = new User();
		Authorities authorities = new Authorities();
		try {

			shopUser = this.shopUserService.getShopUserByUsername(username);
			shopUser.setShop_id(showUser.getShop_id());
			this.shopUserService.updateShopUser(shopUser);

			authorities = this.authoritiesService.getDetailAuthorities(username);
			authorities.setAuthority(showUser.getAuthority());
			this.authoritiesService.updateAuthorities(authorities);

			if (showUser.getPassWord() == null) {
				user = this.userService.findUserByUsername(username);
				user.setEnabled(showUser.isEnabled());
				this.userService.updateUser(user);
			} else {
				user.setUsername(username);
				String encoded = new BCryptPasswordEncoder().encode(showUser.getPassWord());
				user.setPassword(encoded);
				user.setEnabled(showUser.isEnabled());
				this.userService.updateUser(user);
			}
			logger.info(principal.getName() +"  update account user: "+ showUser.getUserName());
			return 200;
		} catch (Exception e) {
			logger.error(principal.getName() +"  update false user: " + showUser.getUserName(),e);
			return 500;
		}
	}

}
