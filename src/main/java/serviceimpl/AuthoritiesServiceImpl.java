package serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.AuthoritiesDao;
import model.Authorities;
import service.AuthoritiesService;

@Service
@Transactional(readOnly = true)
@ComponentScan("daoimpl.*")
public class AuthoritiesServiceImpl implements AuthoritiesService {
	@Autowired
	private AuthoritiesDao authoritiesDao;
	
	@Override
	public int insertAnthorities(Authorities authorities) {
		return this.authoritiesDao.insertAnthorities(authorities);
	}

	@Override
	public Authorities getDetailAuthorities(String username) {
		return authoritiesDao.getDetailAuthorities(username);
	}

	@Override
	public int updateAuthorities(Authorities authorities) {
		return authoritiesDao.updateAuthorities(authorities);
	}

}
