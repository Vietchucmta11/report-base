package serviceimpl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.UserDao;
import model.User;
import service.AuthoritiesService;

@Service("userDetailsService")
@ComponentScan("daoimpl.*")
public class UserDetailsServiceImpl implements UserDetailsService {
	final static Logger logger= Logger.getLogger(UserDetailsServiceImpl.class);
	
	 @Autowired
	  private UserDao userDao;

	 @Autowired
	 private AuthoritiesService authoritiesService;
	 
	  @Transactional(readOnly = true)
	  @Override
	  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

	    User user = userDao.findUserByUsername(username);
	    UserBuilder builder = null;
	    String authority;
	    if (user != null) {
	      builder = org.springframework.security.core.userdetails.User.withUsername(username);
	      builder.disabled(!user.isEnabled());
	      builder.password(user.getPassword());
	      authority = authoritiesService.getDetailAuthorities(username).getAuthority();
	      builder.authorities(authority);
	    } else {
	      throw new UsernameNotFoundException("User not found.");
	    }
	    logger.info(String.format("%s have authority %s working with system",username,authority));
	    return builder.build();
	  }
}
