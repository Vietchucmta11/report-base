package serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.ShopDao;
import model.Shop;
import service.ShopService;

@Service
@Transactional
@ComponentScan("daoimpl.*")
public class ShopServiceImpl implements ShopService {
	@Autowired
	private ShopDao shopDao;

	@Override
	public Shop getDetailShopById(int shop_id) {
		return shopDao.getDetailShopById(shop_id);
	}

	@Override
	public List<Shop> getListShop() {
		return shopDao.getListShop();
	}

	@Override
	public int insertShop(Shop shop) {
		return shopDao.insertShop(shop);
	}

	@Override
	public int updateShop(Shop shop) {
		return shopDao.updateShop(shop);
	}
}
