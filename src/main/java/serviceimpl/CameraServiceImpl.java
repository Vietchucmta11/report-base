package serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.CameraDao;
import model.Camera;
import service.CameraService;

@Service
@Transactional
@ComponentScan("daoimpl.*")
public class CameraServiceImpl implements CameraService {

	@Autowired
	private CameraDao cameraDao;

	@Override
	public Camera getDetailCameraById(int camera_id) {
		return cameraDao.getDetailCameraById(camera_id);
	}

	@Override
	public List<Camera> getListCameraByShopId(int shop_id) {
		return cameraDao.getListCameraByShopId(shop_id);
	}

	@Override
	public boolean updateStatusCamera(Camera camera) {
		return cameraDao.updateStatusCamera(camera);
	}

	@Override
	public int insertCamera(Camera camera) {
		return cameraDao.insertCamera(camera);
	}

}
