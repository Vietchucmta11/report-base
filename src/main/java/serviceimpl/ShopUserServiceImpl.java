package serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.ShopUserDao;
import model.ShopUser;
import service.ShopUserService;

@Service
@Transactional
@ComponentScan("daoimpl.*")
public class ShopUserServiceImpl implements ShopUserService {

	@Autowired
	private ShopUserDao shopUserDao;

	@Override
	public ShopUser getShopUserByUsername(String username) {
		return shopUserDao.getShopUserByUsername(username);
	}

	// @Override
	// public ShopUser getShopUserByShopId(int shop_id) {
	// return shopUserDao.getShopUserByShopId(shop_id);
	// }
	@Override
	public int insertShopUser(ShopUser shopUser) {
		return shopUserDao.insertShopUser(shopUser);
	}

	@Override
	public int updateShopUser(ShopUser shopUser) {
		return shopUserDao.updateShopUser(shopUser);
	}

}
