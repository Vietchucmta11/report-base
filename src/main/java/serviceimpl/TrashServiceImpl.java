package serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.TrashDao;
import helper.UrlHelper;
import model.Trash;
import service.TrashService;

@Service
@Component
@Transactional
@ComponentScan("daoimpl.*")
@PropertySource("classpath:application.properties")
public class TrashServiceImpl implements TrashService {


	@Value("${photo.url.prefix}")
	private String prefixPhotoUrl;

	UrlHelper helper = UrlHelper.getSingleTon();

	
	@Autowired
	private TrashDao trashDao;

	@Override
	public List<Trash> getListTrashByShop(int shop_id) {
		return helper.toFullPathTrash(this.trashDao.getListTrashByShop(shop_id),prefixPhotoUrl);		
	}

	@Override
	public boolean deleteTrash(int id) {
		return this.trashDao.deleteTrash(id);
	}

	@Override
	public boolean insertTrash(Trash trash) {
		return this.trashDao.insertTrash(trash);
	}

	@Override
	public Trash getTrashById(int id) {
		return this.trashDao.getTrashById(id);
	}

}
