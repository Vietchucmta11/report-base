package serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import dao.CustomerDao;
import helper.UrlHelper;
import model.Customer;
import service.CustomerService;

@Service
@Transactional
@Component
@ComponentScan("daoimpl.*")
@PropertySource("classpath:application.properties")
public class CustomerServiceImpl implements CustomerService {

	@Value("${photo.url.prefix}")
	private String prefixPhotoUrl;

	UrlHelper helper = UrlHelper.getSingleTon();

	@Autowired
	private CustomerDao customerDao;

	@Override
	public int insertCustomer(Customer customer) {
		return customerDao.insertCustomer(customer);
	}

	@Override
	public Customer getCustomerById(int id) {
		return customerDao.getCustomerById(id);
	}

	@Override
	public int updateCustomer(int id, Customer customer) {
		return customerDao.updateCustomer(id, customer);
	}

	@Override
	public List<Customer> getListCustomerByShop(int shop_id) {
		return customerDao.getListCustomerByShop(shop_id);
	}

	@Override
	public int updateAnalytic(Customer customer) {
		return customerDao.updateAnalytic(customer);
	}

	@Override
	public List<Customer> getListCustomeByShopDate(int Shop_id, String strDate) {
		return helper.toFullPath(customerDao.getListCustomeByShopDate(Shop_id, strDate), prefixPhotoUrl);
	}

	@Override
	public List<Customer> getListCustomerByShopCameraDate(int shop_id, int camera_id, String strDate) {
		return helper.toFullPath(customerDao.getListCustomerByShopCameraDate(shop_id, camera_id, strDate),
				prefixPhotoUrl);
	}

	@Override
	public boolean updateListAnalytic(Customer customer) {
		return customerDao.updateListAnalytic(customer);
	}

	@Override
	public Customer getCustomerByTracking_New(int shop_id, String tracking_new) {
		return customerDao.getCustomerByTracking_New(shop_id, tracking_new);
	}

	@Override
	public List<Customer> getListCustomerDateToDate(int shop_id, String startDate, String endDate) {
		return customerDao.getListCustomerDateToDate(shop_id, startDate, endDate);
	}

	@Override
	public List<Customer> getListCustomerDate(int shop_id, String strDate) {
		return customerDao.getListCustomerDate(shop_id, strDate);
	}

	@Override
	public boolean deleteCustomerById(int id) {
		return customerDao.deleteCustomerById(id);
	}

	@Override
	public boolean undoCustomer(Customer customer) {
		return customerDao.undoCustomer(customer);
	}

}
