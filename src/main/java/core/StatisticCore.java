package core;

import java.util.Date;
import java.util.List;

import model.Customer;
import model.Statistic;
import model.StatisticLine;

public interface StatisticCore {
	public List<Statistic> statisticByEmotion(List<Customer> customers);

	public List<Statistic> statisticByAge(List<Customer> customers);

	public List<Statistic> statisticByGender(List<Customer> customers);
	
	public StatisticLine statisticLineDateToDate(List<Customer> customers, Date startDate, Date endDate);

	public StatisticLine statisticLineByGender(List<Customer> customers, Date startDate, Date endDate);

	public StatisticLine statisticLineEmotion(List<Customer> customers, Date startDate, Date endDate);

	public StatisticLine statisticLineAge(List<Customer> customers, Date startDate, Date endDate);
	
	public List<Statistic> statisticLinedate(List<Customer> customers);
	
	public StatisticLine statisticLineGenderDate(List<Customer> customers);
	
	public StatisticLine statisticLineAgeDate(List<Customer> customers);
	
	public StatisticLine statisticLineEmtionDate(List<Customer> customers);
}
