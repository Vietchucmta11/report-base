package api;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import core.StatisticCore;
import model.Customer;
import model.ShopUser;
import model.Statistic;
import model.StatisticLine;
import service.CustomerService;
import service.ShopUserService;

@Controller
@ComponentScan({ "serviceimpl", "coreimpl" })
public class StatisticApi {

	final static Logger logger = Logger.getLogger(StatisticApi.class);
	@Autowired
	private CustomerService customerService;

	@Autowired
	private ShopUserService shopUserService;

	@Autowired
	private StatisticCore statisticCore;

	/// thong ke trong khoảng ngày
	@RequestMapping(value = "/user/statistic/emotion/{startDate}_{endDate}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<Statistic> statisticByEmotion(@PathVariable(value = "startDate") String startDate,
			@PathVariable(value = "endDate") String endDate, Principal principal) {
		List<Customer> customers = new ArrayList<Customer>();
		ShopUser shopUser = new ShopUser();
		List<Statistic> statistics = new ArrayList<Statistic>();
		try {
			shopUser = this.shopUserService.getShopUserByUsername(principal.getName());
			customers = this.customerService.getListCustomerDateToDate(shopUser.getShop_id(), startDate, endDate);
			statistics = statisticCore.statisticByEmotion(customers);
			logger.info(String.format("%s Get list statistic by emotion startDate: %s to endDate: %s",
					principal.getName(), startDate, endDate));

			return statistics;
		} catch (Exception e) {
			logger.error(
					String.format("%s Get list statistic by emotion startDate: %s to endDate: %s false. Exception: %s",
							principal.getName(), startDate, endDate, e));

			return null;
		}
	}

	@RequestMapping(value = "/user/statistic/gender/{startDate}_{endDate}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<Statistic> statisticByGender(@PathVariable(value = "startDate") String startDate,
			@PathVariable(value = "endDate") String endDate, Principal principal) {
		List<Customer> customers = new ArrayList<Customer>();
		ShopUser shopUser = new ShopUser();
		List<Statistic> statistics = new ArrayList<Statistic>();
		try {
			shopUser = this.shopUserService.getShopUserByUsername(principal.getName());
			customers = this.customerService.getListCustomerDateToDate(shopUser.getShop_id(), startDate, endDate);
			statistics = statisticCore.statisticByGender(customers);
			logger.info(String.format("%s Get list statistic by gender startDate: %s to endDate: %s ",
					principal.getName(), startDate, endDate));
			return statistics;
		} catch (Exception e) {
			logger.error(
					String.format("%s Get list statistic by gender startDate: %s to endDate: %s false. Exception: %s",
							principal.getName(), startDate, endDate, e));
			return null;
		}
	}

	@RequestMapping(value = "/user/statistic/age/{startDate}_{endDate}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<Statistic> statisticByAge(@PathVariable(value = "startDate") String startDate,
			@PathVariable(value = "endDate") String endDate, Principal principal) {
		List<Customer> customers = new ArrayList<Customer>();
		ShopUser shopUser = new ShopUser();
		List<Statistic> statistics = new ArrayList<Statistic>();
		try {
			shopUser = this.shopUserService.getShopUserByUsername(principal.getName());
			customers = this.customerService.getListCustomerDateToDate(shopUser.getShop_id(), startDate, endDate);
			statistics = statisticCore.statisticByAge(customers);
			logger.info(String.format(" %s Get list statistic by age startDate:%s to endDate:%s", principal.getName(),
					startDate, endDate));

			return statistics;
		} catch (Exception e) {
			logger.error(String.format(" %s Get list statistic by age startDate:%s to endDate:%s false. Exception: %s",
					principal.getName(), startDate, endDate, e));

			return null;
		}
	}

	// get data statistic line date to date
	@RequestMapping(value = "/user/statistic/line/{startDate}_{endDate}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public StatisticLine statisticLineDateToDate(@PathVariable(value = "startDate") String startDate,
			@PathVariable(value = "endDate") String endDate, Principal principal) {
		List<Customer> customers = new ArrayList<Customer>();
		ShopUser shopUser = new ShopUser();
		StatisticLine statisticLine = new StatisticLine();
		try {
			shopUser = this.shopUserService.getShopUserByUsername(principal.getName());
			customers = this.customerService.getListCustomerDateToDate(shopUser.getShop_id(), startDate, endDate);
			SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
			Date sDate = formater.parse(startDate);
			Date eDate = formater.parse(endDate);
			statisticLine = statisticCore.statisticLineDateToDate(customers, sDate, eDate);
			logger.info(String.format("%s get list statistic line of shop_id:%d from startDate:%s to endDate: %s",
					principal.getName(), shopUser.getShop_id(), startDate, endDate));

			return statisticLine;
		} catch (Exception e) {
			logger.error(String.format(
					"%s get list statistic line of shop_id:%d from startDate:%s to endDate: %s false. Exception: %s",
					principal.getName(), shopUser.getShop_id(), startDate, endDate, e));
			return null;
		}
	}

	// get data statistic gender
	@RequestMapping(value = "/user/statistic/linegender/{startDate}_{endDate}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public StatisticLine statisticLineGender(@PathVariable(value = "startDate") String startDate,
			@PathVariable(value = "endDate") String endDate, Principal principal) {
		List<Customer> customers = new ArrayList<Customer>();
		ShopUser shopUser = new ShopUser();
		StatisticLine statisticLine = new StatisticLine();
		try {
			shopUser = this.shopUserService.getShopUserByUsername(principal.getName());
			customers = this.customerService.getListCustomerDateToDate(shopUser.getShop_id(), startDate, endDate);
			SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
			Date sDate = formater.parse(startDate);
			Date eDate = formater.parse(endDate);
			statisticLine = statisticCore.statisticLineByGender(customers, sDate, eDate);
			logger.info(
					String.format("%s get list statistic line gender of shop_id:%d from startDate : %s to endDate: %s",
							principal.getName(), shopUser.getShop_id(), startDate, endDate));

			return statisticLine;
		} catch (Exception e) {
			logger.error(String.format(
					"%s get list statistic line gender of shop_id:%d from startDate : %s to endDate: %s flase. Exception: %s",
					principal.getName(), shopUser.getShop_id(), startDate, endDate, e));

			return null;
		}
	}

	@RequestMapping(value = "/user/statistic/lineemotion/{startDate}_{endDate}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public StatisticLine statisticLineEmotion(@PathVariable(value = "startDate") String startDate,
			@PathVariable(value = "endDate") String endDate, Principal principal) {
		List<Customer> customers = new ArrayList<Customer>();
		ShopUser shopUser = new ShopUser();
		StatisticLine statisticLine = new StatisticLine();
		try {
			shopUser = this.shopUserService.getShopUserByUsername(principal.getName());
			customers = this.customerService.getListCustomerDateToDate(shopUser.getShop_id(), startDate, endDate);
			SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
			Date sDate = formater.parse(startDate);
			Date eDate = formater.parse(endDate);
			statisticLine = this.statisticCore.statisticLineEmotion(customers, sDate, eDate);
			logger.info(
					String.format("%s get list statistic line emotion of shop_id:%d  from startDate:%s  endDate: %s",
							principal.getName(), shopUser.getShop_id(), startDate, endDate));

			return statisticLine;
		} catch (Exception e) {
			logger.error(String.format(
					"%s get list statistic line emotion of shop_id:%d  from startDate:%s  endDate: %s false. Excweption: %s",
					principal.getName(), shopUser.getShop_id(), startDate, endDate, e));
			return null;
		}
	}

	// thống kê độ tuổi cho bd line chart
	@RequestMapping(value = "/user/statistic/lineage/{startDate}_{endDate}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public StatisticLine statisticLineAge(@PathVariable(value = "startDate") String startDate,
			@PathVariable(value = "endDate") String endDate, Principal principal) {
		List<Customer> customers = new ArrayList<Customer>();
		ShopUser shopUser = new ShopUser();
		StatisticLine statisticLine = new StatisticLine();
		try {
			shopUser = shopUserService.getShopUserByUsername(principal.getName());
			customers = customerService.getListCustomerDateToDate(shopUser.getShop_id(), startDate, endDate);
			SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
			Date sDate = formater.parse(startDate);
			Date eDate = formater.parse(endDate);
			statisticLine = statisticCore.statisticLineAge(customers, sDate, eDate);
			logger.info(
					String.format(" %s get list statistic line age of shop_id:%d from startDate: %s to endDate: %s ",
							principal.getName(), shopUser.getShop_id(), startDate, endDate));
			return statisticLine;
		} catch (Exception e) {
			logger.error(String.format(
					" %s get list statistic line age of shop_id:%d from startDate: %s to endDate: %s false. Exception: %s ",
					principal.getName(), shopUser.getShop_id(), startDate, endDate, e));
			return null;
		}
	}
	// thống kê lượng ảnh trong các ngày

	////// end thống kê trong khoảng ngày

	//// Thống kê trong ngày

	@RequestMapping(value = "/user/statistic/linedate/{date}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<Statistic> statisticDate(@PathVariable(value = "date") String strDate, Principal principal) {
		List<Statistic> statistics = new ArrayList<Statistic>();
		List<Customer> customers = new ArrayList<Customer>();
		ShopUser shopUSer = new ShopUser();
		try {
			shopUSer = this.shopUserService.getShopUserByUsername(principal.getName());
			customers = customerService.getListCustomerDate(shopUSer.getShop_id(), strDate);
			statistics = statisticCore.statisticLinedate(customers);
			return statistics;
		} catch (Exception e) {
			return null;
		}
	}

	@RequestMapping(value = "/user/statistic/genderdate/{date}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public StatisticLine statisticLineGenderDate(@PathVariable(value = "date") String startDate, Principal principal) {
		List<Customer> customers = new ArrayList<Customer>();
		ShopUser shopUser = new ShopUser();
		StatisticLine statisticLine = new StatisticLine();
		try {
			shopUser = shopUserService.getShopUserByUsername(principal.getName());
			customers = customerService.getListCustomerDate(shopUser.getShop_id(), startDate);
			statisticLine = statisticCore.statisticLineGenderDate(customers);
			logger.info(principal.getName() + " get list statistic line gender on date of shop_id: "
					+ shopUser.getShop_id() + " on date: " + startDate + " / ");
			return statisticLine;
		} catch (Exception e) {
			logger.error(principal.getName() + " get false list statistic line gender on date of shop_id: "
					+ shopUser.getShop_id() + " on date: " + startDate + " / ", e);
			return null;
		}
	}

	// ve bieu do tron thống kê cảm xúc trong ngày
	@RequestMapping(value = "/user/statistic/emotiondate/{date}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public StatisticLine statisticLineEmotionDate(@PathVariable(value = "date") String startDate, Principal principal) {
		List<Customer> customers = new ArrayList<Customer>();
		ShopUser shopUser = new ShopUser();
		StatisticLine statisticLine = new StatisticLine();
		try {
			shopUser = shopUserService.getShopUserByUsername(principal.getName());
			customers = customerService.getListCustomerDate(shopUser.getShop_id(), startDate);
			statisticLine = statisticCore.statisticLineEmtionDate(customers);
			logger.info(principal.getName() + " get list statistic line emotion on date of shop_id: "
					+ shopUser.getShop_id() + " on date: " + startDate + " / ");
			return statisticLine;
		} catch (Exception e) {
			logger.error(principal.getName() + " get false list statistic line emotion on date of shop_id: "
					+ shopUser.getShop_id() + " on date: " + startDate + " / ", e);
			return null;
		}
	}

	@RequestMapping(value = "/user/statistic/agedate/{date}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public StatisticLine statisticLineAgeDate(@PathVariable(value = "date") String startDate, Principal principal) {
		List<Customer> customers = new ArrayList<Customer>();
		ShopUser shopUser = new ShopUser();
		StatisticLine statisticLine = new StatisticLine();
		try {
			shopUser = shopUserService.getShopUserByUsername(principal.getName());
			customers = customerService.getListCustomerDate(shopUser.getShop_id(), startDate);
			statisticLine = statisticCore.statisticLineAgeDate(customers);
			logger.info(principal.getName() + " get list statistic line emotion on date of shop_id: "
					+ shopUser.getShop_id() + " on date: " + startDate + " / ");
			return statisticLine;
		} catch (Exception e) {
			logger.error(principal.getName() + " get false list statistic line emotion on date of shop_id: "
					+ shopUser.getShop_id() + " on date: " + startDate + " / ", e);
			return null;
		}
	}

	//// end thống kê trong ngày

	// thống kê tổng quát
	@RequestMapping(value = "/user/statistic/sum/{startDate}_{endDate}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<Statistic> statisticSum(@PathVariable(value = "startDate") String startDate,
			@PathVariable(value = "endDate") String endDate, Principal principal) {
		List<Statistic> statistics = new ArrayList<Statistic>();
		List<Customer> customers = new ArrayList<Customer>();
		ShopUser shopUser = new ShopUser();
		try {
			shopUser = this.shopUserService.getShopUserByUsername(principal.getName());
			customers = this.customerService.getListCustomerDateToDate(shopUser.getShop_id(), startDate, endDate);
			List<Statistic> statisticsEmotion = new ArrayList<Statistic>();
			List<Statistic> statisticsAge = new ArrayList<Statistic>();
			List<Statistic> statisticGender = new ArrayList<Statistic>();
			// tổng số
			Statistic stcSum = new Statistic();
			stcSum.setName("Tổng số");
			stcSum.setQuatity(customers.size());
			statistics.add(stcSum);
			// giới tính
			statisticGender = this.statisticCore.statisticByGender(customers);
			statistics.addAll(statisticGender);
			// tuoi
			statisticsAge = this.statisticCore.statisticByAge(customers);
			Statistic stcAge = new Statistic("", 0);
			for (Statistic s : statisticsAge) {
				if (s.getQuatity() > stcAge.getQuatity()) {
					stcAge = s;
				}
			}
			statistics.add(stcAge);
			// emotion
			statisticsEmotion = this.statisticCore.statisticByEmotion(customers);
			Statistic stcEmotion = new Statistic("", 0);
			for (Statistic s : statisticsEmotion) {
				if (s.getQuatity() > stcEmotion.getQuatity()) {
					stcEmotion = s;
				}
			}
			statistics.add(stcEmotion);
			logger.info(String.format(
					"%s get statis sum by shop_id: %d start date: %s to end date: %s have sum: %s, gender: %s,age common: %s ,emotion common: %s",
					principal.getName(), shopUser.getShop_id(), startDate, endDate, stcSum.toString(),
					statisticGender.toString(), stcAge.toString(), stcEmotion.toString()));
			return statistics;
		} catch (Exception e) {
			logger.error(principal.getName() + "get statis sum false", e);
			return null;
		}
	}

	@RequestMapping(value = "/user/shop_camera={str}/sum/{startDate}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<Statistic> statisticSumByCamera(@PathVariable(value = "startDate") String startDate,
			@PathVariable(value = "str") String str, Principal principal) {
		String[] chuoi = str.split("\\-");
		int camera_id = Integer.parseInt(chuoi[1]);
		List<Statistic> statistics = new ArrayList<Statistic>();
		List<Customer> customers = new ArrayList<Customer>();
		ShopUser shopUser = new ShopUser();
		try {
			shopUser = this.shopUserService.getShopUserByUsername(principal.getName());
			customers = this.customerService.getListCustomerByShopCameraDate(shopUser.getShop_id(), camera_id,
					startDate);
			List<Statistic> statisticsEmotion = new ArrayList<Statistic>();
			List<Statistic> statisticsAge = new ArrayList<Statistic>();
			List<Statistic> statisticGender = new ArrayList<Statistic>();
			// tổng số
			Statistic stcSum = new Statistic();
			stcSum.setName("Tổng số");
			stcSum.setQuatity(customers.size());
			statistics.add(stcSum);
			// giới tính
			statisticGender = this.statisticCore.statisticByGender(customers);
			statistics.addAll(statisticGender);
			// tuoi
			statisticsAge = this.statisticCore.statisticByAge(customers);
			Statistic stcAge = new Statistic("", 0);
			for (Statistic s : statisticsAge) {
				if (s.getQuatity() > stcAge.getQuatity()) {
					stcAge = s;
				}
			}
			statistics.add(stcAge);
			// emotion
			statisticsEmotion = this.statisticCore.statisticByEmotion(customers);
			Statistic stcEmotion = new Statistic("", 0);
			for (Statistic s : statisticsEmotion) {
				if (s.getQuatity() > stcEmotion.getQuatity()) {
					stcEmotion = s;
				}
			}
			statistics.add(stcEmotion);
			logger.info(String.format(
					"%s get statis sum by camera_id: %d date: %s have sum: %s, gender: %s,age common: %s ,emotion common: %s",
					principal.getName(), camera_id, startDate, stcSum.toString(), statisticGender.toString(),
					stcAge.toString(), stcEmotion.toString()));
			return statistics;
		} catch (Exception e) {
			logger.error(principal.getName() + "get false statis sum by camera: " + camera_id, e);
			return null;
		}
	}
}
