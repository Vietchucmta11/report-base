package api;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import model.Camera;
import model.Capture;
import service.CameraService;

@RestController
@ComponentScan("serviceimpl")
@PropertySource("classpath:application.properties")
public class CameraApi {
	@Autowired
	private Environment env;

	final static Logger logger = Logger.getLogger(CameraApi.class);

	@Autowired
	private CameraService cameraService;

	// update last time camera
	@RequestMapping(value = "/camera/update", method = RequestMethod.POST, produces = "application/json")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public String updateCustomer(@RequestBody Camera camera) {
		Camera oldCamera = new Camera();
		try {
			oldCamera = this.cameraService.getDetailCameraById(camera.getCamera_id());
			boolean check = false;
			// if (camera.getBios().equals(camera_old.getBios()) == true) {
			// x += this.cameraService.updateStatusCamera(camera_old);
			// }
			if (oldCamera.getShop_id() == camera.getShop_id()) {
				oldCamera.setLast_time(camera.getLast_time());
				check = this.cameraService.updateStatusCamera(oldCamera);
			}
			if (check == true) {
				logger.info(String.format("Update last_time camera have data:%s result true", camera.toString()));
				return "{\"result\":true}";
			} else {
				logger.error(String.format(
						"Update last_time camera have data:%s result false because camera doesn't belong shop",
						camera.toString()));
				return "{\"result\":false}";
			}
		} catch (Exception e) {
			logger.error(String.format("Update last_time camera have data:%s result false. Exception: %s",
					camera.toString(), e));
			return "{\"result\":false}";
		}
	}

	// update bios camera
	// @RequestMapping(value = "/camera/bios", method = RequestMethod.POST, produces
	// = "application/json")
	// @ResponseStatus(HttpStatus.OK)
	// @ResponseBody
	// public String updateBiosCustomer(@RequestBody Camera camera) {
	// logger.info("Recive data update bios client");
	// Camera camera_old = new Camera();
	// try {
	// camera_old = this.cameraService.getDetailCameraById(camera.getCamera_id());
	// camera_old.setLast_time(camera.getLast_time());
	// camera_old.setBios(camera.getBios());
	// int x = 0;
	// if (camera_old.getStatus() == 0) {
	// x += this.cameraService.updateStatusCamera(camera_old);
	// }
	// if (x == 1) {
	// logger.info("Update bios 200: " + camera.getBios());
	// return "{\"result\": true}";
	// } else {
	// logger.info("update false bios 500");
	// return "{\"result\": false}";
	// }
	// } catch (Exception e) {
	// logger.error("update bios false", e);
	// return "{\"result\": false}";
	// }
	// }
	//
	@RequestMapping(value = "/camera/capture", method = RequestMethod.POST, produces = "application/json", consumes = "multipart/form-data")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public String saveCapture(@RequestParam("image") MultipartFile file, @RequestParam("metadata") String strCapture) {
		logger.info(String.format("Receive data capture in client, metadata: %s", strCapture));
		logger.debug("File" + file.getName());
		Capture capture;
		// Camera camera;
		try {
			capture = parseDataCapture(strCapture);
			if (capture == null) {
				logger.info("Not info capture");
				return "{\"result\":false}";
			}
			// camera = this.cameraService.getDetailCameraById(capture.getCamera_id());
			// if (capture.getBios().equals(camera.getBios()) == true) {
			String strShop = Integer.toString(capture.getShop_id());
			for (int i = 0; i < (5 - Integer.toString(capture.getShop_id()).length()); i++) {
				strShop = "0" + strShop;
			}

			String strCam = Integer.toString(capture.getCamera_id());
			for (int i = 0; i < (5 - Integer.toString(capture.getCamera_id()).length()); i++) {
				strCam = "0" + strCam;
			}

			DateFormat df = new SimpleDateFormat("yyyy_MM_dd");
			String strDate = df.format(capture.getDate());
			byte[] bytes = file.getBytes();
			String rootPath = env.getProperty("fileUrl");
			String linkFile = rootPath + File.separator + strShop + File.separator + strCam
					+ File.separator + strDate;
			linkFile = linkFile.replaceAll("/+", "/");
			File dir = new File(linkFile);
			if (!dir.exists()) {
				dir.mkdirs();
			}
			Long lDate = capture.getDate().getTime();
			String name;
			name = lDate.toString() + "." + file.getOriginalFilename().split("\\.")[1];
			File serverFile = new File(dir.getAbsolutePath() + File.separator + name);
			BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
			stream.write(bytes);
			stream.close();

			logger.info("save capture: " + dir.getPath());

			return "{\"result\":true}";
			// } else {
			// logger.error("save capture not bios");
			// return "false";
			// }
		} catch (Exception e) {
			logger.error("save false capture: ", e);
			return "{\"result\":false}";
		}
	}

	public Capture parseDataCapture(String strJson) {
		JSONParser parser = new JSONParser();
		Capture capture = new Capture();
		try {
			Object obj = parser.parse(strJson);
			JSONObject jsonObject = new JSONObject();
			jsonObject = (JSONObject) obj;
			// capture.setBios((String) jsonObject.get("bios"));
			capture.setShop_id(((Long) jsonObject.get("shop_id")).intValue());
			capture.setCamera_id(((Long) jsonObject.get("camera_id")).intValue());
			capture.setDate(parseTimeUnix((String) jsonObject.get("date")));
			logger.info(String.format("info capture:%s", capture.toString()));
			return capture;
		} catch (Exception e) {
			logger.error(String.format("not info capture:%s. Exception: %s", strJson, e));
			return null;
		}
	}

	public Date parseTimeUnix(String strTime) {
		try {
			long fDate = Long.parseLong(strTime) * 1000;
			Date date = new Date(fDate);
			logger.debug("convert time:" + date.toString());
			return date;
		} catch (Exception e) {
			logger.error("convert time false: " + strTime);
			return null;
		}
	}
}
