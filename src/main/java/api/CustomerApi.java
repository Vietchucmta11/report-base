package api;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import helper.UrlHelper;
import model.Customer;
import service.CustomerService;

@RestController
@Component
@ComponentScan("serviceimpl")
@PropertySource("classpath:application.properties")
public class CustomerApi {

	final static Logger logger = Logger.getLogger(CustomerApi.class);

	@Value("${photo.url.prefix}")
	private String prefixPhotoUrl;

	UrlHelper helper = UrlHelper.getSingleTon();

	@Autowired
	private CustomerService customerService;

	@RequestMapping(value = "/customer/{id}", method = RequestMethod.GET, produces = "application/json")
	// @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public Customer getCustomerById(@PathVariable("id") int id) {
		try {
			Customer customer = new Customer();
			customer = helper.toSimplePath(customerService.getCustomerById(id), prefixPhotoUrl);
			logger.info(String.format("Detal customer by Id:%d result: %s", id, customer.toString()));
			return customer;
		} catch (Exception e) {
			logger.error(String.format("Get detail customer by id: %d false", id));
			return null;
		}
	}

	public String minusTwoDateTime(Date date1, Date date2) {
		long diff = (date2.getTime() - date1.getTime()) / 1000;
		// long h=diff/3600;
		// long m=(diff-h*3600)/60;
		// long s=diff-h*3600-m*60;
		// return Long.toString(h)+":"+Long.toString(m)+":"+Long.toString(s);
		return Long.toString(diff);
	}

	@RequestMapping(value = "/customer/insert", method = RequestMethod.POST, produces = "application/json")
	// @ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public String postCustomer(@RequestBody Customer customer) {
		logger.info("Received data from deeplearning server!");
		logger.debug("Customer info: " + customer.toString());
		Date dateReport = new Date();
		Date dateDeep = customer.getDatetime_new();
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss dd-MM-yyyy");
		logger.info(String.format("Camera: %s, Report: %s. Diff: %s (s)", formatter.format(dateDeep),
				formatter.format(dateReport), minusTwoDateTime(dateDeep, dateReport)));
		Customer customer_old;// = new Customer()
		try {
			int insertStatus;// = 0;
			customer_old = this.customerService.getCustomerByTracking_New(customer.getShop_id(),
					customer.getTracking_old());
			if (customer_old != null && customer.getSimilarity() > 70) {
				customer.setName(customer_old.getName());
				logger.info("Insert customer with Similarity >70 and tracking_old");
			} else {
				logger.info("Insert customer not tracking old");
			}

			customer.setStatus(0);
			insertStatus = this.customerService.insertCustomer(customer);

			if (insertStatus == 1) {
				logger.info(String.format("Insert customer:%s result:200 ", customer.toString()));
				return "{\"result\":" + HttpStatus.OK + "}";
			} else {
				// 409
				logger.error("Insert customer : false 500");
				return "{\"result\":"+HttpStatus.CONFLICT+"}";
			}

		} catch (Exception e) {
			// 500
			logger.error(String.format("Insert customer:%s result: 500. Exception: %s", customer.toString(), e));
			return "{\"result\":"+HttpStatus.INTERNAL_SERVER_ERROR+"}";
		}

	}

	@RequestMapping(value = "/customer/update/{id}", method = RequestMethod.PUT, produces = "application/json")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public int updateCustomer(@PathVariable("id") int id, @RequestBody Customer customer) {
		try {
			int x = this.customerService.updateCustomer(id, customer);
			if (x == 1) {
				return 200;
			} else {
				return 500;
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return 500;
		}

	}

}
