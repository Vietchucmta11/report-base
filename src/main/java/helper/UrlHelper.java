package helper;

import java.util.List;

import org.jboss.logging.Logger;
import model.Customer;
import model.Trash;

//@Component
//@PropertySource("classpath:application.properties")
public class UrlHelper {

	final static Logger logger = Logger.getLogger(UrlHelper.class);

	// @Autowired
	// private Environment env;

	// @Value("${photo.url.prefix}")
	// private String prefixPhotoUrl;

	public UrlHelper() {
		// this.prefixPhotoUrl = env.getProperty("photo.url.prefix");
		// this.prefixPhotoUrl = "http://192.168.0.178/datasection/sources/images";
	}

	private static UrlHelper instance;

	public static UrlHelper getSingleTon() {
		if (instance == null) {
			instance = new UrlHelper();
		}

		return instance;
	}

	public String generatePhotoUrl(String prefix, String path) {
		String concat;
//		if (path.startsWith("http")) {
//			path = path.replaceAll("/+", "/");
//			String[] split = path.split("/", 6);
//			path = split[5];
//		}

		concat = prefix + "/" + path;

		concat = concat.replaceAll("/+", "/").replaceAll(":/", "://");

		// logger.info("ABCDEF: ." + concat);
		return concat;
	}

	public List<Customer> toFullPath(List<Customer> customers, String prefixPhotoUrl) {
		for (Customer customer : customers) {
			String image_new = customer.getImage_new();
			String generatePhotoUrl = generatePhotoUrl(prefixPhotoUrl, image_new);
			customer.setImage_new(generatePhotoUrl);

			String image_old = customer.getImage_old();
			generatePhotoUrl = generatePhotoUrl(prefixPhotoUrl, image_old);
			customer.setImage_old(generatePhotoUrl);
		}

		return customers;
	}
	
	public List<Trash> toFullPathTrash(List<Trash> trashs, String prefixPhotoUrl) {
		for (Trash trash : trashs) {
			String image_new = trash.getImage_new();
			String generatePhotoUrl = generatePhotoUrl(prefixPhotoUrl, image_new);
			trash.setImage_new(generatePhotoUrl);

			String image_old = trash.getImage_old();
			generatePhotoUrl = generatePhotoUrl(prefixPhotoUrl, image_old);
			trash.setImage_old(generatePhotoUrl);
		}

		return trashs;
	} 
	
	
	public Customer toSimplePath(Customer customer, String prefixPhotoUrl) {
		String image_new = customer.getImage_new();
		String generatePhotoUrl = generatePhotoUrl(prefixPhotoUrl, image_new);
		customer.setImage_new(generatePhotoUrl);

		String image_old = customer.getImage_old();
		generatePhotoUrl = generatePhotoUrl(prefixPhotoUrl, image_old);
		customer.setImage_old(generatePhotoUrl);

		return customer;
	}

}
