package daoimpl;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import dao.AuthoritiesDao;
import model.Authorities;

@Repository
@Transactional
public class AuthoritiesDaoImpl implements AuthoritiesDao{

	final static Logger logger= Logger.getLogger(AuthoritiesDaoImpl.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public int insertAnthorities(Authorities authorities) {
		//Session session= sessionFactory.getCurrentSession();
		try {
			sessionFactory.getCurrentSession().save(authorities);
			logger.debug(String.format("Create authority:%s",authorities.toString()));
			return 1;
		} catch (Exception e) {
			logger.error(String.format("Create authority:%s. Exception: %s",authorities.toString(),e));
			return 0;
		}		
	}

	@Override
	public Authorities getDetailAuthorities(String username) {
		Session session= sessionFactory.getCurrentSession();
		Authorities authorities= new Authorities();
		try {
			String sql="SELECT c FROM Authorities c where c.username='"+username+"'";
			@SuppressWarnings("unchecked")
			Query<Authorities> q=session.createQuery(sql);
			authorities=q.getSingleResult();
			logger.debug(String.format("username have authority: %s",authorities.toString()));
			return authorities;
		} catch (Exception e) {
			logger.error(String.format("get false authority of username: %s. Exception: %s",username,e));
			return null;
		}
	}

	@Override
	public int updateAuthorities(Authorities authorities) {
		Session session= sessionFactory.getCurrentSession();
		try {
			session.update(authorities);
			logger.debug(String.format("update authority:%s",authorities.toString()));
			return 1;
		} catch (Exception e) {
			logger.error(String.format("update authority false:%s. Exception: %s",authorities.toString(),e));
			return 0;
		}
		
	}

}
