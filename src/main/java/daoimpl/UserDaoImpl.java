package daoimpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import dao.UserDao;
import model.User;

@Repository
@Transactional
public class UserDaoImpl implements UserDao {
	final static Logger logger = Logger.getLogger(UserDaoImpl.class);
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public User findUserByUsername(String username) {
		User user = new User();

		try {
			user = sessionFactory.getCurrentSession().get(User.class, username);
			logger.debug(String.format("Find User By username:%s have data:%s", username, user.toString()));
			return user;
		} catch (Exception e) {
			logger.info(String.format("Find user by username: %s false. Exception: %s ", username, e));
			return null;
		}
	}

	@Override
	public List<User> getListUsers() {
		List<User> users = new ArrayList<User>();
		Session session = sessionFactory.getCurrentSession();
		try {
			String sql = "SELECT c FROM User c";
			@SuppressWarnings("unchecked")
			Query<User> q = session.createQuery(sql);
			users = q.getResultList();
			logger.debug("Get list user have size: " + users.size());
			return users;
		} catch (Exception e) {
			logger.error(String.format("Not data list user, Exception: %s", e));
			return null;
		}

	}

	@Override
	public boolean insertUser(User user) {
		try {
			sessionFactory.getCurrentSession().save(user);
			logger.debug(String.format("Create user have data: %s", user.toString()));
			return true;
		} catch (Exception e) {
			logger.error(String.format("Create false user: %s, Exception:%s ", user.toString(), e));
			return false;
		}
	}

	@Override
	public boolean updateUser(User user) {
		try {
			sessionFactory.getCurrentSession().update(user);
			logger.debug(String.format("Update user: %s", user.toString()));
			return true;
		} catch (Exception e) {
			logger.error(String.format("Update user: %s false. Exception:%s", user.toString(),e));
			return false;
		}
	}

}
