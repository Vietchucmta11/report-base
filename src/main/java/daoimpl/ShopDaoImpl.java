package daoimpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import dao.ShopDao;
import model.Shop;

@Repository
@Transactional
public class ShopDaoImpl implements ShopDao {

	final static Logger logger = Logger.getLogger(ShopDaoImpl.class);

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Shop getDetailShopById(int shop_id) {
		Shop shop = new Shop();
		try {
			shop = sessionFactory.getCurrentSession().get(Shop.class, shop_id);
			logger.debug("Get detal shop_id: " + shop_id);
			return shop;
		} catch (Exception e) {
			logger.error(String.format("Get data by shod_id=%d. Exception: %s ", shop_id, e));
			return null;
		}
	}

	@Override
	public List<Shop> getListShop() {
		List<Shop> shops = new ArrayList<Shop>();
		Session session = sessionFactory.getCurrentSession();
		try {
			String sql = "SELECT c FROM Shop c";
			@SuppressWarnings("unchecked")
			Query<Shop> q = session.createQuery(sql);
			shops = q.getResultList();
			logger.debug("Get list shop");
			return shops;
		} catch (Exception e) {
			logger.error("get data false", e);
			return null;
		}

	}

	@Override
	public int insertShop(Shop shop) {
		try {
			sessionFactory.getCurrentSession().save(shop);
			logger.debug(String.format("Create new shop:%s ", shop.toString()));
			return 200;
		} catch (Exception e) {
			logger.error(String.format("Create false new shop:%s. Exception: %s", shop.toString(), e));
			return 500;
		}
	}

	@Override
	public int updateShop(Shop shop) {
		try {
			sessionFactory.getCurrentSession().update(shop);
			logger.debug(String.format("Update shop_id=%d, data: %s ", shop.getShop_id(), shop.toString()));
			return 200;
		} catch (Exception e) {
			logger.error(String.format("Update false shop_id= %d, data=%s. Exception: %s ", shop.getShop_id(),
					shop.toString(), e));
			return 500;
		}
	}

}
