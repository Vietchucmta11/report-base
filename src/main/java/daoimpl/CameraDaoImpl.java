package daoimpl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import dao.CameraDao;
import model.Camera;

@Repository
@Transactional
public class CameraDaoImpl implements CameraDao {

	final static Logger logger = Logger.getLogger(CameraDaoImpl.class);

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public Camera getDetailCameraById(int camera_id) {
		Camera camera = new Camera();
		Session session = sessionFactory.getCurrentSession();
		try {
			camera = session.get(Camera.class, camera_id);
			logger.debug(String.format("Get detail have camera_id= %d and data= %s  ", camera_id, camera.toString()));
			return camera;
		} catch (Exception e) {
			logger.error(String.format("gest detail camera by id:%d false. Exception:%s", camera_id, e));
			return null;
		}
	}

	@Override
	public List<Camera> getListCameraByShopId(int shop_id) {
		List<Camera> cameras = new ArrayList<Camera>();
		Session session = sessionFactory.getCurrentSession();
		try {
			String sql = "SELECT c FROM Camera c where c.shop_id=" + shop_id + "";
			@SuppressWarnings("unchecked")
			Query<Camera> q = session.createQuery(sql);
			cameras = q.getResultList();
			logger.debug(String.format("Get list camera by shop_id=%d, size=%s  ", shop_id, cameras.size()));
			return cameras;
		} catch (Exception e) {
			logger.error(String.format("Get false list camera by shop_id: %d", e));
			return null;
		}
	}

	@Override
	public boolean updateStatusCamera(Camera camera) {
		Session session = sessionFactory.getCurrentSession();
		try {
			session.update(camera);
			logger.debug(String.format("Update camera: %s", camera.toString()));
			return true;
		} catch (Exception e) {
			logger.error(String.format("Update data camera false: %s. Exception: %s", camera.toString(), e));
			return false;
		}
	}

	@Override
	public int insertCamera(Camera camera) {
		try {
			sessionFactory.getCurrentSession().save(camera);
			logger.debug(String.format("create new camera:%s of shop_id=%d ", camera.toString(), camera.getShop_id()));
			return 200;
		} catch (Exception e) {
			logger.error(String.format("Create new camera false: %s, Exception: %s", camera.toString(), e));
			return 500;
		}
	}

	@Override
	public List<Camera> getListAllCamera() {
		List<Camera> cameras = new ArrayList<Camera>();
		Session session = sessionFactory.getCurrentSession();
		try {
			String sql = "SELECT c FROM Camera c";
			@SuppressWarnings("unchecked")
			Query<Camera> q = session.createQuery(sql);
			cameras = q.getResultList();
			logger.debug("Get all list camera");
			return cameras;
		} catch (Exception e) {
			logger.error("get false all list camera, Exception: ", e);
			return null;
		}
	}

}
