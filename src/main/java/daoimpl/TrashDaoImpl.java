package daoimpl;

import java.util.ArrayList;
import java.util.List;


import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import dao.TrashDao;
import model.Trash;

@Repository
@Transactional
public class TrashDaoImpl implements TrashDao {

	final static Logger logger = Logger.getLogger(CustomerDaoImpl.class);

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<Trash> getListTrashByShop(int shop_id) {
		List<Trash> undos = new ArrayList<Trash>();
		Session session = sessionFactory.getCurrentSession();
		try {
			String sql = "SELECT c FROM Trash c where c.shop_id=" + shop_id + " ORDER BY c.datedel DESC";
			@SuppressWarnings("unchecked")
			Query<Trash> q = session.createQuery(sql);
			undos = q.getResultList();
			logger.debug("Get list trash of shop_id " + shop_id);

			return undos;
		} catch (Exception e) {
			logger.error("Get list trash of shop_id " + shop_id + "False: ", e);
			return null;
		}
	}

	@Override
	public boolean deleteTrash(int id) {
		Session session = sessionFactory.getCurrentSession();
		try {
			Trash customer = session.load(Trash.class, id);
			session.delete(customer);
			logger.debug(String.format("delate undo by id=%d", id));
			return true;
		} catch (Exception e) {
			logger.error(String.format("Delete undo by if= %d false. Exception: %s", id, e));
			return false;
		}
	}

	@Override
	public boolean insertTrash(Trash undo) {
		try {
			sessionFactory.getCurrentSession().save(undo);
			logger.debug(String.format("Insert undo:%s ", undo.toString()));
			return true;
		} catch (Exception e) {
			logger.error(String.format("Insert undo: %s false. Exception:%s ", undo.toString(), e));
			return false;
		}
	}

	@Override
	public Trash getTrashById(int id) {
		Trash trash= new Trash();
		try {
			trash = sessionFactory.getCurrentSession().get(Trash.class, id);
			logger.debug(String.format("Get trash by Id=%d result data: %s ", id, trash.toString()));
			return trash;
		} catch (Exception e) {
			logger.error(String.format("Get trash by Id=%d false. Exception: %s", e));
			return null;
		}
	}
}
