package daoimpl;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import dao.ShopUserDao;
import model.ShopUser;

@Repository
@Transactional
public class ShopUserDaoImpl implements ShopUserDao {
	final static Logger logger = Logger.getLogger(ShopUserDaoImpl.class);
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public ShopUser getShopUserByUsername(String username) {
		ShopUser shopUser = new ShopUser();
		Session session = sessionFactory.getCurrentSession();
		try {
			String sql = "SELECT c FROM ShopUser c WHERE c.username='" + username + "'";
			@SuppressWarnings("unchecked")
			Query<ShopUser> q = session.createQuery(sql);
			shopUser = q.getSingleResult();
			logger.debug(String.format("Get shopUser by username:%s  have data: %s", username, shopUser.toString()));
			return shopUser;
		} catch (Exception e) {
			logger.error(String.format("Get shopUser by username=%s false. Exception: %s", username, e));
			return null;
		}
	}

	@Override
	public ShopUser getShopUserByShopId(int shop_id) {
		ShopUser shopUser = new ShopUser();
		Session session = sessionFactory.getCurrentSession();
		try {
			String sql = "SELECT c FROM ShopUser c WHERE c.shop_id=" + shop_id + "";
			@SuppressWarnings("unchecked")
			Query<ShopUser> q = session.createQuery(sql);
			shopUser = q.getSingleResult();
			logger.debug("Get shopUser by shop_id: " + shop_id);
			return shopUser;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("False: ", e);
			return null;
		}
	}

	@Override
	public int insertShopUser(ShopUser shopUser) {
		// Session session= sessionFactory.getCurrentSession();
		try {
			sessionFactory.getCurrentSession().save(shopUser);
			logger.debug(String.format("Create shopUser have data: %s",shopUser.toString()));
			return 1;
		} catch (Exception e) {
			logger.error(String.format("Create shopUser have data: %s false. Exception: %s",shopUser.toString(), e));
			return 0;
		}
	}

	@Override
	public int updateShopUser(ShopUser shopUser) {
		try {
			sessionFactory.getCurrentSession().update(shopUser);
			logger.debug(String.format("Update shopUser have data; %s", shopUser.toString()));
			return 1;
		} catch (Exception e) {
			logger.error(String.format("Update shopUser data; %s false. Exception:%s",shopUser.toString() ,e));
			return 0;
		}
	}

}
