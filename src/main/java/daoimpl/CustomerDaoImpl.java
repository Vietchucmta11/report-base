package daoimpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import dao.CustomerDao;
import model.Customer;

@Repository
@Component
@Transactional
@ComponentScan("daoimpl.*")
@PropertySource("classpath:application.properties")
public class CustomerDaoImpl implements CustomerDao {

	final static Logger logger = Logger.getLogger(CustomerDaoImpl.class);

	@Autowired
	private SessionFactory sessionFactory;

	@Value("${customer.update_info.similarity.min}")
	private String sMin_similarity;

	// @Autowired
	// private Environment env;

	@Autowired
	private CustomerDao customerDao;

	@Override
	public int insertCustomer(Customer customer) {
		try {
			sessionFactory.getCurrentSession().save(customer);
			logger.debug(String.format("Insert customer:%s ", customer.toString()));
			return 1;
		} catch (Exception e) {
			logger.error(String.format("Insert customer: %s false. Exception:%s ", customer.toString(), e));
			return 0;
		}
	}

	@Override
	public Customer getCustomerById(int id) {
		Customer customer;
		try {
			customer = sessionFactory.getCurrentSession().get(Customer.class,id);
			logger.debug(String.format("Get customer by Id=%d result data: %s ", id, customer.toString()));
			return customer;
		} catch (Exception e) {
			logger.error(String.format("Get customer by Id=%d false. Exception: %s", e));
			return null;
		}
	}

	@Override
	public int updateCustomer(int id, Customer customer) {
		Customer customer_old = new Customer();
		Session session = sessionFactory.getCurrentSession();
		try {
			customer_old = session.byId(Customer.class).load(id);
			customer_old.setCamera_id(customer.getCamera_id());
			customer_old.setShop_id(customer.getShop_id());
			customer_old.setTracking_new(customer.getTracking_new());
			customer_old.setAge(customer.getAge());
			customer_old.setEmotion(customer.getEmotion());
			customer_old.setGender(customer.getGender());
			customer_old.setSimilarity(customer.getSimilarity());
			customer_old.setTracking_old(customer.getTracking_old());
			customer_old.setImage_new(customer.getImage_new());
			customer_old.setDatetime_new(customer.getDatetime_new());
			customer_old.setImage_old(customer.getImage_old());
			customer_old.setDatetime_old(customer.getDatetime_old());
			customer_old.setName(customer.getName());
			customer_old.setStatus(customer.getStatus());
			session.flush();
			logger.info("update customer: " + customer_old.getId());
			return 1;
		} catch (Exception e) {
			logger.error("update customer false ", e);
			return 0;
		}
	}

	@Override
	public List<Customer> getListCustomerByShop(int shop_id) {
		List<Customer> customers = new ArrayList<Customer>();
		Session session = sessionFactory.getCurrentSession();
		try {
			CriteriaBuilder buider = session.getCriteriaBuilder();
			CriteriaQuery<Customer> query = buider.createQuery(Customer.class);
			Root<Customer> root = query.from(Customer.class);
			query.select(root).where(buider.equal(root.get("shop_id"), shop_id));
			query.orderBy(buider.desc(root.get("id")));
			Query<Customer> q = session.createQuery(query);
			customers = q.getResultList();
			logger.debug(String.format("Get list Customer by shop_id =%d result  size: %d", shop_id, customers.size()));
			return customers;
		} catch (Exception e) {
			logger.error(String.format("Get list customer by shop_id= %d false, Exception: %s", shop_id, e));
			return null;
		}
	}

	@Override
	public List<Customer> getListCustomeByShopDate(int Shop_id, String strDate) {
		List<Customer> customers = new ArrayList<Customer>();
		Session session = sessionFactory.getCurrentSession();
		try {
			String sql = "SELECT c FROM Customer c where c.shop_id=" + Shop_id + " and date(c.datetime_new) like '"
					+ strDate + "' ORDER BY c.datetime_new DESC";
			@SuppressWarnings("unchecked")
			Query<Customer> q = session.createQuery(sql);
			customers = q.getResultList();
			logger.debug(String.format("Get list customer by shop_id: %d and date: %s  result total: %d", Shop_id,
					strDate, customers.size()));
			return customers;
		} catch (Exception e) {
			logger.error(String.format("Get list false customer by shop_id=%d and date: %s. Exception: %s", Shop_id,
					strDate, e));
			return null;
		}
	}

	// update tung ban ghi
	@Override
	public int updateAnalytic(Customer customer) {
		Session session = sessionFactory.getCurrentSession();
		try {
			session.update(customer);
			logger.debug(String.format("Updated customer: %s", customer.toString()));
			return 1;
		} catch (Exception e) {
			logger.error(String.format("Updated customer: %s \n Exception: %s", customer.toString(), e));
			return 0;
		}
	}

	// lay danh sach customer theo shop_id, camera_id, thep ngay
	@Override
	public List<Customer> getListCustomerByShopCameraDate(int shop_id, int camera_id, String strDate) {
		List<Customer> customers = new ArrayList<Customer>();
		Session session = sessionFactory.getCurrentSession();
		try {
			String sql = "SELECT c FROM Customer c where c.shop_id=" + shop_id + "and c.camera_id=" + camera_id
					+ " and date(c.datetime_new) like '" + strDate + "' ORDER BY c.datetime_new DESC";
			@SuppressWarnings("unchecked")
			Query<Customer> q = session.createQuery(sql);
			customers = q.getResultList();
			logger.debug(String.format("Get list customer by shop_id=%d, camera_id=%d and date= %s have size= %d",
					shop_id, camera_id, strDate, customers.size()));
			return customers;
		} catch (Exception e) {
			logger.error(
					String.format("Get false list customer by shop_id=%d, camera_id=%d and date= %s. Exception: %s",
							shop_id, camera_id, strDate, e));
			return null;
		}
	}

	// lay danh sach customer theo shop_id and tracking_old
	@Override
	public List<Customer> getListCustomerbyTrackingOld(int shop_id, String tracking_old) {
		List<Customer> customers = new ArrayList<Customer>();
		Session session = sessionFactory.getCurrentSession();
		try {
			String sql = "SELECT c FROM Customer c where c.shop_id=" + shop_id + "and c.tracking_old='" + tracking_old
					+ "'";
			@SuppressWarnings("unchecked")
			Query<Customer> q = session.createQuery(sql);
			customers = q.getResultList();
			logger.debug(String.format("Get list customer by shop_id=%d and tracking_old=%s  ", shop_id, tracking_old));
			return customers;
		} catch (Exception e) {
			logger.error(String.format("Get list customer by shop_id=%d and tracking_old=%s false. Exceptiong: %s",
					shop_id, tracking_old, e));
			return null;
		}
	}

	@Override
	public boolean updateListAnalytic(Customer customer) {
		List<Customer> relatedCustomers = new ArrayList<Customer>();
		try {
			/**
			 * Update for current record
			 */
			logger.info("Similarity: " + sMin_similarity);
			int iMin_similarity;
			try {
				iMin_similarity = Integer.parseInt(sMin_similarity);
			} catch (Exception e) {
				logger.error("customer.update.info.similarity.min in file application.properties not number");
				iMin_similarity = 100;
			}

			customerDao.updateAnalytic(customer);
			// customerDao.updateCustomer(customer.getId(), customer);

			logger.debug("sucess update update customer id: " + customer.getId());

			if (customer.getStatus() == 1) {
				relatedCustomers.addAll(
						customerDao.getListCustomerbyTrackingOld(customer.getShop_id(), customer.getTracking_old()));
				relatedCustomers.addAll(
						customerDao.getListCustomerbyTrackingOld(customer.getShop_id(), customer.getTracking_new()));
				relatedCustomers
						.add(customerDao.getCustomerByTracking_New(customer.getShop_id(), customer.getTracking_old()));
			} else if (customer.getStatus() == -1) {
				relatedCustomers.addAll(
						customerDao.getListCustomerbyTrackingOld(customer.getShop_id(), customer.getTracking_new()));
			}

			if (relatedCustomers.size() > 0) {
				for (Customer relatedCustomer : relatedCustomers) {
					if (relatedCustomer.getSimilarity() > iMin_similarity && relatedCustomer.getStatus() == 0) {
						relatedCustomer.setAge(customer.getAge());
						relatedCustomer.setGender(customer.getGender());
						relatedCustomer.setName(customer.getName());
						// relatedCustomer.setStatus(0);
						customerDao.updateAnalytic(relatedCustomer);
						logger.debug(String.format(
								"Update information for related customer_id: %d,"
										+ " with tracking_old=%s, tracking_new=%s, json=%s",
								relatedCustomer.getId(), relatedCustomer.getTracking_old(),
								relatedCustomer.getTracking_new(), relatedCustomer.toString()));
					} else {
						logger.debug(String.format(
								"Ignore to update because status != 1 or similarity < MIN_SIMILARITY, json=%s",
								relatedCustomer.toString()));
					}
				}

				logger.info(String.format("Success updated %d related customer of customer_id: %d",
						relatedCustomers.size(), customer.getId()));
			}

			return true;
		} catch (Exception e) {
			logger.error("update false: ", e);
			return false;
		}
	}

	@Override
	public Customer getCustomerByTracking_New(int shop_id, String tracking_new) {
		Customer customer = new Customer();
		Session session = sessionFactory.getCurrentSession();
		try {
			String sql = "SELECT c FROM Customer c where c.shop_id=" + shop_id + " and c.tracking_new='" + tracking_new
					+ "'";
			@SuppressWarnings("unchecked")
			Query<Customer> q = session.createQuery(sql);
			customer = q.getSingleResult();
			logger.info(String.format("Updated customer with shop_id=%d, tracking_new=%s", shop_id, tracking_new));

			return customer;
		} catch (Exception e) {
			logger.error("Get customer by tracking_new and shop_id: " + shop_id + "-" + tracking_new + "False", e);

			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Customer> getListCustomerDateToDate(int shop_id, String startDate, String endDate) {
		List<Customer> customers = new ArrayList<Customer>();
		Session session = sessionFactory.getCurrentSession();
		try {
			String sql = "SELECT c FROM Customer c WHERE c.shop_id=" + shop_id + " and (date(c.datetime_new)  BETWEEN '"
					+ startDate + "' AND '" + endDate + "')";
			Query<Customer> q = session.createQuery(sql);
			customers = q.getResultList();
			logger.debug("Get list customer of shop_id " + shop_id + " by date to date:" + startDate + "/" + endDate);

			return customers;
		} catch (Exception e) {
			logger.error("Get list customer of shop_id " + shop_id + " by date to date:" + startDate + "/" + endDate
					+ "False: ", e);
			return null;
		}
	}

	@Override
	public List<Customer> getListCustomerDate(int shop_id, String strDate) {
		List<Customer> customers = new ArrayList<Customer>();
		Session session = sessionFactory.getCurrentSession();
		try {
			String sql = "SELECT c FROM Customer c WHERE c.shop_id=" + shop_id + " and date(c.datetime_new)='" + strDate
					+ "'";
			@SuppressWarnings("unchecked")
			Query<Customer> q = session.createQuery(sql);
			customers = q.getResultList();
			logger.debug("get list customer of shop_id " + shop_id + " by date: " + strDate);

			return customers;
		} catch (Exception e) {
			logger.error("get false list customer of shop_id: " + shop_id + " by date: " + strDate);

			return null;
		}
	}

	@Override
	public boolean deleteCustomerById(int id) {
		Session session = sessionFactory.getCurrentSession();
		try {
			Customer customer = session.load(Customer.class, id);
			session.delete(customer);
			logger.debug(String.format("delate customer by id=%d", id));
			return true;
		} catch (Exception e) {
			logger.error(String.format("Delete customer by if= %d false. Exception: %s", id, e));
			return false;
		}
	}

	@Override
	public boolean undoCustomer(Customer customer) {
		try {
			sessionFactory.getCurrentSession().update(customer);
			logger.debug(String.format("undo customer:%s ", customer.toString()));
			return true;
		} catch (Exception e) {
			logger.error(String.format("undo customer: %s false. Exception:%s ", customer.toString(), e));
			return false;
		}
	}

}
