package controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import model.Camera;
import model.Customer;
import model.ShopUser;
import model.Trash;
import service.CameraService;
import service.CustomerService;
import service.ShopUserService;
import service.TrashService;

@Controller
@ComponentScan("serviceimpl.*")
@PropertySource("classpath:application.properties")
public class CustomerController {

	final static Logger logger = Logger.getLogger(CustomerController.class);

	@Autowired
	private Environment env;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private ShopUserService shopUserService;

	@Autowired
	private CameraService cameraService;

	@Autowired
	private TrashService trashService;

	// view home
	@RequestMapping(value = "/user/home", method = RequestMethod.GET)
	public String homeUser(ModelMap mm, Principal principal) {
		ShopUser shopUser = new ShopUser();
		try {
			shopUser = this.shopUserService.getShopUserByUsername(principal.getName());
			if (shopUser != null) {
				// mm.put("idTable", "listAnalyticHome");
				logger.info(String.format("%s seen view page with shop_id: %d ", principal.getName(),
						shopUser.getShop_id()));
				return "listAnalytic";
			} else {
				mm.put("message", "Bạn không có quyền!!!");
				return "404";
			}
		} catch (Exception e) {
			mm.put("message", "Lỗi!!!");
			logger.error(String.format("%s seen view page with shop_id: %d false. Exception: %s", principal.getName(),
					shopUser.getShop_id(), e));
			return "404";
		}
	}

	// json ket qua phan tich view Home
	@RequestMapping(value = "/user/home/date={strDate}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<Customer> listAnalyticViewHome(@PathVariable(value = "strDate") String strDate, Principal principal) {
		List<Customer> customers = new ArrayList<Customer>();
		ShopUser shopUser = new ShopUser();
		try {
			shopUser = this.shopUserService.getShopUserByUsername(principal.getName());
			if (shopUser != null) {
				customers = this.customerService.getListCustomeByShopDate(shopUser.getShop_id(), strDate);
				logger.info(String.format("%s get data Customer of shop_id: %d on Date:%s", principal.getName(),
						shopUser.getShop_id(), strDate));
				return customers;
			} else {
				logger.error("Data null");
				return null;
			}
		} catch (Exception e) {
			logger.error(String.format("%s get data Customer of shop_id: %d on Date:%s false. Exception: %s",
					principal.getName(), shopUser.getShop_id(), strDate, e));
			return null;
		}
	}

	// view list analytic by shop_id
	@RequestMapping(value = "/user/shop={id}", method = RequestMethod.GET)
	public String listAnalytic(ModelMap mm, @PathVariable(value = "id") int id, Principal principal) {
		ShopUser shopUser = new ShopUser();
		try {
			shopUser = this.shopUserService.getShopUserByUsername(principal.getName());
			if (shopUser.getShop_id() == id) {
				mm.put("shopId", id);
				// mm.put("idTable", "listAnalyticByShop");
				logger.info(String.format(" %s seen view list analyitc with shop_id: %d ", principal.getName(),
						shopUser.getShop_id()));
				return "listAnalytic";
			} else {
				mm.put("message", "Bạn không có quyền!!!");
				logger.error("Not page: " + id);
				return "404";
			}
		} catch (Exception e) {
			mm.put("message", "Lỗi!!!");
			logger.error(String.format(" %s seen view list analyitc with shop_id: %d false. Exception: %s",
					principal.getName(), shopUser.getShop_id(), e));
			return "404";
		}
	}

	// ket qua Json cua shop theo ngay
	@RequestMapping(value = "/user/shop={id}/date={strDate}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<Customer> listAnalyticByDate(@PathVariable(value = "id") int id,
			@PathVariable(value = "strDate") String strDate, Principal principal) {
		List<Customer> customers = new ArrayList<Customer>();
		ShopUser shopUser = new ShopUser();
		try {
			shopUser = this.shopUserService.getShopUserByUsername(principal.getName());
			if (shopUser.getShop_id() == id) {
				customers = this.customerService.getListCustomeByShopDate(id, strDate);
				logger.info(String.format("%s get list data Json analytic with shop_id:%d and date: %s",
						principal.getName(), shopUser.getShop_id(), strDate));
				return customers;
			} else {
				logger.info("Date null");
				return null;
			}
		} catch (Exception e) {
			logger.error(
					String.format("%s get list data Json analytic with shop_id:%d and date: %s false. Exception: %s",
							principal.getName(), shopUser.getShop_id(), strDate, e));
			return null;
		}
	}

	// view list analytic by shop_id and camera_id
	@RequestMapping(value = "/user/shop_camera={str}", method = RequestMethod.GET)
	public String listAnalyticByCameraId(@PathVariable(value = "str") String str, ModelMap mm, Principal principal) {
		String[] chuoi = str.split("\\-");
		int shop_id = Integer.parseInt(chuoi[0]);
		int camera_id = Integer.parseInt(chuoi[1]);
		ShopUser shopUser = new ShopUser();
		List<Camera> cameras = new ArrayList<Camera>();
		try {
			shopUser = this.shopUserService.getShopUserByUsername(principal.getName());
			cameras = this.cameraService.getListCameraByShopId(shop_id);
			boolean check = false;
			for (Camera camera : cameras) {
				if (camera.getCamera_id() == camera_id) {
					check = true;
				}
			}
			if (shopUser.getShop_id() == shop_id && check == true) {
				mm.put("shopId", str);
				// mm.put("idTable", "listAnalyticByCamera");
				logger.info(String.format("%s seen view analytic with shop_id camera: %s", principal.getName(), str));
				return "listAnalytic";
			} else {
				mm.put("message", "Bạn không có quyền!!!");
				logger.error("");
				return "404";
			}
		} catch (Exception e) {
			mm.put("message", "Lỗi!!!");
			logger.error(String.format("%s seen view analytic with shop_id camera: %s false. Exception: %s",
					principal.getName(), str, e));
			return "404";
		}
	}

	// ket qua JSON theo shop_id, camera_id
	@RequestMapping(value = "/user/shop_camera={str}/date={strDate}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<Customer> listAnalyticByCameraIdAndDate(@PathVariable(value = "str") String str,
			@PathVariable(value = "strDate") String strDate, Principal principal) {
		String[] chuoi = str.split("\\-");
		int id = Integer.parseInt(chuoi[0]);
		int camera_id = Integer.parseInt(chuoi[1]);
		List<Customer> customers = new ArrayList<Customer>();
		ShopUser shopUser = new ShopUser();
		List<Camera> cameras = new ArrayList<Camera>();
		try {
			shopUser = this.shopUserService.getShopUserByUsername(principal.getName());
			cameras = this.cameraService.getListCameraByShopId(id);
			boolean check = false;
			for (Camera camera : cameras) {
				if (camera.getCamera_id() == camera_id) {
					check = true;
				}
			}
			if (shopUser.getShop_id() == id && check == true) {
				customers = this.customerService.getListCustomerByShopCameraDate(id, camera_id, strDate);
				logger.info(String.format("%s Get list analytic Json but shop_id, camera_id: %s and date: %s ",
						principal.getName(), str, strDate));
				return customers;
			} else {
				logger.error("Date null");
				return null;
			}
		} catch (Exception e) {
			logger.error(String.format(
					"%s Get list analytic Json but shop_id, camera_id: %s and date: %s false. Exception: %s",
					principal.getName(), str, strDate, e));
			return null;
		}
	}

	// update kieu post json
	@RequestMapping(value = "/user/update/image={id}", method = RequestMethod.POST, produces = "application/json")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public int updateImageJson(@RequestBody Customer customer, Principal principal) {
		logger.debug("Received update request with customer_id = " + customer.getId());
		Customer oldCustomer;// = new Customer();
		ShopUser shopUser;// = new ShopUser();
		try {
			oldCustomer = this.customerService.getCustomerById(customer.getId());
			if (oldCustomer == null) {
				logger.error("Cannot find customer_id = " + customer.getId());
				return 500;
			}

			logger.info("update customer old: " + oldCustomer.toString());
			shopUser = this.shopUserService.getShopUserByUsername(principal.getName());

			if (shopUser == null) {
				logger.error("Cannot find shop with userName = " + principal.getName());
				return 500;
			}
			/**
			 * Delete link info
			 */
			if (customer.getStatus() == -1) {
				oldCustomer.setImage_old(env.getProperty("linkImage"));
				oldCustomer.setTracking_old(null);
				oldCustomer.setDatetime_old(null);
				oldCustomer.setSimilarity((float) 0);
			}

			/**
			 * Update info
			 */
			oldCustomer.setGender(customer.getGender());
			oldCustomer.setAge(customer.getAge());
			oldCustomer.setName(customer.getName());
			oldCustomer.setStatus(customer.getStatus());
			this.customerService.updateListAnalytic(oldCustomer);
			logger.info("Success updated info for customer_id = " + customer.getId());

			return 200;

		} catch (Exception e) {
			logger.error("Fail to updated customer_id = " + customer.getId() + "Exception", e);

			return 500;
		}
	}

	@RequestMapping(value = "/user/delete/id={customer_id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public boolean deleteCustomer(@PathVariable(value = "customer_id") int id, Principal principal) {
		ShopUser shopUser;
		Customer customer;
		try {
			shopUser = this.shopUserService.getShopUserByUsername(principal.getName());
			customer = this.customerService.getCustomerById(id);
			boolean success;
			if (shopUser.getShop_id() == customer.getShop_id()) {
				success = this.customerService.deleteCustomerById(id);
			} else {
				success = false;
			}
			if (success == true) {
				Trash trash = new Trash();
				trash = convertToTrash(customer);
				trash.setDatedel(new Date());
				boolean x = trashService.insertTrash(trash);
				logger.info(principal.getName() + " delete customer by: " + id + "//" + x);
				return true;
			} else {
				logger.error(principal.getName() + " not delete customer by: " + id);
				return false;
			}
		} catch (Exception e) {
			logger.error(principal.getName() + " delete false customer by: " + id, e);
			return false;
		}
	}

	public Trash convertToTrash(Customer customer) {
		Trash trash = new Trash();
		trash.setId(customer.getId());
		trash.setCamera_id(customer.getCamera_id());
		trash.setShop_id(customer.getShop_id());
		trash.setTracking_new(customer.getTracking_new());
		trash.setAge(customer.getAge());
		trash.setGender(customer.getGender());
		trash.setEmotion(customer.getEmotion());
		trash.setImage_new(customer.getImage_new());
		trash.setDatetime_new(customer.getDatetime_new());
		trash.setTracking_old(customer.getTracking_old());
		trash.setImage_old(customer.getImage_old());
		trash.setDatetime_old(customer.getDatetime_old());
		trash.setSimilarity(customer.getSimilarity());
		trash.setName(customer.getName());
		trash.setStatus(customer.getStatus());
		return trash;
	}
}
