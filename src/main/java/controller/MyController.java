package controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import model.ShopUser;
import model.ShowCamera;
import model.Authorities;
import model.Camera;
import service.AuthoritiesService;
import service.CameraService;
import service.ShopUserService;

@Controller
@ComponentScan("serviceimpl.*")
public class MyController {
	final static Logger logger = Logger.getLogger(MyController.class);

	// view login
	@RequestMapping(value = { "/", "/login" })
	public String login() {
		logger.info("view login");
		return "login";
	}

	@Autowired
	private ShopUserService shopUserService;

	@Autowired
	private CameraService cameraService;

	// show header
	@RequestMapping(value = "/user/header", method = RequestMethod.GET)
	public String header(ModelMap mm, Principal principal) {
		ShopUser shopUser;
		List<Camera> cameras = new ArrayList<Camera>();
		try {
			shopUser = this.shopUserService.getShopUserByUsername(principal.getName());
			cameras = this.cameraService.getListCameraByShopId(shopUser.getShop_id());
			List<ShowCamera> showCameras = new ArrayList<ShowCamera>();
			for (Camera camera : cameras) {
				ShowCamera showCamera = new ShowCamera();
				long minus = 0;
				Date time_last = camera.getLast_time();
				Date time_new = new Date();
				minus = (time_new.getTime() - time_last.getTime()) / 1000;
				showCamera.setCamera_id(camera.getCamera_id());
				showCamera.setShop_id(camera.getShop_id());
				showCamera.setName(camera.getName());
				showCamera.setPlace(camera.getPlace());
				showCamera.setMinus_time((int) minus);
				showCameras.add(showCamera);
			}
			mm.put("shopId", shopUser.getShop_id());
			mm.put("listCamera", showCameras);
			mm.put("message", principal.getName());
			logger.info(principal.getName() + " join header of " + shopUser.getShop_id() + " abour camera: "
					+ showCameras.size());
			return "header";
		} catch (Exception e) {
			return "header";
		}
	}

	// show footer
	@RequestMapping(value = "/user/footer", method = RequestMethod.GET)
	public String footer(ModelMap mm) {
		return "footer";
	}

	@RequestMapping(value = "/user/statistic", method = RequestMethod.GET)
	public String chart() {
		logger.info("View chart");
		return "chart";
	}

	@RequestMapping(value = "/user/statistic_date", method = RequestMethod.GET)
	public String chartbydate() {
		return "chartbydate";
	}

	@Autowired
	private AuthoritiesService authoritiesService;

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String urlPage(Principal principal) {
		Authorities authorities;
		try {
			authorities = this.authoritiesService.getDetailAuthorities(principal.getName());
			if (authorities.getAuthority().equals("ROLE_USER") == true) {
				logger.info(String.format("View home user:%s", principal.getName()));
				return "redirect:user/home";
			} else if (authorities.getAuthority().equals("ROLE_ADMIN") == true) {
				logger.info(String.format("View home admin: %s", principal.getName()));
				return "redirect:admin/home";
			} else {
				logger.error(String.format("%s login false.", principal.getName()));
				return "redirect:login";
			}
		} catch (Exception e) {
			logger.error(String.format("%s login false. Exception:%s  ", principal.getName(), e));
			return "redirect:login";
		}
	}

}
