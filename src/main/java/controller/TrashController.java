package controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import model.Customer;
import model.ShopUser;
import model.Trash;
import service.CustomerService;
import service.ShopUserService;
import service.TrashService;

@Controller
@Component
@ComponentScan("serviceimpl.*")
public class TrashController {

	final static Logger logger = Logger.getLogger(TrashController.class);

	@Autowired
	private TrashService trashService;

	@Autowired
	private ShopUserService shopUserService;

	@Autowired
	private CustomerService customerService;

	@RequestMapping(value = "/user/trash", method = RequestMethod.GET)
	public String linktrash(Principal princial) {
		return "trash";
	}

	@RequestMapping(value = "/user/trash", method = RequestMethod.GET, produces = "application/json")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<Trash> getListUndoByDate(Principal principal) {
		List<Trash> undos = new ArrayList<Trash>();
		ShopUser shopUser;
		try {
			shopUser = this.shopUserService.getShopUserByUsername(principal.getName());
			undos = this.trashService.getListTrashByShop(shopUser.getShop_id());
			logger.info(String.format("%s get list trash of shop_id= %d", principal.getName(),
					shopUser.getShop_id()));
			return undos;
		} catch (Exception e) {
			logger.error(String.format("%s get list trash false. Exception: %s", principal.getName(), e));
			return null;
		}
	}

	@RequestMapping(value = "/user/undo/id={id}", method = RequestMethod.POST, produces = "application/json")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public boolean undoCustomer(Principal principal, @PathVariable(value = "id") int id) {
		Trash trash;
		ShopUser shopUser;
		Customer customer;
		try {
			shopUser = this.shopUserService.getShopUserByUsername(principal.getName());
			trash = this.trashService.getTrashById(id);
			boolean success = this.trashService.deleteTrash(id);
			if (success == true) {
				customer = convertToCustomer(trash);
				int x = this.customerService.insertCustomer(customer);
				if (x == 1) {
					logger.info(String.format("%s delete trash of shop_id= %d result: %d", principal.getName(),
							shopUser.getShop_id(), x));
					return true;
				} else {
					this.trashService.insertTrash(trash);
					return false;
				}
			} else {
				logger.info(String.format("%s delete trash of shop_id= %d false", principal.getName(),
						shopUser.getShop_id()));
				return false;
			}
		} catch (Exception e) {
			logger.error(String.format("%s get list trash false. Exception: %s", principal.getName(), e));
			return false;
		}
	}

	public Customer convertToCustomer(Trash trash) {
		Customer customer = new Customer();
		customer.setId(trash.getId());
		customer.setCamera_id(trash.getCamera_id());
		customer.setShop_id(trash.getShop_id());
		customer.setTracking_new(trash.getTracking_new());
		customer.setAge(trash.getAge());
		customer.setGender(trash.getGender());
		customer.setEmotion(trash.getEmotion());
		customer.setImage_new(trash.getImage_new());
		customer.setDatetime_new(trash.getDatetime_new());
		customer.setTracking_old(trash.getTracking_old());
		customer.setImage_old(trash.getImage_old());
		customer.setDatetime_old(trash.getDatetime_old());
		customer.setSimilarity(trash.getSimilarity());
		customer.setName(trash.getName());
		customer.setStatus(trash.getStatus());
		return customer;
	}

}
