package controller;

import java.util.ArrayList;
import java.util.List;
import org.jboss.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import model.Camera;
import model.Customer;
import service.CameraService;
import service.CustomerService;

@Controller
@ComponentScan("service.*")

public class CameraController {

	final static Logger logger = Logger.getLogger(CameraController.class);

	@Autowired
	private CameraService cameraService;

//	@Autowired
//	private ShopUserService shopUserService;\
	
	
	@RequestMapping(value="/test")
	public String testcheckall(ModelMap mm) {
		return "test";
	}

}
