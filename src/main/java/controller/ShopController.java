package controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import model.Shop;
import service.ShopService;

@Controller
@ComponentScan("serviceimpl.*")
public class ShopController {
	final static Logger logger= Logger.getLogger(ShopController.class);
	
	@Autowired
	private ShopService shopService;

	@RequestMapping(value="/admin/listShop",method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public List<Shop>  getListShop(){
		List<Shop> shops= new ArrayList<Shop>();
		try {
			shops=this.shopService.getListShop();
			return shops;
		} catch (Exception e) {
			logger.error("Not data json list shop",e);
			return null;
		}
		
	}
}
