package dao;

import java.util.List;

import model.User;

public interface UserDao {
	public User findUserByUsername(String username);

	public List<User> getListUsers();

	public boolean insertUser(User user);

	public boolean updateUser(User user);
}
