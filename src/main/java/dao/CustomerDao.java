package dao;

import java.util.List;

import model.Customer;

public interface CustomerDao {
	public int insertCustomer(Customer customer);

	public Customer getCustomerById(int id);

	public int updateCustomer(int id, Customer customer);

	public List<Customer> getListCustomerByShop(int shop_id);

	public List<Customer> getListCustomeByShopDate(int Shop_id, String strDate);

	public List<Customer> getListCustomerByShopCameraDate(int shop_id, int camera_id, String strDate);

	// lay list customer trong 2 khoang ngay
	public List<Customer> getListCustomerDateToDate(int shop_id, String startDate, String endDate);

	public int updateAnalytic(Customer customer);

	// get list customer theo tracking old.
	public List<Customer> getListCustomerbyTrackingOld(int shop_id, String tracking_old);

	// search customer by tracking_new
	public Customer getCustomerByTracking_New(int shop_id, String tracking_new);

	// update cac ban ghi co cung tracking_old
	public boolean updateListAnalytic(Customer customer);

	// lay list customer trong 1 ngay

	public List<Customer> getListCustomerDate(int shop_id, String strDate);

	public boolean deleteCustomerById(int id);

	public boolean undoCustomer(Customer customer);
}
