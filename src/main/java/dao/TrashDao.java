package dao;

import java.util.List;

import model.*; 

public interface TrashDao {
	public List<Trash> getListTrashByShop(int shop_id);
	
	public boolean deleteTrash(int id);
	
	public boolean insertTrash(Trash undo);
	
	public Trash getTrashById(int id);
}
