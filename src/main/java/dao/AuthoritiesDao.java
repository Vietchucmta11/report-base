package dao;

import model.Authorities;

public interface AuthoritiesDao {

	public int insertAnthorities(Authorities authorities);
	
	public Authorities getDetailAuthorities(String username);
	
	public int updateAuthorities(Authorities authorities);
}
