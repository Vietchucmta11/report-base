package dao;

import model.ShopUser;

public interface ShopUserDao {
	public ShopUser getShopUserByUsername(String username);

	public ShopUser getShopUserByShopId(int shop_id);

	public int insertShopUser(ShopUser shopUser);
	
	public int updateShopUser(ShopUser shopUser);
}
