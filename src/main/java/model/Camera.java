package model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Entity
@Table(name = "camera")
public class Camera {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "camera_id")
	private int camera_id;

	@Column(name = "shop_id")
	private int shop_id;

	@Column(name = "name")
	private String name;

	@Column(name = "place")
	private String place;

	@Column(name = "last_time")
	private Date last_time;

	// @Column(name = "bios")
	// private String bios;
	//
	// @Column(name = "status")
	// private int status;

	public Camera() {
	};

	public Camera(int camera_id, int shop_id, Date last_time) {
		this.camera_id = camera_id;
		this.shop_id = shop_id;
		// this.bios = bios;
		this.last_time = last_time;
	}

	public Camera(int shop_id, String name, String place, Date last_time) {
		this.shop_id = shop_id;
		this.name = name;
		this.place = place;
		this.last_time = last_time;
		// this.bios = bios;
		// this.status = status;
	}

	public Camera(int camera_id, int shop_id, String name, String place, Date last_time) {
		this.camera_id = camera_id;
		this.shop_id = shop_id;
		this.name = name;
		this.place = place;
		// this.bios = bios;
		// this.status = status;
	}

	public Camera(int shop_id, String name, String place) {
		this.shop_id = shop_id;
		this.name = name;
		this.place = place;
	}

	// public String getBios() {
	// return bios;
	// }
	//
	// public void setBios(String bios) {
	// this.bios = bios;
	// }
	//
	// public int getStatus() {
	// return status;
	// }
	//
	// public void setStatus(int status) {
	// this.status = status;
	// }

	public Date getLast_time() {
		return last_time;
	}

	public void setLast_time(Date last_time) {
		this.last_time = last_time;
	}

	public int getCamera_id() {
		return camera_id;
	}

	public void setCamera_id(int camera_id) {
		this.camera_id = camera_id;
	}

	public int getShop_id() {
		return shop_id;
	}

	public void setShop_id(int shop_id) {
		this.shop_id = shop_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	@Override
	public String toString() {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			return objectMapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			return super.toString();
		}
	}
}
