package model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Entity
@Table(name = "customer")
public class Customer {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;

	@Column(name = "camera_id")
	private int camera_id;

	@Column(name = "shop_id")
	private int shop_id;

	@Column(name = "tracking_new")
	private String tracking_new;

	@Column(name = "gender")
	private String gender;

	@Column(name = "age")
	private int age;

	@Column(name = "emotion")
	private String emotion;

	@Column(name = "image_new")
	private String image_new;

	@Column(name = "datetime_new")
	private Date datetime_new;

	@Column(name = "similarity")
	private Float similarity;

	@Column(name = "tracking_old")
	private String tracking_old;

	@Column(name = "image_old")
	private String image_old;

	@Column(name = "datetime_old")
	private Date datetime_old;

	@Column(name = "name")
	private String name;

	@Column(name = "status")
	private int status;

	public Customer() {
	};

	public Customer(int id, int camera_id, int shop_id, String tracking_new, String gender, int age, String emotion,
			String image_new, Date datetime_new, Float similarity, String tracking_old, String image_old,
			Date datetime_old, String name) {
		this.id = id;
		this.camera_id = camera_id;
		this.shop_id = shop_id;
		this.tracking_new = tracking_new;
		this.gender = gender;
		this.age = age;
		this.emotion = emotion;
		this.image_new = image_new;
		this.image_old = image_old;
		this.datetime_new = datetime_new;
		this.datetime_old = datetime_old;
		this.similarity = similarity;
		this.tracking_old = tracking_old;
		this.name = name;
	}

	public Customer(int camera_id, int shop_id, String tracking_new, String gender, int age, String emotion,
			String image_new, Date datetime_new, Float similarity, String tracking_old, String image_old,
			Date datetime_old, String name) {
		this.camera_id = camera_id;
		this.shop_id = shop_id;
		this.tracking_new = tracking_new;
		this.gender = gender;
		this.age = age;
		this.emotion = emotion;
		this.image_new = image_new;
		this.image_old = image_old;
		this.datetime_new = datetime_new;
		this.datetime_old = datetime_old;
		this.similarity = similarity;
		this.tracking_old = tracking_old;
		this.name = name;
	}

	public Customer(int camera_id, int shop_id, String tracking_new, String gender, int age, String emotion,
			String image_new, Date datetime_new, Float similarity, String tracking_old, String image_old,
			Date datetime_old, String name, int status) {
		this.camera_id = camera_id;
		this.shop_id = shop_id;
		this.tracking_new = tracking_new;
		this.gender = gender;
		this.age = age;
		this.emotion = emotion;
		this.image_new = image_new;
		this.image_old = image_old;
		this.datetime_new = datetime_new;
		this.datetime_old = datetime_old;
		this.similarity = similarity;
		this.tracking_old = tracking_old;
		this.name = name;
		this.status = status;
	}

	public Customer(int id, String gender, int age, String name, int status) {
		this.id = id;
		this.gender = gender;
		this.age = age;
		this.name = name;
		this.status = status;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getTracking_new() {
		return tracking_new;
	}

	public void setTracking_new(String tracking_new) {
		this.tracking_new = tracking_new;
	}

	public Float getSimilarity() {
		return similarity;
	}

	public void setSimilarity(Float similarity) {
		this.similarity = similarity;
	}

	public String getTracking_old() {
		return tracking_old;
	}

	public void setTracking_old(String tracking_old) {
		this.tracking_old = tracking_old;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getCamera_id() {
		return camera_id;
	}

	public void setCamera_id(int camera_id) {
		this.camera_id = camera_id;
	}

	public int getShop_id() {
		return shop_id;
	}

	public void setShop_id(int shop_id) {
		this.shop_id = shop_id;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getEmotion() {
		return emotion;
	}

	public void setEmotion(String emotion) {
		this.emotion = emotion;
	}

	public String getImage_new() {
		return image_new;
	}

	public void setImage_new(String image_new) {
		this.image_new = image_new;
	}

	public String getImage_old() {
		return image_old;
	}

	public void setImage_old(String image_old) {
		this.image_old = image_old;
	}

	public Date getDatetime_new() {
		return datetime_new;
	}

	public void setDatetime_new(Date datetime_new) {
		this.datetime_new = datetime_new;
	}

	public Date getDatetime_old() {
		return datetime_old;
	}

	public void setDatetime_old(Date datetime_old) {
		this.datetime_old = datetime_old;
	}

	@Override
	public String toString() {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			return objectMapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			return super.toString();
		}
	}

}
