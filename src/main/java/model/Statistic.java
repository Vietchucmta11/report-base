package model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Statistic {
	
	private String name;
	private int quatity;
	
	public Statistic() {};
	
	public Statistic(String name, int quatity) {
		this.name= name;
		this.quatity=quatity;
	}
	public String getName() {
		return name;
	}
	public int getQuatity() {
		return quatity;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setQuatity(int quatity) {
		this.quatity = quatity;
	}
	
	@Override
	public String toString() {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			return objectMapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			return super.toString();
		}
	}
}
