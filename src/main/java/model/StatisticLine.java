package model;

import java.util.List;

public class StatisticLine {
	private List<String> strDate;
	private List<Line> dataLine;

	public StatisticLine() {
	};

	public List<String> getStrDate() {
		return strDate;
	}

	public void setStrDate(List<String> strDate) {
		this.strDate = strDate;
	}

	public List<Line> getDataLine() {
		return dataLine;
	}

	public void setDataLine(List<Line> dataLine) {
		this.dataLine = dataLine;
	}

}
