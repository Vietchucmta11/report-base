package model;

import java.util.List;

public class Line {
	private String name;
	
	private List<Integer> data;

	public Line() {

	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Integer> getData() {
		return data;
	}

	public void setData(List<Integer> data) {
		this.data = data;
	}
}
