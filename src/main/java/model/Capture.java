package model;

import java.util.Date;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Capture {
	private int shop_id;

	private int camera_id;

	//private String bios;

	private String url;

	private Date date;

	public Capture() {
	};

	public Capture(int shop_id, int camera_id, Date date) {
		this.shop_id = shop_id;
		this.camera_id = camera_id;
	//	this.bios = bios;
		this.date = date;
	}

	public Capture(String url, Date date) {
		this.url = url;
		this.date = date;
	};

	public int getShop_id() {
		return shop_id;
	}

	public void setShop_id(int shop_id) {
		this.shop_id = shop_id;
	}

	public int getCamera_id() {
		return camera_id;
	}

	public void setCamera_id(int camera_id) {
		this.camera_id = camera_id;
	}

//	public String getBios() {
//		return bios;
//	}
//
//	public void setBios(String bios) {
//		this.bios = bios;
//	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	@Override
	public String toString() {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			return objectMapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			return super.toString();
		}
	}
}
