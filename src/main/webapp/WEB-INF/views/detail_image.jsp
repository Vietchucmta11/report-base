<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Kết quả phân tích</title>
<link rel="shortcut icon" href="<c:url value="/sources/images/favicon.png"/>" type="image/x-icon">

</head>
<body class="skin-blue sidebar-mini"
	style="height: auto; min-height: 100%;">
	<div class="wrapper" style="height: auto; min-height: 100%;">
		<%-- <jsp:include page="/user/header" /> --%>
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper" style="min-height: 966px;">
			<div>
				<div class="col-md-3" style="font-size: 24px;">Chi tiết dự
					đoán</div>
				<div class="col-md-4" style="font-size: 24px;"></div>
			</div>
			<div class="content col-md-12" style="font-size: 15px;">
				<div class="box">
					<form method="post"
						action="${pageContext.request.contextPath}/user/update/image=${detailCustomer.id}">
						<div class="box-body ">
							<div class="col=md-12 row">
								<div class="col-md-1"></div>
								<div class="col-md-5">
									<div>
										<img src="<c:url value="${detailCustomer.image_new }"/>"
											style="width: 250px; height: 300px;" />
									</div>
									<div>
										<strong>Ngày chụp: ${detailCustomer.datetime_new}</strong>
									</div>
								</div>
								<div class="col-md-1"></div>
								<div class="col-md-5">
									<div>
										<img src="<c:url value="${detailCustomer.image_old }"/>"
											style="width: 250px; height: 300px;" />
									</div>
									<div>
										<strong>Ngày chụp: ${detailCustomer.datetime_old}</strong>
									</div>
								</div>
							</div>
							<div class="col=md-12 row" style="">
								<div class="col-md-6">
									<div class="row" style="margin-top: 15px;">
										<div class="col-md-1"></div>
										<div class="col-md-4">
											<strong>Giới tính</strong>
										</div>
										<div class="col-md-6">
											<c:set var="gender" value="${detailCustomer.gender}" />
											<c:if test="${gender=='Male'}">
												<select id="dropdown" name="gender" style="width: 195px;">
													<option value="Male" selected>Male</option>
													<option value="Female">Female</option>
												</select>
											</c:if>
											<c:if test="${gender=='Female'}">
												<select id="dropdown" name="gender" style="width: 195px;">
													<option value="Male">Male</option>
													<option value="Female" selected>Female</option>
												</select>
											</c:if>
											<%-- <input type="text" name="gender"
												value="${detailCustomer.gender}" /> --%>
										</div>
									</div>
									<div class="row" style="margin-top: 15px;">
										<div class="col-md-1"></div>
										<div class="col-md-4">
											<strong>Tuổi</strong>
										</div>
										<div class="col-md-6">
											<input name="age" value="${detailCustomer.age}" min="0"
												max="100" type="number" style="width: 195px;" />
										</div>
									</div>
									<div class="row" style="margin-top: 15px;">
										<div class="col-md-1"></div>
										<div class="col-md-4">
											<strong>Họ và tên</strong>
										</div>
										<div class="col-md-6">
											<input type="text" name="nameImage"
												value="${detailCustomer.name}" />
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="row" style="margin-top: 15px;">
										<div class="col-md-1"></div>
										<c:set var="trangthai" value="${detailCustomer.status}" />
										<c:if test="${trangthai==-1}">
											<div class="col-md-1">
												<strong>Sai</strong>
											</div>
											<div class="col-md-1">
												<input type="radio" name="cbxStatus" value="-1" checked />
											</div>
											<div class="col-md-1">
												<strong>Đúng</strong>
											</div>
											<div class="col-md-1">
												<input type="radio" name="cbxStatus" value="1" />
											</div>
										</c:if>
										<c:if test="${trangthai==0}">
											<div class="col-md-1">
												<strong>Sai</strong>
											</div>
											<div class="col-md-1">
												<input type="radio" name="cbxStatus" value="-1" />
											</div>
											<div class="col-md-1">
												<strong>Đúng</strong>
											</div>
											<div class="col-md-1">
												<input type="radio" name="cbxStatus" value="1" checked />
											</div>
										</c:if>
										<c:if test="${trangthai==1}">
											<div class="col-md-1">
												<strong>Sai</strong>
											</div>
											<div class="col-md-1">
												<input type="radio" name="cbxStatus" value="-1" />
											</div>
											<div class="col-md-1">
												<strong>Đúng</strong>
											</div>
											<div class="col-md-1">
												<input type="radio" name="cbxStatus" value="1" checked />
											</div>
										</c:if>
									</div>
									<div style="margin-top: 30px;">
										<input type="submit" value="Update" id="btnUpdate" />
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- /.content-wrapper -->
		<%-- <jsp:include page="/user/footer" /> --%>
	</div>
</body>
</html>