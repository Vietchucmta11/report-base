<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Thống kê trong ngày</title>
<link rel="shortcut icon"
	href="<c:url value="/sources/images/favicon.png"/>" type="image/x-icon">
<link href="<c:url value="/sources/css/style.css" />" rel="stylesheet">
</head>
<body class="skin-blue sidebar-mini"
	style="height: auto; min-height: 100%;">
	<div class="wrapper" style="height: auto; min-height: 100%;">
		<jsp:include page="/user/header" />
		<div class="content-wrapper"
			style="min-height: 1000px; margin-top: 50px;">
			<div id="loadingDiv">
				<img src="<c:url value="/sources/images/demo_wait.gif"/>" width="64"
					height="64" /><br>Loading..
			</div>
			<div id="loadingDibLast">
				<div class="content-header">
					<div class="row" style="margin-bottom: 15px;">
						<div class="col-md-6">
							<div class="col-md-3">
								<strong>Chọn ngày:</strong>
							</div>
							<div class="col-md-3">
								<input type="text" name="chartLineByDate" value=""
									style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%" />
							</div>
						</div>
					</div>
					<div class="row showHtmlSum"></div>
				</div>
				<div class="content">
					<div class="row">
						<div class="col-md-12">
							<label>
								<h3>
									<strong>Tổng quát<strong>
								</h3>
							</label>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="box box-primary">
								<div class="box-header with-border">
									<div>
										<div class="col-md-6">
											<i class="fa fa-bar-chart-o"></i>
											<h3 class="box-title">Thống kê lượt ảnh theo ngày</h3>
										</div>
									</div>
									<div class="box-tools pull-right">
										<button type="button" class="btn btn-box-tool"
											data-widget="collapse">
											<i class="fa fa-minus"></i>
										</button>
										<button type="button" class="btn btn-box-tool"
											data-widget="remove">
											<i class="fa fa-times"></i>
										</button>
									</div>
								</div>
								<div id="lineChartDate" class="box-body"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label>
								<h3>
									<strong>Giới tính<strong>
								</h3>
							</label>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="box box-primary">
								<div class="box-header with-border">
									<div>
										<div class="col-md-6">
											<i class="fa fa-bar-chart-o"></i>
											<h3 class="box-title">Thống kê giới tính theo ngày</h3>
										</div>
									</div>
									<div class="box-tools pull-right">
										<button type="button" class="btn btn-box-tool"
											data-widget="collapse">
											<i class="fa fa-minus"></i>
										</button>
										<button type="button" class="btn btn-box-tool"
											data-widget="remove">
											<i class="fa fa-times"></i>
										</button>
									</div>
								</div>
								<div id="lineChartGenderDate" class="box-body"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<label>
								<h3>
									<strong>Cảm xúc<strong>
								</h3>
							</label>
						</div>
						<div class="col-md-6">
							<label>
								<h3>
									<strong>Độ tuổi<strong>
								</h3>
							</label>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="box box-primary">
								<div class="box-header with-border">
									<div>
										<div class="col-md-6">
											<i class="fa fa-bar-chart-o"></i>
											<h3 class="box-title">Thống kê cảm xúc theo ngày</h3>
										</div>
									</div>
									<div class="box-tools pull-right">
										<button type="button" class="btn btn-box-tool"
											data-widget="collapse">
											<i class="fa fa-minus"></i>
										</button>
										<button type="button" class="btn btn-box-tool"
											data-widget="remove">
											<i class="fa fa-times"></i>
										</button>
									</div>
								</div>
								<div id="lineChartEmotionDate" class="box-body"></div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="box box-primary">
								<div class="box-header with-border">
									<div>
										<div class="col-md-6">
											<i class="fa fa-bar-chart-o"></i>
											<h3 class="box-title">Thống kê độ tuổi theo ngày</h3>
										</div>
									</div>
									<div class="box-tools pull-right">
										<button type="button" class="btn btn-box-tool"
											data-widget="collapse">
											<i class="fa fa-minus"></i>
										</button>
										<button type="button" class="btn btn-box-tool"
											data-widget="remove">
											<i class="fa fa-times"></i>
										</button>
									</div>
								</div>
								<div id="lineChartAgeDate" class="box-body"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<jsp:include page="/user/footer" />
	</div>
	<script src="<c:url value="/sources/highcharts/js/highcharts.js" />"></script>
	<script src="<c:url value="/sources/highcharts/js/series-label.js" />"></script>
	<script src="<c:url value="/sources/highcharts/js/exporting.js" />"></script>
	<script src="<c:url value="/sources/highcharts/js/export-data.js" />"></script>
	<script src="<c:url value="/sources/highcharts/js/chartbydate.js" />"></script>
</body>
</html>