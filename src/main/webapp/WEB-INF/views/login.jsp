<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Login</title>
<link rel="shortcut icon" href="<c:url value="/sources/images/favicon.png"/>" type="image/x-icon">

<link href="<c:url value="/sources/css/bootstrap.css"/>"
	rel="stylesheet">
<link href="<c:url value="/sources/css/font-awesome.min.css"/>"
	rel="stylesheet">
<link href="<c:url value="/sources/css/ionicons.min.css"/>"
	rel="stylesheet">
<link href="<c:url value="/sources/css/AdminLTE.min.css"/>"
	rel="stylesheet">
<link href="<c:url value="/sources/css/blue.css"/>" rel="stylesheet">
</head>
<body class="hold-transition login-page">
	<div class="login-box">
		<div class="login-logo">
			<a href="../../index2.html"><b>Login</b></a>
		</div>
		<!-- /.login-logo -->
		<div class="login-box-body">
			<p class="login-box-msg">Sign in to start your BUSINESS</p>

			<form action='<spring:url value="/home"/>'
				method="post">
				<div class="form-group has-feedback">
					<input type="text" name="username" class="form-control"
						placeholder="Username">
				</div>
				<div class="form-group has-feedback">
					<input type="password" name="password" class="form-control"
						placeholder="Password">
				</div>
				<div class="row">
					<!-- <div class="col-xs-8">
						<div class="checkbox icheck">
							<label class="">
									<input type="checkbox" />
									 Remember Me
							</label>
						</div>
					</div> -->
					<!-- /.col -->
					<div class="col-xs-4">
						<button type="submit" class="btn btn-primary btn-block btn-flat">Sign
							In</button>
					</div>
					<!-- /.col -->
				</div>
			</form>
			<!-- <a href="#">I forgot my password</a><br>  -->
		</div>
		<!-- /.login-box-body -->
	</div>
	<script src="<c:url value="/sources/js/jquery-3.3.1.min.js" />"></script>
	<script src="<c:url value="/sources/js/bootstrap.js" />"></script>
	<script src="<c:url value="/sources/js/icheck.min.js" />"></script>
</body>
</html>