<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Trang chủ</title>
<link rel="shortcut icon" href="<c:url value="/sources/images/favicon.png"/>" type="image/x-icon">

</head>
<body class="skin-blue sidebar-mini"
	style="height: auto; min-height: 100%;">
	<div class="wrapper" style="height: auto; min-height: 100%;">
<%-- 		<jsp:include page="/user/header" /> --%>
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper" style="min-height: 966px;">
			<div>
				<div class="col-md-3" style="font-size: 24px;">Danh sách dự
					đoán</div>
				<div class="col-md-2"></div>
				<div class="col-md-2">
					<laybel>Chọn ngày</laybel>
				</div>
				<div class="col-md-2">
					<input id="dateSearch" type="date" required
						pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" style="height: 32px;">
				</div>
				<div class="col-md-1">
					<button id="btnSearchByDate">Search</button>
				</div>
			</div>
			<div class="content">
				<div class="row">
					<div class="col-xs-12">
						<div class="box">
							<div class="box-body">
								<div id="example1_wrapper"
									class="dataTables_wrapper form-inline dt-bootstrap">
									<div class="row"></div>
									<div class="row">
										<div class="col-md-12">
											<div id="wrapper">
												<section>
												<div id="testpaging"></div>
												</section>
											</div>
											<table class="table table-borderred table-striped dataTable"
												id="showlist_dudoan">
												<thead>
													<tr>
														<th></th>
														<th>Ảnh</th>
														<th>Giới tính</th>
														<th>Tuổi</th>
														<th>Cảm xúc</th>
														<th>Ảnh cũ</th>
														<th style="width: 30px;"></th>
													</tr>
												</thead>
												<tbody id="${idTable}">
													<!-- List analytic -->
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--Test phan trang  -->
			
		</div>
		<!-- /.content-wrapper -->
		<%-- <jsp:include page="/user/footer" /> --%>
	</div>
</body>
</html>