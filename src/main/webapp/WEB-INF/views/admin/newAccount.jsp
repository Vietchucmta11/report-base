<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Tạo tài khoản</title>
<link rel="shortcut icon" href="<c:url value="/sources/images/favicon.png"/>" type="image/x-icon">

<link href="<c:url value="/sources/css/pagination.css"/>"
	rel="stylesheet">
<style type="text/css">
ul, li {
	list-style: none;
}

#wrapper {
	width: 900px;
	margin: 20px auto;
}

.data-container {
	margin-top: 20px;
}

.data-container ul {
	padding: 0;
	margin: 0;
}

.data-container li {
	margin-bottom: 5px;
	padding: 5px 10px;
	background: #eee;
	color: #666;
}
</style>
</head>
<body class="skin-blue sidebar-mini"
	style="height: auto; min-height: 100%;">
	<div class="wrapper" style="height: auto; min-height: 100%;">
		<jsp:include page="/admin/header" />
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper" style="min-height: 966px;">
			<!-- //code table dữ liệu -->
			<div>
				<div class="col-md-3" style="font-size: 24px;">Tạo tài khoản</div>
				<div class="col-md-2"></div>
				<div class="col-md-7">
					<div class="row"
						style="font-size: 14px; margin-top: 7px; margin-bottom: 5px;">
						<div class="col-md-2"></div>
						<div class="col-md-3"></div>
						<div class="col-md-2"></div>
						<div class="col-md-2"></div>
						<div class="col-md-3"></div>
					</div>
				</div>
			</div>
			<div class="content">
				<div class="row">
					<div class="col-xs-12">
						<div class="box">
							<div class="box-body" style="background-color: #fcfcfc;">
								<div id="example1_wrapper"
									class="dataTables_wrapper form-inline dt-bootstrap">
									<div class="row"></div>
									<div class="row">
										<div class="col-md-12">
											<a
												href="${pageContext.request.contextPath}/admin/listAccount">
												<div id="" class="col-md-2">
													<button type="button" class="btn btn-block btn-info">
														Danh sách User</span>
													</button>
											</a>
										</div>
										<div class="col-md-12" style="margin-top: 30px;">
											<div class="col-md-2"></div>
											<div class="col-md-6">
												<table class="table table-bordered">
													<tr>
														<td><strong>Username:</strong></td>
														<td><input id="usernameUser" value=""/></td>
													</tr>
													<tr>
														<td><strong>Password</strong></td>
														<td><input id="passwordUser" value=""/></td>
													</tr>
													<tr>
														<td><strong>Shop</strong></td>
														<td id="listShopUser"></td>
													</tr>
													<tr>
														<td><strong>Authority</strong></td>
														<td><select id="authorityUser" name="authority"
															style="width: 174px; height: 26px;">
																<option value="ROLE_USER" selected>ROLE_USER</option>
																<option value="ROLE_ADMIN">ROLE_ADMIN</option>
														</select></td>
													</tr>
													<tr>
														<td><strong>Enabled</strong></td>
														<td><input type="radio" name="cbxEnabledUser"
															value="0" checked /><strong>Khóa</strong>
															<div style="height: 10px;"></div> <input type="radio"
															name="cbxEnabledUser" value="1" /><strong>Mở</strong></td>
													</tr>
													<tr>
														<td></td>
														<td><button id="btnCreateAccount" class="btn btn-block btn-info"
																style="width: 80px;">Save</button></td>
													</tr>
												</table>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- /.content-wrapper -->
	<jsp:include page="/admin/footer" />	
	</div>
	<script src="<c:url value="/sources/js/saveUser.js" />"></script>
</body>
</html>