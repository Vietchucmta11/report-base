<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Danh sach camera</title>
<link rel="shortcut icon"
	href="<c:url value="/sources/images/favicon.png"/>" type="image/x-icon">
<link href="<c:url value="/sources/css/pagination.css"/>"
	rel="stylesheet">
<style type="text/css">
ul, li {
	list-style: none;
}

#wrapper {
	width: 900px;
	margin: 20px auto;
}

.data-container {
	margin-top: 20px;
}

.data-container ul {
	padding: 0;
	margin: 0;
}

.data-container li {
	margin-bottom: 5px;
	padding: 5px 10px;
	background: #eee;
	color: #666;
}
</style>
</head>
<body class="skin-blue sidebar-mini"
	style="height: auto; min-height: 100%;">
	<div class="wrapper" style="height: auto; min-height: 100%;">
		<jsp:include page="/admin/header" />
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper" style="min-height: 966px;">
			<div>
				<div class="col-md-3" style="font-size: 24px;">Danh sách
					camera</div>
				<div class="col-md-2"></div>
				<div class="col-md-7"></div>
			</div>
			<div class="content">
				<div class="row">
					<div class="col-md-12">
						<div class="box">
							<div class="box-header with-border" style="font-size: 24px;">Thêm
								cửa hàng</div>
							<div class="box-body">
								<div id="example1_wrapper"
									class="dataTables_wrapper form-inline dt-bootstrap box-body no-padding">
									<div class="row">
										<div class="col-md-2"></div>
										<div class="col-md-8">
											<table class="table table-bordered">
												<tr>
													<td><strong>Shop</strong></td>
													<td id="listShopUser"></td>
													<td><strong>Name:</strong></td>
													<td><input id="nameCamera" value="" /></td>
												</tr>
												<tr>
													<td><strong>Place</strong></td>
													<td><input id="placeCamera" value="" /></td>
													<td></td>
													<td><button id="btnCreateCamera"
															class="btn btn-block btn-info" style="width: 80px;">Save</button></td>
												</tr>
											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="box">
							<div class="box-body">
								<div class="col-md-2">
									<div class="paginated"></div>
									<div id="listShop"></div>
								</div>
								<div class="col-md-10">
									<table class="table table-bordered table-striped paginated">
									</table>
									<div id="showListCamera"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="modal fade" id="myModal" role="dialog">
			<div class="modal-dialog" style="width: 1000px;">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Chi tiết camera</h4>
					</div>
					<div class="modal-body" style="height: 300px;">
						<div id="showDetailCamera"></div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		<!-- /.content-wrapper -->
		<jsp:include page="/admin/footer" />
	</div>
	<script src="<c:url value="/sources/js/pagination.min.js" />"></script>
	<script src="<c:url value="/sources/js/camera.js" />"></script>
</body>
</html>