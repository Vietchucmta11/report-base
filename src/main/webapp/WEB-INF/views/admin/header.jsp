<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.Date"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Header admin</title>
<link rel="shortcut icon"
	href="<c:url value="/sources/images/favicon.png"/>" type="image/x-icon">

<link href="<c:url value="/sources/css/bootstrap.css"/>"
	rel="stylesheet">
<link href="<c:url value="/sources/css/font-awesome.min.css"/>"
	rel="stylesheet">
<link href="<c:url value="/sources/css/ionicons.min.css"/>"
	rel="stylesheet">
<link href="<c:url value="/sources/css/jquery-jvectormap.css"/>"
	rel="stylesheet">
<link href="<c:url value="/sources/css/AdminLTE.min.css"/>"
	rel="stylesheet">
<link href="<c:url value="/sources/css/_all-skins.min.css"/>"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<link href="<c:url value="/sources/date/daterangepicker.css"/>"
	rel="stylesheet">
<style type="text/css">
.jqstooltip {
	position: absolute;
	left: 0px;
	top: 0px;
	visibility: hidden;
	background: rgb(0, 0, 0) transparent;
	background-color: rgba(0, 0, 0, 0.6);
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000,
		endColorstr=#99000000);
	-ms-filter:
		"progid:DXImageTransform.Microsoft.gradient(startColorstr=#99000000, endColorstr=#99000000)";
	color: white;
	font: 10px arial, san serif;
	text-align: left;
	white-space: nowrap;
	padding: 5px;
	border: 1px solid white;
	box-sizing: content-box;
	z-index: 10000;
}

.jqsfield {
	color: white;
	font: 10px arial, san serif;
	text-align: left;
}
</style>
</head>
<body>
	<header class="main-header">
		<!-- Logo -->
		<a href="${pageContext.request.contextPath}/admin/home" class="logo">
			<!-- mini logo for sidebar mini 50x50 pixels --> <span
			class="logo-mini"><i class="glyphicon glyphicon-home"></i></span> <!-- logo for regular state and mobile devices -->
			<span class="logo-lg"><b>Admin</b></span>
		</a>
		<!-- Header Navbar: style can be found in header.less -->
		<nav class="navbar navbar-static-top">
			<!-- Sidebar toggle button-->
			<div class="row col-md-12">
				<div class="col-md-1">
					<a href="${pageContext.request.contextPath}/user/home"
						class="sidebar-toggle" data-toggle="push-menu" role="button">
						<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
					</a>
				</div>
				<div class="col-md-3"></div>
				<div class="col-md-4"></div>
				<div class="col-md-3"></div>
				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<!-- User Account: style can be found in dropdown.less -->
						<li class="dropdown user user-menu"><a href="#"
							class="dropdown-toggle" data-toggle="dropdown"
							aria-expanded="false"> <img
								src="<c:url value="/sources/images/user.png"/>"
								class="user-image" alt="User Image"> <span
								class="hidden-xs">${message} <i
									class="fa fa-fw fa-sort-down"></i></span>
						</a>
							<ul class="dropdown-menu extended logout" style="width: 150px;">
								<li class="user-footer" style="background-color: #3c8dbc;">
									<div class="pull-right">
										<a href="${pageContext.request.contextPath}/logout"
											class="btn btn-default btn-flat"
											style="width: 120px; border-radius: 5px;"><i
											class="fa fa-fw fa-key"></i> Log out</a>
									</div>
								</li>
							</ul></li>
					</ul>
				</div>
			</div>

		</nav>
	</header>
	<!-- Left side column. contains the logo and sidebar -->
	<aside class="main-sidebar">
		<!-- sidebar: style can be found in sidebar.less -->
		<section class="sidebar" style="height: auto;">
			<!-- Sidebar user panel -->
			<div class="user-panel">
				<div class="pull-left image">
					<img src="<c:url value="/sources/images/user.png"/>"
						class="img-circle" alt="User Image">
				</div>
				<div class="pull-left info">
					<p>${message}</p>
					<a href="#"><i class="fa fa-circle text-success"></i> Online</a>
				</div>
			</div>
			<!-- search form -->
			<form action="#" method="get" class="sidebar-form">
				<div class="input-group">
					<input type="text" name="q" class="form-control"
						placeholder="Search..."> <span class="input-group-btn">
						<button type="submit" name="search" id="search-btn"
							class="btn btn-flat">
							<span class="glyphicon glyphicon-search"></span>
							<!-- <i class="fa fa-search"></i> -->
						</button>
					</span>

				</div>
			</form>
			<!-- /.search form -->
			<ul class="sidebar-menu tree" data-widget="tree">
				<li class="treeview"><a href="#"> <i
						class="fa fa-fw fa-users"></i> <span>Quản lý tài khoản</span> <span
						class="pull-right-container"> <i
							class="fa fa-angle-left pull-right"></i></span>
				</a>
					<ul class="treeview-menu" style="display: none;">
						<li><a
							href="${pageContext.request.contextPath}/admin/listAccount">
								<i class="fa fa-fw fa-circle"></i>Danh sách tài khoản
						</a></li>
						<li><a
							href="${pageContext.request.contextPath}/admin/newAccount"> <i
								class="fa fa-fw fa-circle"></i>Tạo tài khoản
						</a></li>
					</ul></li>
				<li class=""><a
					href="${pageContext.request.contextPath}/admin/shop"><i
						class="fa fa-fw fa-shopping-cart"></i> <span>Quản lý cửa
							hàng</span> <!-- <span class="pull-right-container"> <i
							class="fa fa-angle-left pull-right"></i>
					</span>  --></a> <%-- <ul class="treeview-menu" style="display: none;">
						<li><a href="${pageContext.request.contextPath}/admin/listshop"> <i class="fa fa-fw fa-circle"></i>Danh
								sách cửa hàng
						</a></li>
						<li><a href="#"> <i class="fa fa-fw fa-circle"></i>Thêm
								cửa hàng
						</a></li>
					</ul> --%></li>
				<li class=""><a
					href="${pageContext.request.contextPath}/admin/camera"><i
						class="fa fa-fw fa-camera-retro"></i> <span>Quản lý camera</span>
						<span class="pull-right-container"> </span> </a></li>
				<li class=""><a><i class="fa fa-pie-chart"></i> <span>Đổi
							mật khẩu</span> <span class="pull-right-container"> </span></a></li>
			</ul>
			<!-- link list cua hang -->
		</section>
		<!-- /.sidebar -->
	</aside>
</body>
</html>