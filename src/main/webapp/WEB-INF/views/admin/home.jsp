<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Trang chủ Admin</title>
<link rel="shortcut icon" href="<c:url value="/sources/images/favicon.png"/>" type="image/x-icon">

</head>
<body class="skin-blue sidebar-mini"
	style="height: auto; min-height: 100%;">
	<div class="wrapper" style="height: auto; min-height: 100%;">
		<jsp:include page="/admin/header" />
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper" style="min-height: 966px;">
			<div>
				
			</div>
			<div class="content">
				<div class="row">
					<div class="col-xs-12">
						<div class="box">
							<div class="box-body">
								<div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.content-wrapper -->
		<jsp:include page="/admin/footer" />
	</div>
</body>
</html>