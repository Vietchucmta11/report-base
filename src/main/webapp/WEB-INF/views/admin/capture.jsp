<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Capture</title>
<link rel="shortcut icon"
	href="<c:url value="/sources/images/favicon.png"/>" type="image/x-icon">

<link href="<c:url value="/sources/css/pagination.css"/>"
	rel="stylesheet">
<style type="text/css">
ul, li {
	list-style: none;
}

#wrapper {
	width: 900px;
	margin: 20px auto;
}

.data-container {
	margin-top: 20px;
}

.data-container ul {
	padding: 0;
	margin: 0;
}

.data-container li {
	margin-bottom: 5px;
	padding: 5px 10px;
	background: #eee;
	color: #666;
}
</style>
</head>
<body class="skin-blue sidebar-mini"
	style="height: auto; min-height: 100%;">
	<div class="wrapper" style="height: auto; min-height: 100%;">
		<jsp:include page="/admin/header" />
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper" style="min-height: 966px;">
			<!-- //code table dữ liệu -->
			<div class="content-header">
				<div class="row" style="margin-bottom: 15px;">
					<div class="col-md-6">
						<div class="col-md-3">
							<strong>Chọn ngày:</strong>
						</div>
						<div class="col-md-3">
							<input type="text" name="searchCapture" value=""
								style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%" />
						</div>
						<div>
							<input id="keyCameraId" value="${cameraId}" type="hidden"/>
						</div>
					</div>
				</div>
			</div>
			<div class="content">
				<div class="row">
					<div class="col-md-12" id="showListCapture">
						<!--  -->
					</div>
				</div>
				<!-- /.content-wrapper -->
			</div>
		</div>
		<jsp:include page="/admin/footer" />
	</div>

	<script src="<c:url value="/sources/js/pagination.min.js" />"></script>
	<script src="<c:url value="/sources/js/capture.js" />"></script>
</body>
</html>