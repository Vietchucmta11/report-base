<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Footer</title>
<link rel="shortcut icon" href="<c:url value="/sources/images/favicon.png"/>" type="image/x-icon">

</head>
<body>
	<footer class="main-footer">
	<div class="pull-right hidden-xs">
		<b></b>
	</div>
	<strong><a href="http://datasection.com.vn/">Datasection
			Việt Nam</a></strong> </footer>
	<script src="<c:url value="/sources/js/jquery-3.3.1.min.js" />"></script>
	<script src="<c:url value="/sources/js/bootstrap.js" />"></script>
	<script src="<c:url value="/sources/js/fastclick.js" />"></script>
	<script src="<c:url value="/sources/js/adminlte.min.js" />"></script>
	<script src="<c:url value="/sources/js/jquery.sparkline.min.js" />"></script>
	<script
		src="<c:url value="/sources/js/jquery-jvectormap-1.2.2.min.js" />"></script>
	<script
		src="<c:url value="/sources/js/jquery-jvectormap-world-mill-en.js" />"></script>
	<script src="<c:url value="/sources/js/jquery.slimscroll.min.js" />"></script>
	<%-- <script src="<c:url value="/sources/js/dashboard2.js" />"></script> --%>
	<%-- <script src="<c:url value="/sources/js/Chart.js" />"></script> --%>
	<script src="<c:url value="/sources/js/demo.js" />"></script>
	<script src="<c:url value="/sources/date/moment.js" />"></script>
	<script src="<c:url value="/sources/date/daterangepicker.js" />"></script>
</body>
</html>