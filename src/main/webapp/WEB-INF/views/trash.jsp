<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Lọc trùng lặp</title>
<link rel="shortcut icon"
	href="<c:url value="/sources/images/favicon.png"/>" type="image/x-icon">

<link href="<c:url value="/sources/css/pagination.css"/>"
	rel="stylesheet">
<style type="text/css">
ul, li {
	list-style: none;
}

#wrapper {
	width: 900px;
	margin: 20px auto;
}

.data-container {
	margin-top: 20px;
}

.data-container ul {
	padding: 0;
	margin: 0;
}

.data-container li {
	margin-bottom: 5px;
	padding: 5px 10px;
	background: #eee;
	color: #666;
}
</style>
<link href="<c:url value="/sources/css/style.css" />" rel="stylesheet">
</head>
<body class="skin-blue sidebar-mini"
	style="height: auto; min-height: 100%;">
	<div class="wrapper" style="height: auto; min-height: 100%;">
		<jsp:include page="/user/header" />
		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper"
			style="min-height: 966px; margin-top: 50px;">
			<div id="loadingDiv">
				<img src="<c:url value="/sources/images/demo_wait.gif"/>" width="64"
					height="64" /><br>Loading..
			</div>
			<div id="loadingDibLast">
				<!-- //code table dữ liệu -->
				<div class="content-header ">
					<div class="row" style="margin-bottom: 15px;">
						<!-- <div class="col-md-4 col-sm-6">
							<div class="col-md-4">
								<strong>Chọn ngày</strong>
							</div>
							<div class="col-md-5">
								<input id="getDateSearch" type="text" name="dateSearch" value=""
									style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%" />
								<div id="getDateTrash"
									style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 100%">
									<i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
									<span id="valueDate"></span> <b class="caret"></b>
								</div>
							</div>
						</div> -->
					</div>
					<div class="row" id="showHtmlSum"></div>
				</div>
				<div class="content ">
					<div class="row">
						<div class="col-xs-12">
							<div class="box">
								<div class="box-header">
									<div class="row">
										<div class="col-md-6">
											<h3 class="box-title">Danh sách bản ghi lọc trùng lặp</h3>
										</div>
										<div class="col-md-6 col-sm-12">
											<div class="col-md-2">
												<%-- <input id="keyShopId" value="${shopId}" type="hidden" /> --%>
											</div>
											<div class="col-md-4"></div>
											<div class="col-md-3">
												<strong>Số bản ghi:</strong>
											</div>
											<div class="col-md-3">
												<select id="pagSize" name="size"
													class="form-control input-sm" style="width: 85px;">
													<option value="5">5</option>
													<option value="10">10</option>
													<option value="20">20</option>
													<option value="50" selected>50</option>
												</select>
											</div>
										</div>
									</div>
									<!-- <div class="row">
										<div class="col-md-6"></div>
										<div class="col-md-6">
											<div class="col-md-2">
												<strong>Nhập tỷ lệ:</strong>
											</div>
											<div class="col-md-3">
												<input id="inputSimilarity" type="number" min="0" max="100"
													style="width: 100%; height: 34px;" />
											</div>
											<div class="col-md-2">
												<button id="applySimilarity" type="button"
													class="btn btn-block btn-info">Apply</button>
											</div>
										</div>
									</div> -->
								</div>
								<div class="box-body">
									<div id="example1_wrapper"
										class="dataTables_wrapper form-inline dt-bootstrap">
										<div class="row">
											<div class="col-md-12">
												<table class="table table-bordered table-striped dataTable "
													id="tableTrash">
													<thead>
														<tr>
															<th>Lọc lúc</th>
															<th>Ảnh trong ngày</th>
															<th>Tên</th>
															<th>Giới tính</th>
															<th>Tuổi</th>
															<th>Cảm xúc</th>
															<th>Ảnh giống nhất trong quá khứ</th>
															<th>Độ giống nhau <!-- <input
																onkeyup="myFunction()" id="inputSimilarity"
																type="number" min="0" max="100" placeholder="Nhập tỷ lệ"
																style="width: 60%; float: right;" /> -->
															</th>
															<th></th>
														</tr>
													</thead>
													<tbody>
													</tbody>
												</table>
												<div class="showlistTrash"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<jsp:include page="/user/footer" />
		<!-- /.content-wrapper -->
	</div>
	<script src="<c:url value="/sources/js/bootstrap-notify.js" />"></script>
	<script src="<c:url value="/sources/js/pagination.min.js" />"></script>
	<script src="<c:url value="/sources/js/trash.js"/>"></script>
</body>
</html>