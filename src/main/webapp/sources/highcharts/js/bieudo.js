/**
 * 
 * 
 */

$(document).ready(function() {
	$(document).ajaxStart(function() {
		$("#loadingDibLast").css("display", "none");
		$("#loadingDiv").css("display", "block");
	});
	$(document).ajaxStop(function() {
		$("#loadingDiv").css("display", "none");
		$("#loadingDibLast").css("display", "block");
	});

	var url = document.URL;
	settingDateRangePicker(url);
});

function showHtmlStatisSum(startDate, endDate) {
	$
			.getJSON(
					"../user/statistic/sum/" + startDate + "_" + endDate,
					function(data, err) {
						$("#showHtmlSumDTD").empty();
						var strVar = "";
						strVar += "					<div class=\"col-md-3 col-sm-6 col-xs-12\">";
						strVar += "						<div class=\"info-box\">";
						strVar += "							<span class=\"info-box-icon bg-aqua\"><i class=\"fa fa-fw fa-file-image-o\" style=\"margin-top: 22px;\"><\/i><\/span>";
						strVar += "							<div class=\"info-box-content\">";
						strVar += "								<span class=\"info-box-text\">Tổng số bức ảnh:<\/span> <span";
						strVar += "									class=\"info-box-number\">"
								+ data[0].quatity
								+ "<small> Ảnh<\/small><\/span>";
						strVar += "							<\/div>";
						strVar += "							<!-- \/.info-box-content -->";
						strVar += "						<\/div>";
						strVar += "						<!-- \/.info-box -->";
						strVar += "					<\/div>";
						strVar += "					<div class=\"col-md-3 col-sm-6 col-xs-12\">";
						strVar += "						<div class=\"info-box\">";
						strVar += "							<span class=\"info-box-icon bg-red\"><i";
						strVar += "								class=\"fa fa-fw fa-transgender\" style=\"margin-top: 22px;\"><\/i><\/span>";
						strVar += "							<div class=\"info-box-content\">";
						strVar += "								<span class=\"info-box-text\">Male: <\/span> <strong>"
								+ data[1].quatity
								+ "<small> Ảnh<\/small><\/strong>";
						strVar += "								<span class=\"info-box-text\">Female: <\/span> <strong>"
								+ data[2].quatity
								+ "<small> Ảnh<\/small><\/strong>";
						strVar += "							<\/div>";
						strVar += "							<!-- \/.info-box-content -->";
						strVar += "						<\/div>";
						strVar += "						<!-- \/.info-box -->";
						strVar += "					<\/div>";
						strVar += "					<div class=\"col-md-3 col-sm-6 col-xs-12\">";
						strVar += "						<div class=\"info-box\">";
						strVar += "							<span class=\"info-box-icon bg-green\"><i";
						strVar += "								class=\"ion ion-ios-gear-outline\" style=\"margin-top: 22px;\"><\/i><\/span>";
						strVar += "							<div class=\"info-box-content\">";
						strVar += "								<span class=\"info-box-text\">Độ tuổi phổ biến:<\/span> <span";
						strVar += "									class=\"info-box-number\">";
						if (data[0].quatity == 0) {
							strVar += 0;
						} else {
							strVar += data[3].name + ": " + data[3].quatity;
						}
						strVar += "<small> Ảnh<\/small><\/span>";
						strVar += "							<\/div>";
						strVar += "							<!-- \/.info-box-content -->";
						strVar += "						<\/div>";
						strVar += "						<!-- \/.info-box -->";
						strVar += "					<\/div>";
						strVar += "					<div class=\"col-md-3 col-sm-6 col-xs-12\">";
						strVar += "						<div class=\"info-box\">";
						strVar += "							<span class=\"info-box-icon bg-yellow\"><i";
						strVar += "								class=\"fa fa-fw fa-smile-o\" style=\"margin-top: 22px;\"><\/i><\/span>";
						strVar += "							<div class=\"info-box-content\">";
						strVar += "								<span class=\"info-box-text\">Cảm xúc phổ biến:<\/span> <span";
						strVar += "									class=\"info-box-number\">";
						if (data[0].quatity == 0) {
							strVar += 0;
						} else {
							strVar += data[3].name + ": " + data[3].quatity;
						}
						strVar += "<small> Ảnh<\/small><\/span>";
						strVar += "							<\/div>";
						strVar += "							<!-- \/.info-box-content -->";
						strVar += "						<\/div>";
						strVar += "					<\/div>";
						$("#showHtmlSumDTD").append(strVar);
					});
};

function settingDateRangePicker(url) {
	var start = moment().subtract(6, 'days');
	var end = moment();
	function cb(start, end) {
		$('#reportrange span').html(
				start.format('DD/MM/YYYY') + ' - ' + end.format('DD/MM/YYYY'));
		showHtmlStatisSum(start.format('YYYY-MM-DD'), end.format('YYYY-MM-DD'));
		statisticLineDateToDate(url, "#linechartdate", start
				.format('YYYY-MM-DD'), end.format('YYYY-MM-DD'));
		statisticEmotion(url, "#chartEmotion", start.format('YYYY-MM-DD'), end
				.format('YYYY-MM-DD'));
		statisticGender(url, "#chartGender", start.format('YYYY-MM-DD'), end
				.format('YYYY-MM-DD'))
		statisticAge(url, "#chartAge", start.format('YYYY-MM-DD'), end
				.format('YYYY-MM-DD'));
		statisticLineGender(url, "#lineChartGender",
				start.format('YYYY-MM-DD'), end.format('YYYY-MM-DD'));
		statisticPieEmotion(url, "#lineChartEmotion", start
				.format('YYYY-MM-DD'), end.format('YYYY-MM-DD'));
		statisticPieAge(url, "#lineChartAge", start.format('YYYY-MM-DD'), end
				.format('YYYY-MM-DD'));
	}
	$('#reportrange')
			.daterangepicker(
					{
						startDate : start,
						endDate : end,
						ranges : {
							'Last 7 Days' : [ moment().subtract(6, 'days'),
									moment() ],
							'Last 30 Days' : [ moment().subtract(29, 'days'),
									moment() ],
							'This Month' : [ moment().startOf('month'),
									moment().endOf('month') ],
							'Last Month' : [
									moment().subtract(1, 'month').startOf(
											'month'),
									moment().subtract(1, 'month')
											.endOf('month') ],
						}
					}, cb);

	cb(start, end);
}

function statisticLineDateToDate(url, id, startDate, endDate) {
	url += "/line/" + startDate + "_" + endDate;
	var txtTitle = "";
	if (startDate === endDate) {
		txtTitle += "Thống kê số bức ảnh ngày " + startDate + "";
	} else if (startDate != endDate) {
		txtTitle += " Thống kê số bức ảnh từ ngày: " + startDate + " đến "
				+ endDate + "";
	}
	$.getJSON(url, function(data, err) {
		console.log(data);
		$(id).highcharts({
			chart : {
				type : "line"
			},
			title : {
				text : txtTitle
			},
			xAxis : {
				categories : data.strDate,
				title : {
					text : "Ngày"
				}
			},
			yAxis : {
				title : {
					text : "Số ảnh (Bức)"
				},
				min : 0
			},
			plotOptions : {
				line : {
					marker : {
						enable : true
					}
				}
			},
			colors : [ "rgb(124,181,236)", "#f202ad" ],
			series : data.dataLine,
		})
	});
}
// thong ke cảm xúc
function statisticEmotion(url, id, startDate, endDate) {
	var processed_json = new Array();
	url += "/emotion/" + startDate + "_" + endDate;
	var txtTitle = "";
	if (startDate === endDate) {
		txtTitle += "Thống kê cảm xúc ngày " + startDate + "";
	} else if (startDate !== endDate) {
		txtTitle += "Thống kê cảm xúc từ ngày " + startDate + " đến " + endDate
				+ "";
	}
	$.getJSON(url, function(data) {
		for (var i = 0; i < data.length; i++) {
			processed_json.push([ data[i].name, data[i].quatity ]);
		}
		console.log(data);
		$(id).highcharts({
			chart : {
				type : "column"
			},
			title : {
				text : txtTitle
			},
			xAxis : {
				type : 'category',
				allowDecimals : false,
				title : {
					text : ""
				}
			},
			yAxis : {
				title : {
					text : "Số ảnh (Bức)"
				}
			},
			series : [ {
				name : "Số ảnh",
				data : processed_json
			} ]
		});
	});
};
// thong kê giới tính
function statisticGender(url, id, startDate, endDate) {
	var processed_json = new Array();
	url += "/gender/" + startDate + "_" + endDate;
	var txtTitle = "";
	if (startDate === endDate) {
		txtTitle += "Thống kê giới tính ngày " + startDate + "";
	} else if (startDate !== endDate) {
		txtTitle += "Thống kê giới tính từ ngày " + startDate + " đến "
				+ endDate + "";
	}
	var processed_json = new Array();
	$
			.getJSON(
					url,
					function(data) {
						for (var i = 0; i < data.length; i++) {
							processed_json
									.push([ data[i].name, data[i].quatity ]);
						}
						console.log(data);
						var pieColors = (function() {
							var colors = [];
							colors.push("rgb(124,181,236)");
							colors.push("#f202ad");
							return colors;
						}());
						$(id)
								.highcharts(
										{
											chart : {
												plotBackgroundColor : null,
												plotBorderWidth : null,
												plotShadow : false,
												type : 'pie'
											},
											title : {
												text : txtTitle
											},
											tooltip : {
												pointFormat : '{series.name}: <b>{point.percentage:.1f}%</b>'
											},
											plotOptions : {
												pie : {
													allowPointSelect : true,
													cursor : 'pointer',
													colors : pieColors,
													dataLabels : {
														enabled : true,
														format : '<b>{point.name}</b><br>{point.percentage:.1f} %',
														distance : -50,
														filter : {
															property : 'percentage',
															operator : '>',
															value : 4
														}
													}
												}
											},
											series : [ {
												name : 'Customer',
												data : processed_json
											} ]
										});
					});
};
// thong ke tuổi bd pie chart
function statisticAge(url, id, startDate, endDate) {
	var processed_json = new Array();
	url += "/age/" + startDate + "_" + endDate;
	var txtTitle = "";
	if (startDate === endDate) {
		txtTitle += "Thống kê độ tuổi ngày " + startDate + "";
	} else if (startDate !== endDate) {
		txtTitle += "Thống kê độ tuổi từ ngày " + startDate + " đến " + endDate
				+ "";
	}
	$.getJSON(url, function(data) {
		for (var i = 0; i < data.length; i++) {
			processed_json.push([ data[i].name, data[i].quatity ]);
		}
		console.log(data);
		$(id).highcharts({
			chart : {
				type : "column"
			},
			title : {
				text : txtTitle
			},
			xAxis : {
				type : 'category',
				allowDecimals : false,
				title : {
					text : ""
				}
			},
			yAxis : {
				title : {
					text : "Số ảnh"
				}
			},
			series : [ {
				name : "Số lượng",
				data : processed_json
			} ]
		});
	});
};
// thong ke gender theo tung ngày line chart
function statisticLineGender(url, id, startDate, endDate) {
	url += "/linegender/" + startDate + "_" + endDate;
	var txtTitle = "";
	if (startDate === endDate) {
		txtTitle += "Thống kê giới tính ngày " + startDate + "";
	} else if (startDate != endDate) {
		txtTitle += " Thống kê giới tình từ ngày: " + startDate + " đến "
				+ endDate + "";
	}
	$.getJSON(url, function(data, err) {
		console.log(data);
		$(id).highcharts({
			chart : {
				type : "line"
			},
			title : {
				text : txtTitle
			},
			xAxis : {
				categories : data.strDate,
				title : {
					text : "Ngày"
				}
			},
			yAxis : {
				title : {
					text : "Số ảnh"
				},
				min : 0
			},
			plotOptions : {
				line : {
					marker : {
						enable : true
					}
				}
			},
			colors : [ "rgb(124,181,236)", "#f202ad" ],
			series : data.dataLine,
		})
	});
}
// thong ke cam xuc cho pie chart;
function statisticPieEmotion(url, id, startDate, endDate) {
	var dataY = new Array();
	url += "/emotion/" + startDate + "_" + endDate;
	var txtTitle = "";
	if (startDate == endDate) {
		txtTitle += "Thống kê cảm xúc ngày: " + startDate + "";
	} else if (startDate != endDate) {
		txtTitle += "Thống kê cảm xúc từ ngày: " + startDate + " đến "
				+ endDate + "";
	}
	$
			.getJSON(
					url,
					function(data, err) {
						for (var i = 0; i < data.length; i++) {
							dataY.push([ data[i].name, data[i].quatity ])
						}
						var pieColors = (function() {
							var colors = [];
							colors.push("#f00");
							colors.push("#1afd01");
							colors.push("#1f1e1e");
							colors.push("rgb(128,133,233)");
							colors.push("#f9e800");
							colors.push("#c74b19");
							colors.push("#0dc2fb");
							return colors;
						}());
						$(id)
								.highcharts(
										{
											chart : {
												plotBackgroundColor : null,
												plotBorderWidth : null,
												plotShadow : false,
												type : 'pie'
											},
											title : {
												text : txtTitle
											},
											tooltip : {
												pointFormat : '{series.name}: <b>{point.percentage:.1f}%</b>'
											},
											plotOptions : {
												pie : {
													allowPointSelect : true,
													cursor : 'pointer',
													colors : pieColors,
													dataLabels : {
														enabled : false,
														format : '<b>{point.name}</b>: {point.percentage:.1f} %',
													},
													showInLegend : true
												}
											},
											series : [ {
												name : "Số ảnh",
												data : dataY
											} ]
										})
					})
}
// thong ke do tuoi cho pie chart
function statisticPieAge(url, id, startDate, endDate) {
	url += "/age/" + startDate + "_" + endDate;
	var dataY = new Array();
	var txtTitle = "";
	if (startDate == endDate) {
		txtTitle += "Thống kê độ tuổi ngày: " + startDate + "";
	} else if (startDate != endDate) {
		txtTitle += "Thống kê độ tuổi từ ngày: " + startDate + " đến "
				+ endDate + "";
	}
	$.getJSON(url, function(data, err) {
		for (var i = 0; i < data.length; i++) {
			dataY.push([ data[i].name, data[i].quatity ])
		}
		var pieColors = (function() {
			var colors = [];
			colors.push("#ffbf00");
			colors.push("#ffff00");
			colors.push("#bfff00");
			colors.push("#80ff00");
			colors.push("#00ff00");
			colors.push("#00ffbf");
			colors.push("#00bfff");
			colors.push("#0080ff");
			colors.push("#0000ff");
			colors.push("#8000ff");
			colors.push("#bf00ff");
			colors.push("#ff0040");
			return colors;
		}());
		$(id).highcharts({
			chart : {
				plotBackgroundColor : null,
				plotBorderWidth : null,
				plotShadow : false,
				type : 'pie'
			},
			title : {
				text : txtTitle
			},
			tooltip : {
				pointFormat : '{series.name}: <b>{point.percentage:.1f}%</b>'
			},
			plotOptions : {
				pie : {
					allowPointSelect : true,
					cursor : 'pointer',
					dataLabels : {
						enabled : false
					},
					showInLegend : true
				}
			},
			series : [ {
				name : "",
				colorByPoint : true,
				data : dataY
			} ],
		})
	})
}
