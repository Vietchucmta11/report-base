/**
 * 
 */
$(document).ready(function() {
	$(document).ajaxStart(function() {
		$("#loadingDibLast").css("display", "none");
		$("#loadingDiv").css("display", "block");
	});
	$(document).ajaxStop(function() {
		$("#loadingDiv").css("display", "none");
		$("#loadingDibLast").css("display", "block");
	});

	settingDRPbyDate();
});

function showHtmlStatisSum(startDate){
	$.getJSON("../user/statistic/sum/"+startDate+"_"+startDate, function(data, err){
		$(".showHtmlSum").empty();
		var strVar="";
		strVar += "					<div class=\"col-md-3 col-sm-6 col-xs-12\">";
		strVar += "						<div class=\"info-box\">";
		strVar += "							<span class=\"info-box-icon bg-aqua\"><i class=\"fa fa-fw fa-file-image-o\" style=\"margin-top: 22px;\"><\/i><\/span>";
		strVar += "							<div class=\"info-box-content\">";
		strVar += "								<span class=\"info-box-text\">Tổng số bức ảnh:<\/span> <span";
		strVar += "									class=\"info-box-number\">"+data[0].quatity+"<small> Ảnh<\/small><\/span>";
		strVar += "							<\/div>";
		strVar += "							<!-- \/.info-box-content -->";
		strVar += "						<\/div>";
		strVar += "						<!-- \/.info-box -->";
		strVar += "					<\/div>";
		strVar += "					<div class=\"col-md-3 col-sm-6 col-xs-12\">";
		strVar += "						<div class=\"info-box\">";
		strVar += "							<span class=\"info-box-icon bg-red\"><i";
		strVar += "								class=\"fa fa-fw fa-transgender\" style=\"margin-top: 22px;\"><\/i><\/span>";
		strVar += "							<div class=\"info-box-content\">";
		strVar += "								<span class=\"info-box-text\">Male: <\/span> <strong>"+data[1].quatity+"<small> Ảnh<\/small><\/strong>";
		strVar += "								<span class=\"info-box-text\">Female: <\/span> <strong>"+data[2].quatity+"<small> Ảnh<\/small><\/strong>";
		strVar += "							<\/div>";
		strVar += "							<!-- \/.info-box-content -->";
		strVar += "						<\/div>";
		strVar += "						<!-- \/.info-box -->";
		strVar += "					<\/div>";
		strVar += "					<div class=\"col-md-3 col-sm-6 col-xs-12\">";
		strVar += "						<div class=\"info-box\">";
		strVar += "							<span class=\"info-box-icon bg-green\"><i";
		strVar += "								class=\"ion ion-ios-gear-outline\" style=\"margin-top: 22px;\"><\/i><\/span>";
		strVar += "							<div class=\"info-box-content\">";
		strVar += "								<span class=\"info-box-text\">Độ tuổi phổ biến:<\/span> <span";
		strVar += "									class=\"info-box-number\">";
		if(data[0].quatity==0){
			strVar+=0;
		}else{
			strVar+=data[3].name + ": " + data[3].quatity;
		}
		strVar+="<small> Ảnh<\/small><\/span>";
		strVar += "							<\/div>";
		strVar += "							<!-- \/.info-box-content -->";
		strVar += "						<\/div>";
		strVar += "						<!-- \/.info-box -->";
		strVar += "					<\/div>";
		strVar += "					<div class=\"col-md-3 col-sm-6 col-xs-12\">";
		strVar += "						<div class=\"info-box\">";
		strVar += "							<span class=\"info-box-icon bg-yellow\"><i";
		strVar += "								class=\"fa fa-fw fa-smile-o\" style=\"margin-top: 22px;\"><\/i><\/span>";
		strVar += "							<div class=\"info-box-content\">";
		strVar += "								<span class=\"info-box-text\">Cảm xúc phổ biến:<\/span> <span";
		strVar += "									class=\"info-box-number\">";
		if(data[0].quatity==0){
			strVar+=0;
		}else{
			strVar+=data[4].name + ": " + data[4].quatity;
		}
		strVar+="<small> Ảnh<\/small><\/span>";
		strVar += "							<\/div>";
		strVar += "							<!-- \/.info-box-content -->";
		strVar += "						<\/div>";
		strVar += "					<\/div>";
		$(".showHtmlSum").append(strVar);
	});
};

function settingDRPbyDate(url) {
	var start = moment();
	function cd(start) {
		showHtmlStatisSum(start.format('YYYY-MM-DD'));
		statisticByDate("#lineChartDate", start.format('YYYY-MM-DD'));
		statisticGenderDate("#lineChartGenderDate", start.format('YYYY-MM-DD'));
		statisticEmotionDate("#lineChartEmotionDate", start
				.format('YYYY-MM-DD'));
		statisticAgeDate("#lineChartAgeDate", start.format('YYYY-MM-DD'))
	}
	;
	$('input[name="chartLineByDate"]').daterangepicker({
		singleDatePicker : true,
		showDropdowns : true
	}, cd);
	cd(start);
};

// thống kê số lượng ảnh trong từng ngày
function statisticByDate(id, startDate) {
	var url = "../user/statistic/linedate/" + startDate;
	var datax = new Array();
	var txtTitle = "";
	txtTitle += "Thống kê số lượt ngày: " + startDate + "";
	$.getJSON(url, function(data, err) {
		for (var i = 0; i < data.length; i++) {
			datax.push([ data[i].name, data[i].quatity ]);
		}
		$(id).highcharts({
			chart : {
				type : "column"
			},
			title : {
				text : txtTitle
			},
			xAxis : {
				type : 'category',
				allowDecimals : false,
				title : {
					text : "Giờ"
				}
			},
			yAxis : {
				title : {
					text : "Số ảnh (Ảnh)"
				},
				min : 0
			},
			plotOptions : {
				spline : {
					marker : {
						enabled : true
					}
				}
			},
			series : [ {
				name : "Bức ảnh",
				data : datax
			} ],
		})
	})
};

// thống kê giới tính từng giờ trong ngày
function statisticGenderDate(id, startDate) {
	var url = "../user/statistic/genderdate/" + startDate;
	var txtTitle = "";
	txtTitle += "Thống kê giới tính ngày: " + startDate + "";
	$
			.getJSON(
					url,
					function(data, err) {
						console.log(data);
						$(id)
								.highcharts(
										{
											chart : {
												type : "column"
											},
											title : {
												text : txtTitle
											},
											xAxis : {
												categories : data.strDate,
												title : {
													text : "Giờ"
												}
											},
											yAxis : {
												title : {
													text : "Số ảnh"
												},
												min : 0
											},
											tooltip : {
												headerFormat : '<span style="font-size:10px">{point.key}</span><table>',
												pointFormat : '<tr><td style="color:{series.color};padding:0">{series.name}: </td>'
														+ '<td style="padding:0"><b>{point.y} Bức</b></td></tr>',
												footerFormat : '</table>',
												shared : true,
												useHTML : true
											},
											plotOptions : {
												column : {
													pointPadding : 0,
													borderWidth : 0
												}
											},
											colors : [ "rgb(124,181,236)",
													"#f202ad" ],
											series : data.dataLine,
										})
					})
}

function statisticEmotionDate(id, startDate) {
	var dataY = new Array();
	var url = "../user/statistic/emotion/" + startDate + "_" + startDate;
	var txtTitle = "";
	txtTitle += "Thống kê cảm xúc ngày: " + startDate + "";
	$
			.getJSON(
					url,
					function(data, err) {
						for (var i = 0; i < data.length; i++) {
							dataY.push([ data[i].name, data[i].quatity ])
						}
						var pieColors = (function() {
							var colors = [];
							colors.push("#f00");
							colors.push("#1afd01");
							colors.push("#1f1e1e");
							colors.push("rgb(128,133,233)");
							colors.push("#f9e800");
							colors.push("#c74b19");
							colors.push("#0dc2fb");
							return colors;
						}());
						$(id)
								.highcharts(
										{
											chart : {
												plotBackgroundColor : null,
												plotBorderWidth : null,
												plotShadow : false,
												type : 'pie'
											},
											title : {
												text : txtTitle
											},
											tooltip : {
												pointFormat : '{series.name}: <b>{point.percentage:.1f}%</b>'
											},
											plotOptions : {
												pie : {
													allowPointSelect : true,
													cursor : 'pointer',
													colors : pieColors,
													dataLabels : {
														enabled : false,
														format : '<b>{point.name}</b>: {point.percentage:.1f} %',
													},
													showInLegend: true
												}
											},
											series : [ {
												name : "Số ảnh",
												data : dataY
											} ]
										})
					})
}

function statisticAgeDate(id, startDate) {
	var url = "../user/statistic/age/" + startDate + "_" + startDate;
	var dataY = new Array();
	var txtTitle = "";
	txtTitle += "Thống kê độ tuổi ngày: " + startDate + "";
	$
			.getJSON(
					url,
					function(data, err) {
						for (var i = 0; i < data.length; i++) {
							dataY.push([ data[i].name, data[i].quatity ])
						}
						var pieColors = (function() {
							var colors = [];
							colors.push("#ffbf00");
							colors.push("#ffff00");
							colors.push("#bfff00");
							colors.push("#80ff00");
							colors.push("#00ff00");
							colors.push("#00ffbf");
							colors.push("#00bfff");
							colors.push("#0080ff");
							colors.push("#0000ff");
							colors.push("#8000ff");
							colors.push("#bf00ff");
							colors.push("#ff0040");
							return colors;
						}());
						$(id)
								.highcharts(
										{
											chart : {
												plotBackgroundColor : null,
												plotBorderWidth : null,
												plotShadow : false,
												type : 'pie'
											},
											title : {
												text : txtTitle
											},
											tooltip : {
												pointFormat : '{series.name}: <b>{point.percentage:.1f}%</b>'
											},
											plotOptions : {
												pie : {
													allowPointSelect : true,
													cursor : 'pointer',
													//colors : pieColors,
													dataLabels : {
														enabled : false,
														format : '<b>{point.name}</b>: {point.percentage:.1f} %',
													},
													showInLegend:true
												}
											},
											series : [ {
												name : "",
												data : dataY
											} ],
										})
					})
}