/**
 * Show lít Account
 */

$(document).ready(function() {
	var size = $("#pagSize").val();
	getListAccount(size);
})

function clickUpdateAcc(id) {
	var object= new Object();
	object.userName= id;
	var checkP= $("input[name='cbxReturnPass']:checked").val();
	if(checkP==1){
		object.passWord=$("#passwordAcc").val();
	}else{
		object.passWord=null;
	}
	object.shop_id=$("#shopIdUser").val();
	object.authority=$("#authorityAcc").val();
	var enabled=$("input[name='cbxUpdateAcc']:checked").val();
	if(enabled==0){
		object.enabled = false;
	}else if(enabled==1){
		object.enabled = true;
	}	
	var r = confirm("Bạn muốn lưu lại không?");
	var idTr = "#lastUpdateAcc" + id;
	if (r === true) {
		$.ajax({
			type : "POST",
			contentType : "application/json",
			url : "../admin/update/user="+id+"",
			data : JSON.stringify(object),
			dataType : 'json',
			timeout : 10000,
			success : function(data) {
				if (data === 200) {
					alert("Cập nhật thành công!!!");
					$.getJSON("../admin/detail/user="+ id + "", function(data, err) {
						 showAccLastUpdate(data, idTr);
					});
				} else if (data === 500) {
					alert("Không thành công!!!");
				}
			},
			error : function(e) {
				console.log("error", e);
			}
		})
	}else{
		alert("Kiểm tra nhập liệu!");
	}
};

function showAccLastUpdate(item, id) {
	$(id).empty();
	var strVar="";
	var namecbx = "cbx" + id;	
	strVar += "															<td><\/td>";
	strVar += "<td>" + item.userName + "<\/td>";
	strVar += "															<td>" + item.passWord
			+ "<\/td>";
	strVar += "															<td> " + item.shop_id
			+ "<\/td>";
	strVar += "															<td>" + item.authority
			+ "<\/td>";
	strVar += "<td>"
	if (item.enabled == true) {
		strVar += "											<div >";
		strVar += "												<input type=\"radio\" name=\""
				+ namecbx + "\" value=\"1\" checked\/>";
		strVar += "												<strong>Mở<\/strong>";
		strVar += "												<input type=\"radio\" name=\""
				+ namecbx + "\" value=\"0\" \/>";
		strVar += "												<strong>Khóa<\/strong>";
		strVar += "											<\/div>";
	} else {
		strVar += "											<div>";
		strVar += "												<input type=\"radio\" name=\""
				+ namecbx + "\" value=\"1\" \/>";
		strVar += "												<strong>Mở<\/strong>";
		strVar += "												<input type=\"radio\" name=\""
				+ namecbx + "\" value=\"0\" checked \/>";
		strVar += "												<strong>Khóa<\/strong>";
		strVar += "											<\/div>";
	}
	strVar += "<\/td>"
	strVar += "															<td>";
	strVar += "<button onclick=\"clickShowAcc('"
			+ id
			+ "')\" type=\"button\" class=\"btn btn-info btn-lg\"><span class=\"glyphicon glyphicon-edit\"><\/span><\/button>";
	strVar += "<\/td>";
	$(id).append(strVar);
};
function clickShowAcc(id) {
	$.getJSON("../admin/detail/user=" + id + "", function(data, err) {
		showDetailAcc("#showDetailAccount", data);
	});
	$("#myModal").modal();
};

function getListShopAcc(shop_id) {
	$.getJSON("../admin/listShop", function(data, err) {
		$("#showShopAcc").empty();
		var strVar = "";
		strVar += "<select id=\"shopIdUser\" name=\"shop_id\"";
		strVar += "															style=\"width: 174px; height: 26px;\">";
		$.each(data, function(index, item) {
			if (shop_id == item.shop_id) {
				strVar += "	<option value=\"" + item.shop_id + "\" selected>"
						+ item.name + "<\/option>";
			} else {
				strVar += "	<option value=\"" + item.shop_id + "\">"
						+ item.name + "<\/option>";
			}
		});
		if (shop_id == -1) {
			strVar += "	<option value=\"-1\" selected>admin<\/option>";
		} else {
			strVar += "	<option value=\"-1\" >admin<\/option>";
		}
		strVar += "														<\/select>";
		$("#showShopAcc").append(strVar);
	});
};

function showDetailAcc(id, data) {
	$(id).empty();
	var strVar = "";
	strVar += "<table class=\"table table-bordered\" style=\"margin-top: 30px;\">";
	strVar += "												<tr>";
	strVar += "<td><strong>Username:<\/strong><\/td>";
	strVar += "<td><input id=\"usernameAcc\" value=\"" + data.userName
			+ "\" \/><\/td>";
	strVar += "													<td><strong>Password<\/strong><\/td>";
	strVar += "													<td><input id=\"passwordAcc\" value=\"\" \/>";
	strVar += "												<input type=\"checkbox\" name=\"cbxReturnPass\" value=\"1\" \/>";
	strVar += "												<strong>Đổi MK<\/strong>";
	strVar += "<\/td>";
	strVar += "												<\/tr>";
	strVar += "												<tr>";
	strVar += "													<td><strong>Shop<\/strong><\/td>";
	strVar += "													<td id=\"showShopAcc\">";
	getListShopAcc(data.shop_id);
	strVar += "<\/td>";
	strVar += "													<td><strong>Authority<\/strong><\/td>";
	strVar += "													<td>";
	strVar += "<select id=\"authorityAcc\" name=\"authority\" style=\"width: 174px; height: 26px;\">";
	if (data.authority == "ROLE_ADMIN") {
		strVar += "<option value=\"ROLE_USER\" >ROLE_USER<\/option>";
		strVar += "<option value=\"ROLE_ADMIN\" selected>ROLE_ADMIN<\/option>";
	} else {
		strVar += "<option value=\"ROLE_USER\" selected>ROLE_USER<\/option>";
		strVar += "<option value=\"ROLE_ADMIN\">ROLE_ADMIN<\/option>";
	}
	strVar += "<\/select>";
	strVar += "<\/td>";
	strVar += "												<\/tr>";
	strVar += "<tr><td><strong>Enabled</strong><\/td>";
	var namecbx = "cbxUpdateAcc";
	strVar += "													<td>";
	if (data.enabled == true) {
		strVar += "											<div >";
		strVar += "												<input type=\"radio\" name=\"" + namecbx
				+ "\" value=\"1\" checked\/>";
		strVar += "												<strong>Mở<\/strong>";
		strVar += "												<input type=\"radio\" name=\"" + namecbx
				+ "\" value=\"0\" \/>";
		strVar += "												<strong>Khóa<\/strong>";
		strVar += "											<\/div>";
	} else {
		strVar += "											<div>";
		strVar += "												<input type=\"radio\" name=\"" + namecbx
				+ "\" value=\"1\" \/>";
		strVar += "												<strong>Mở<\/strong>";
		strVar += "												<input type=\"radio\" name=\"" + namecbx
				+ "\" value=\"0\" checked \/>";
		strVar += "												<strong>Khóa<\/strong>";
		strVar += "											<\/div>";
	}
	strVar += "<\/td>";
	strVar += "<td><\/td>";
	strVar += "													<td><button onclick=\"clickUpdateAcc('"+data.userName+"')\"";
	strVar += "															class=\"btn btn-block btn-info\" style=\"width: 80px;\">Save<\/button><\/td>";
	strVar += "<\/tr>";
	strVar += "											<\/table>";
	$(id).append(strVar);
};

function showHtmlListUser(data, pagination, strId) {
	var strVar = "";
	strVar += "<thead>";
	strVar += "													<tr>";
	strVar += "														<th><\/th>";
	strVar += "														<th>Username<\/th>";
	strVar += "<th>Password<\/th>";
	strVar += "														<th>Shop_id<\/th>";
	strVar += "														<th>Authority<\/th>";
	strVar += "														<th>Enabled<\/th>";
	strVar += "													<\/tr>";
	strVar += "												<\/thead>";
	strVar += "												<tbody>";
	var i = 1;
	$
			.each(
					data,
					function(index, item) {
						var idTr = "lastUpdateAcc" + item.userName;
						var namecbx = "cbx" + item.userName;
						strVar += "													<tr id=\"" + idTr + "\">";
						strVar += "															<td><\/td>";
						strVar += "<td>" + item.userName + "<\/td>";
						strVar += "															<td>" + item.passWord
								+ "<\/td>";
						strVar += "															<td> " + item.shop_id
								+ "<\/td>";
						strVar += "															<td>" + item.authority
								+ "<\/td>";
						strVar += "<td>"
						if (item.enabled == true) {
							strVar += "											<div >";
							strVar += "												<input type=\"radio\" name=\""
									+ namecbx + "\" value=\"1\" checked\/>";
							strVar += "												<strong>Mở<\/strong>";
							strVar += "												<input type=\"radio\" name=\""
									+ namecbx + "\" value=\"0\" \/>";
							strVar += "												<strong>Khóa<\/strong>";
							strVar += "											<\/div>";
						} else {
							strVar += "											<div>";
							strVar += "												<input type=\"radio\" name=\""
									+ namecbx + "\" value=\"1\" \/>";
							strVar += "												<strong>Mở<\/strong>";
							strVar += "												<input type=\"radio\" name=\""
									+ namecbx + "\" value=\"0\" checked \/>";
							strVar += "												<strong>Khóa<\/strong>";
							strVar += "											<\/div>";
						}
						strVar += "<\/td>"
						strVar += "															<td>";
						strVar += "<button onclick=\"clickShowAcc('"
								+ item.userName
								+ "')\" type=\"button\" class=\"btn btn-info btn-lg\"><span class=\"glyphicon glyphicon-edit\"><\/span><\/button>";
						strVar += "<\/td>";
						strVar += "														<\/tr>";
						i = i + 1;
					});
	strVar += "<\/tbody>";
	$(strId).prev().html(strVar);
}

function getListAccount(size) {
	var url = "../admin/listUser";
	$.getJSON(url, function(data, err) {
		$('#listAccount').pagination({
			dataSource : data,
			totalNumber : data.length,
			pageSize : size,
			ajax : {
				beforeSend : function() {
					container.prev().html('Loading data from ...');
				}
			},
			callback : function(data, pagination) {
				showHtmlListUser(data, pagination, '#listAccount');
			}
		});
	});
}