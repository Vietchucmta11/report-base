$(document).ready(function() {
	getListShop();
	$("#btnCreateAccount").click(function() {
		createUser();
	})
});

function getListShop() {
	$.getJSON("../admin/listShop", function(data, err) {
		$("#listShopUser").empty();
		var strVar = "";
		strVar += "<select id=\"shopIdUser\" name=\"shop_id\"";
		strVar += "															style=\"width: 174px; height: 26px;\">";
		strVar += "	<option value=\"0\" selected>Chọn shop<\/option>";
		strVar += "	<option value=\"-1\" >Admin<\/option>";
		$.each(data, function(index, item) {
			strVar += "	<option value=\"" + item.shop_id + "\">" + item.name
					+ "<\/option>";
		});
		strVar += "														<\/select>";
		$("#listShopUser").append(strVar);
	});
};

function createUser() {
	var object = new Object();
	object.userName = $("#usernameUser").val();
	object.passWord = $("#passwordUser").val();
	object.shop_id = $("#shopIdUser").val();
	object.authority = $("#authorityUser").val();
	var enabled=$("input[name='cbxEnabledUser']:checked").val();
	if(enabled==0){
		object.enabled = false;
	}else if(enabled==1){
		object.enabled = true;
	}	
	if (object.userName != "" && object.passWord != "" && object.shop_id!=0) {
		var r = confirm("Bạn có muốn lưu lại không?");
		if (r == true) {
			$.ajax({
				type : "POST",
				contentType : "application/json",
				url : "../admin/createUser",
				data : JSON.stringify(object),
				dataType : 'json',
				timeout : 10000,
				success : function(data) {
					if (data === 200) {
						alert("Cập nhật thành công!!!");
						window.location.reload();
					} else if(data ===400){
						alert("Tài khoản đã tồn tại!!!");
					}
					else if (data === 500) {
						alert("Không thành công!!!");
					}
				},
				error : function(e) {
					console.log("error", e);
				}
			});
		}
	} else {
		alert("Kiểm tra lại dữ liệu nhập vào!!!");
	}
}