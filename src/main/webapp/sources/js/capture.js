/**
 * 
 */

// 
$(document).ready(function(){
	settingDateRangPicker();
});

function settingDateRangPicker(){
	var start= moment();
	function cd(start){
		getListCaptureById(start.format('YYYY_MM_DD'));
	};
	
	$('input[name="searchCapture"]').daterangepicker({
		singleDatePicker:true,
		showDropdowns:true
	}, cd);
	cd(start);
};

function getListCaptureById(strDate){
	var camera_id=$("#keyCameraId").val();
	$.getJSON("../listcapture/camera="+camera_id+"/date="+strDate+"", function(data, err){
		showHtmlCapture(data,"#showListCapture");
	});
};

function showHtmlCapture(data, Id){
	$(Id).empty();
	var strVar="";
	for(var i=0; i<data.length;i++){
		strVar += "<div class=\"col-md-4\">";
		strVar += "							<div>";
		strVar += "								<img src=\""+data[i].url+"\" style=\"height: 200px;\">";
		strVar += "							<\/div>";
		strVar += "							<div>";
		strVar += "								<p>"+convertTime(data[i].date)+"<\/p>";
		strVar += "							<\/div>";
		strVar += "						<\/div>";
	}
	$(Id).append(strVar);
};

function convertTime(date) {
	var d = new Date(date)
	return ("0" + d.getDate()).slice(-2) + "-"
			+ ("0" + (d.getMonth() + 1)).slice(-2) + "-" + d.getFullYear()
			+ " " + ("0" + d.getHours()).slice(-2) + ":"
			+"00";
}