/**
 * 
 */
$(document).ready(function() {
	getListShop()
	$("#btnCreateCamera").click(function() {
		insertcamera();
	})
	getListShopCam(10);
})

function updateCamera(camera_id) {
	var object= new Object();
	object.camera_id= camera_id;
	object.shop_id=$("#shopIdCamera").val();
	object.name=$("#unameCamera").val();
	object.place=$("#uplaceCamera").val();
	object.bios=$("#biosCamera").val();
	object.status=$("input[name='cbxStatusCam']:checked").val();
	var r = confirm("Bạn muốn lưu lại không?");
	var idTr="#lastUpdateCamera"+camera_id;
	if (r === true) {
		$.ajax({
			type : "POST",
			contentType : "application/json",
			url : "../admin/update/camera="+camera_id+"",
			data : JSON.stringify(object),
			dataType : 'json',
			timeout : 10000,
			success : function(data) {
				if (data === 200) {
					alert("Cập nhật thành công!!!");
					$.getJSON("../admin/camera/" + camera_id + "", function(data, err) {
						showHtmLastUpdateCam(data, idTr);
					});
				} else if (data === 500) {
					alert("Không thành công!!!");
				}
			},
			error : function(e) {
				console.log("error", e);
			}
		})
	}else{
		alert("Kiểm tra nhập liệu!");
	}
};

function showHtmLastUpdateCam(item, id) {
	$(id).empty();
	var strVar="";
	strVar += "															<td><\/td>";
	strVar += "<td>" + item.camera_id + "<\/td>";
	strVar += "															<td>" + item.name + "<\/td>";
	strVar += "															<td> " + item.place
			+ "<\/td>";
	strVar += "															<td>" + item.bios + "<\/td>";
	strVar += "<td>" + convertTime(item.last_time)
			+ "<\/td>";
	strVar += "<td>" + item.status + "<\/td>";
	strVar += "															<td>";
	strVar += "<button onclick=\"clickDetailCamera("
			+ item.camera_id
			+ ")\" type=\"button\" class=\"btn btn-info btn-lg\"><span class=\"glyphicon glyphicon-edit\"><\/span><\/button><\/td>";	
	$(id).append(strVar);
}
function clickDetailCamera(camera_id) {
	$.getJSON("../admin/camera/" + camera_id + "", function(data, err) {
		showDetailCamera("#showDetailCamera", data);
	});
	$("#myModal").modal();
};

function showDetailCamera(id, data) {
	$(id).empty();
	var strVar = "";
	strVar += "<table class=\"table table-bordered\" style=\"margin-top: 30px;\">";
	strVar += "												<tr>";
	strVar += "<td><strong>Shop_id:<\/strong><\/td>";
	strVar += "<td><input id=\"shopIdCamera\" value=\"" + data.shop_id
			+ "\" \/><\/td>";
	strVar += "<td><strong>Camera_id<\/strong><\/td>";
	strVar += "<td><input id=\"idCamera\" value=\"" + data.camera_id
			+ "\"<\/td>";
	strVar += "<\/tr><tr>";
	strVar += "													<td><strong>Name:<\/strong><\/td>";
	strVar += "													<td><input id=\"unameCamera\" value=\"" + data.name
			+ "\" \/><\/td>";
	strVar += "													<td><strong>Place<\/strong><\/td>";
	strVar += "													<td><input id=\"uplaceCamera\" value=\"" + data.place
			+ "\" \/><\/td>";
	strVar += "												<\/tr>";
	strVar += "												<tr>";
	strVar += "													<td><strong>Bios<\/strong><\/td>";
	strVar += "													<td><input id=\"biosCamera\" value=\"" + data.bios
			+ "\" \/><\/td>";
	strVar += "<td><strong>Status<\/strong><\/td>";
	strVar += "<td>";
	if (data.status === 1) {
		strVar += "											<div class=\"col-md-3\">";
		strVar += "												<strong>Kết Nối<\/strong>";
		strVar += "											<\/div>";
		strVar += "											<div class=\"col-md-1\">";
		strVar += "												<input type=\"radio\" name=\"cbxStatusCam\" value=\"1\" checked \/>";
		strVar += "											<\/div>";
		strVar += "											<div class=\"col-md-3\">";
		strVar += "												<strong>Hủy Kết Nối<\/strong>";
		strVar += "											<\/div>";
		strVar += "											<div class=\"col-md-1\">";
		strVar += "												<input type=\"radio\" name=\"cbxStatusCam\" value=\"0\" \/>";
		strVar += "											<\/div>";
	} else if (data.status === 0) {
		strVar += "											<div class=\"col-md-3\">";
		strVar += "												<strong>Kết nối<\/strong>";
		strVar += "											<\/div>";
		strVar += "											<div class=\"col-md-1\">";
		strVar += "												<input type=\"radio\" name=\"cbxStatusCam\" value=\"1\" \/>";
		strVar += "											<\/div>";
		strVar += "											<div class=\"col-md-3\">";
		strVar += "												<strong>Hủy kết nối<\/strong>";
		strVar += "											<\/div>";
		strVar += "											<div class=\"col-md-1\">";
		strVar += "												<input type=\"radio\" name=\"cbxStatusCam\" value=\"0\" checked \/>";
		strVar += "											<\/div>";
	} 
	strVar+="<\/td>";
	strVar += "												<\/tr>";
	strVar += "<tr><td><\/td>";
	strVar += "													<td><\/td>";
	strVar += "<td><\/td>";
	strVar += "													<td><button onclick=\"updateCamera("
			+ data.camera_id + ")\"";
	strVar += "															class=\"btn btn-block btn-info\" style=\"width: 80px;\">Save<\/button><\/td>";
	strVar += "<\/tr>";
	strVar += "											<\/table>";
	$(id).append(strVar);
};

function clickShowCam(shop_id) {
	var url = "../admin/camera/shop=" + shop_id + "";
	$.getJSON(url, function(data, err) {
		$('#showListCamera').pagination({
			dataSource : data,
			totalNumber : data.length,
			pageSize : 10,
			ajax : {
				beforeSend : function() {
					container.prev().html('Loading data from ...');
				}
			},
			callback : function(data, pagination) {
				showHtmlListCamera(data, pagination, '#showListCamera');
			}
		});
	});
};

function showHtmlListCamera(data, pagination, strId) {
	var strVar = "";
	strVar += "<thead>";
	strVar += "													<tr>";
	strVar += "														<th><\/th>";
	strVar += "														<th>camera_id<\/th>";
	strVar += "<th>Name<\/th>";
	strVar += "														<th>Place<\/th>";
	strVar += "														<th>Bios<\/th>";
	strVar += "<th>Last_time<\/th>";
	strVar += "<th>Status<\/th>";
	strVar += "														<th><\/th>";
	strVar += "													<\/tr>";
	strVar += "												<\/thead>";
	strVar += "												<tbody>";
	$
			.each(
					data,
					function(index, item) {
						strVar += "													<tr id=\"lastUpdateCamera"
								+ item.camera_id + "\">";
						strVar += "															<td><\/td>";
						strVar += "<td>" + item.camera_id + "<\/td>";
						strVar += "															<td>" + item.name + "<\/td>";
						strVar += "															<td> " + item.place
								+ "<\/td>";
						strVar += "															<td>" + item.bios + "<\/td>";
						strVar += "<td>" + convertTime(item.last_time)
								+ "<\/td>";
						strVar += "<td>" + item.status + "<\/td>";
						strVar += "															<td><div>";
						
						strVar += "<button onclick=\"clickDetailCamera("
								+ item.camera_id
								+ ")\" type=\"button\" class=\"btn btn-info\"><span class=\"glyphicon glyphicon-edit\"><\/span><\/button>";
						strVar += "<a href=\"../admin/capture/camera="+item.camera_id+"\"><button type=\"button\" class=\"btn btn-block btn-success\" style=\"width: 40px;\"><span class=\"fa fa-fw fa-file-photo-o\"><\/span><\/button></a>";
						strVar+="<\/div><\/td>";
						strVar += "														<\/tr>";
					});
	strVar += "<\/tbody>";
	$(strId).prev().html(strVar);
};

function getListShop() {
	$.getJSON("../admin/listShop", function(data, err) {
		$("#listShopUser").empty();
		var strVar = "";
		strVar += "<select id=\"shopIdUser\" name=\"shop_id\"";
		strVar += "															style=\"width: 174px; height: 26px;\">";
		strVar += "	<option value=\"0\" selected>Chọn shop<\/option>";
		$.each(data, function(index, item) {
			strVar += "	<option value=\"" + item.shop_id + "\">" + item.name
					+ "<\/option>";
		});
		strVar += "														<\/select>";
		$("#listShopUser").append(strVar);
	});
};

function insertcamera() {
	var object = new Object();
	object.shop_id = $("#shopIdUser").val();
	alert(object.shop_id);
	object.name = $("#nameCamera").val();
	object.place = $("#placeCamera").val();
	if (object.shop_id != 0 && object.name != "" && object.place != "") {
		var r = confirm("Bạn có muốn lưu lại không?");
		if (r == true) {
			$.ajax({
				type : "POST",
				contentType : "application/json",
				url : "../admin/create/camera",
				data : JSON.stringify(object),
				dataType : 'json',
				timeout : 10000,
				success : function(data) {
					if (data === 200) {
						alert("Tạo camera thành công!");
					} else if (data === 500) {
						alert("Không thành công!!!");
					}
				},
				error : function(e) {
					console.log("error", e);
				}
			});
		}
	} else {
		alert("Kiểm tra lại dữ liệu!!!");
	}
};

function showHtmlListShop(data, pagination, strId) {
	var strVar = "";
	$
			.each(
					data,
					function(index, item) {
						strVar += "<div style=\"margin-bottom: 5px;\">";
						strVar += "  <button onclick=\"clickShowCam("
								+ item.shop_id
								+ ")\" class=\"\" type=\"button\" data-toggle=\"dropdown\" style=\"width: 150px;\">"
								+ item.shop_id + ":" + item.name + "";
						strVar += "<\/button>";
						strVar += "<div>";
						strVar += "<\/div>";
						strVar += "<\/div>";
					});
	$(strId).prev().html(strVar);
};

function getListShopCam(size) {
	var url = "../admin/getListShop";
	$.getJSON(url, function(data, err) {
		$('#listShop').pagination({
			dataSource : data,
			totalNumber : data.length,
			pageSize : size,
			ajax : {
				beforeSend : function() {
					container.prev().html('Loading data from ...');
				}
			},
			callback : function(data, pagination) {
				showHtmlListShop(data, pagination, '#listShop');
			}
		});
	});
}

function convertTime(date) {
	var d = new Date(date)
	return ("0" + d.getDate()).slice(-2) + "-"
			+ ("0" + (d.getMonth() + 1)).slice(-2) + "-" + d.getFullYear()
			+ " " + ("0" + d.getHours()).slice(-2) + ":"
			+ ("0" + d.getMinutes()).slice(-2) + ":"
			+ ("0" + d.getSeconds()).slice(-2);
}
