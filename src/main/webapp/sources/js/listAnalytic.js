$(document).ready(function() {
	var time = new Date().getTime();
	$(document.body).bind("mousemove keypress", function(e) {
		time = new Date().getTime();
	});

	function refresh() {
		if (new Date().getTime() - time >= 60000)
			window.location.reload(true);
		else
			setTimeout(refresh, 10000);
	}
	setTimeout(refresh, 10000);

	$(document).ajaxStart(function() {
		$("#loadingDibLast").css("display", "none");
		$("#loadingDiv").css("display", "block");
	});
	$(document).ajaxStop(function() {
		$("#loadingDiv").css("display", "none");
		$("#loadingDibLast").css("display", "block");
	});

	settingDateRangPicker();

	$("#deleteAll").click(function() {
		deleteCustomerAll("#deleteAna");
		$("#deleteAnalytic").modal();
	});

	$("#checkedAll").change(function() {
		if (this.checked) {
			$(".checkSingle").each(function() {
				this.checked = true;
			})
		} else {
			$(".checkSingle").each(function() {
				this.checked = false;
			})
		}
		;
		if (this.checked) {
			document.getElementById("deleteAll").disabled = false;
		} else {
			document.getElementById("deleteAll").disabled = true;
		}
		;
	});

});

function clickCheckBox() {
	$(".checkSingle").change(function() {
		if (this.checked) {
			document.getElementById("deleteAll").disabled = false;
			var isAllChecked = 0;
			$(".checkSingle").each(function() {
				if (!this.checked) {
					isAllChecked = 1;
				}
			})
			if (isAllChecked == 0) {
				$("#checkedAll").prop("checked", true);
			}
		} else if (!this.checked) {
			$("#checkedAll").prop("checked", false)
			var totalChecked;
			totalChecked = $(".checkSingle:checked").length;
			if (totalChecked == 0) {
				document.getElementById("deleteAll").disabled = true;
			}
		}
	});
}

function showHtmlStatisSum(startDate) {
	$
			.getJSON(
					"../user/statistic/sum/" + startDate + "_" + startDate,
					function(data, err) {
						$("#showHtmlSum").empty();
						var strVar = "";
						strVar += "					<div class=\"col-md-3 col-sm-6 col-xs-12\">";
						strVar += "						<div class=\"info-box\">";
						strVar += "							<span class=\"info-box-icon bg-aqua\"><i class=\"fa fa-fw fa-file-image-o\" style=\"margin-top: 22px;\"><\/i><\/span>";
						strVar += "							<div class=\"info-box-content\">";
						strVar += "								<span class=\"info-box-text\">Tổng số bức ảnh:<\/span> <span";
						strVar += "									class=\"info-box-number\">"
								+ data[0].quatity
								+ "<small> Ảnh<\/small><\/span>";
						strVar += "							<\/div>";
						strVar += "							<!-- \/.info-box-content -->";
						strVar += "						<\/div>";
						strVar += "						<!-- \/.info-box -->";
						strVar += "					<\/div>";
						strVar += "					<div class=\"col-md-3 col-sm-6 col-xs-12\">";
						strVar += "						<div class=\"info-box\">";
						strVar += "							<span class=\"info-box-icon bg-red\"><i";
						strVar += "								class=\"fa fa-fw fa-transgender\" style=\"margin-top: 22px;\"><\/i><\/span>";
						strVar += "							<div class=\"info-box-content\">";
						strVar += "								<span class=\"info-box-text\">Male: <\/span> <strong>"
								+ data[1].quatity
								+ "<small> Ảnh<\/small><\/strong>";
						strVar += "								<span class=\"info-box-text\">Female: <\/span> <strong>"
								+ data[2].quatity
								+ "<small> Ảnh<\/small><\/strong>";
						strVar += "							<\/div>";
						strVar += "							<!-- \/.info-box-content -->";
						strVar += "						<\/div>";
						strVar += "						<!-- \/.info-box -->";
						strVar += "					<\/div>";
						strVar += "					<div class=\"col-md-3 col-sm-6 col-xs-12\">";
						strVar += "						<div class=\"info-box\">";
						strVar += "							<span class=\"info-box-icon bg-green\">";
						// strVar+="<img src=\"#\" \/>";
						strVar += "								<i class=\"ion ion-ios-gear-outline\" style=\"margin-top: 22px;\"><\/i>";

						strVar += "<\/span>";
						strVar += "							<div class=\"info-box-content\">";
						strVar += "								<span class=\"info-box-text\">Độ tuổi phổ biến:<\/span> <span";
						strVar += "									class=\"info-box-number\">";
						if (data[0].quatity == 0) {
							strVar += 0;
						} else {
							strVar += data[3].name + ": " + data[3].quatity;
						}
						strVar += "<small> Ảnh<\/small><\/span>";
						strVar += "							<\/div>";
						strVar += "							<!-- \/.info-box-content -->";
						strVar += "						<\/div>";
						strVar += "						<!-- \/.info-box -->";
						strVar += "					<\/div>";
						strVar += "					<div class=\"col-md-3 col-sm-6 col-xs-12\">";
						strVar += "						<div class=\"info-box\">";
						strVar += "							<span class=\"info-box-icon bg-yellow\"><i";
						strVar += "								class=\"fa fa-fw fa-smile-o\" style=\"margin-top: 22px;\"><\/i><\/span>";
						strVar += "							<div class=\"info-box-content\">";
						strVar += "								<span class=\"info-box-text\">Cảm xúc phổ biến:<\/span> <span";
						strVar += "									class=\"info-box-number\">";
						if (data[0].quatity == 0) {
							strVar += 0;
						} else {
							strVar += data[4].name + ": " + data[4].quatity;
						}
						strVar += "<small> Ảnh<\/small><\/span>";
						strVar += "							<\/div>";
						strVar += "							<!-- \/.info-box-content -->";
						strVar += "						<\/div>";
						strVar += "					<\/div>";
						$("#showHtmlSum").append(strVar);
					});
};

function showHtmlStatisSumCam(shop_id, startDate) {
	$
			.getJSON(
					"../user/shop_camera=" + shop_id + "/sum/" + startDate,
					function(data, err) {
						$("#showHtmlSum").empty();
						var strVar = "";
						strVar += "					<div class=\"col-md-3 col-sm-6 col-xs-12\">";
						strVar += "						<div class=\"info-box\">";
						strVar += "							<span class=\"info-box-icon bg-aqua\"><i class=\"fa fa-fw fa-file-image-o\" style=\"margin-top: 22px;\"><\/i><\/span>";
						strVar += "							<div class=\"info-box-content\">";
						strVar += "								<span class=\"info-box-text\">Tổng số bức ảnh:<\/span> <span";
						strVar += "									class=\"info-box-number\">"
								+ data[0].quatity
								+ "<small> Ảnh<\/small><\/span>";
						strVar += "							<\/div>";
						strVar += "							<!-- \/.info-box-content -->";
						strVar += "						<\/div>";
						strVar += "						<!-- \/.info-box -->";
						strVar += "					<\/div>";
						strVar += "					<div class=\"col-md-3 col-sm-6 col-xs-12\">";
						strVar += "						<div class=\"info-box\">";
						strVar += "							<span class=\"info-box-icon bg-red\"><i";
						strVar += "								class=\"fa fa-fw fa-transgender\" style=\"margin-top: 22px;\"><\/i><\/span>";
						strVar += "							<div class=\"info-box-content\">";
						strVar += "								<span class=\"info-box-text\">Male: <\/span> <strong>"
								+ data[1].quatity
								+ "<small> Ảnh<\/small><\/strong>";
						strVar += "								<span class=\"info-box-text\">Female: <\/span> <strong>"
								+ data[2].quatity
								+ "<small> Ảnh<\/small><\/strong>";
						strVar += "							<\/div>";
						strVar += "							<!-- \/.info-box-content -->";
						strVar += "						<\/div>";
						strVar += "						<!-- \/.info-box -->";
						strVar += "					<\/div>";
						strVar += "					<div class=\"col-md-3 col-sm-6 col-xs-12\">";
						strVar += "						<div class=\"info-box\">";
						strVar += "							<span class=\"info-box-icon bg-green\"><i";
						strVar += "								class=\"ion ion-ios-gear-outline\" style=\"margin-top: 22px;\"><\/i><\/span>";
						strVar += "							<div class=\"info-box-content\">";
						strVar += "								<span class=\"info-box-text\">Độ tuổi phổ biến:<\/span> <span";
						strVar += "									class=\"info-box-number\">";
						if (data[0].quatity == 0) {
							strVar += 0;
						} else {
							strVar += data[3].name + ": " + data[3].quatity;
						}
						strVar += "<small> Ảnh<\/small><\/span>";
						strVar += "							<\/div>";
						strVar += "							<!-- \/.info-box-content -->";
						strVar += "						<\/div>";
						strVar += "						<!-- \/.info-box -->";
						strVar += "					<\/div>";
						strVar += "					<div class=\"col-md-3 col-sm-6 col-xs-12\">";
						strVar += "						<div class=\"info-box\">";
						strVar += "							<span class=\"info-box-icon bg-yellow\"><i";
						strVar += "								class=\"fa fa-fw fa-smile-o\" style=\"margin-top: 22px;\"><\/i><\/span>";
						strVar += "							<div class=\"info-box-content\">";
						strVar += "								<span class=\"info-box-text\">Cảm xúc phổ biến:<\/span> <span";
						strVar += "									class=\"info-box-number\">";
						if (data[0].quatity == 0) {
							strVar += 0;
						} else {
							strVar += data[3].name + ": " + data[3].quatity;
						}
						strVar += "<small> Ảnh<\/small><\/span>";
						strVar += "							<\/div>";
						strVar += "							<!-- \/.info-box-content -->";
						strVar += "						<\/div>";
						strVar += "					<\/div>";
						$("#showHtmlSum").append(strVar);
					});
};

function settingDateRangPicker() {
	var start = moment();
	function cd(start) {
		$('#getDateSearch span').html(start.format('DD/MM/YYYY'));
		$("#checkedAll").prop("checked", false);
		document.getElementById("deleteAll").disabled = true;
		getListAnalytic(start.format('YYYY-MM-DD'), 0);
		/*
		 * $("#applySimilarity").click(function() { var filter =
		 * $("#inputSimilarity").val();
		 * getListAnalytic(start.format('YYYY-MM-DD'), filter); });
		 */
	}
	;
	$('#getDateSearch').daterangepicker({
		singleDatePicker : true,
		showDropdowns : true
	}, cd);
	cd(start);
};

function clickUpdate(id) {
	var object = new Object();
	object.id = id;
	object.gender = $('#genderAnalytic').val();
	if (object.gender == "Male" || object.gender == "Female") {
		var check = 1;
	}
	object.age = $("#ageAnalytic").val();
	object.name = $("#nameAnalytic").val();
	object.status = $("input[name='cbxStatus']:checked").val();
	var idTr = "#lastUpdate" + id;
	var r = confirm("Bạn muốn cập nhật không?");
	// $("#updateAnalytic").modal();
	if (object.age > 0 && object.age < 100) {
		if (check == 1) {
			if (r === true) {
				$.ajax({
					type : "POST",
					contentType : "application/json",
					url : "../user/update/image=" + id + "",
					data : JSON.stringify(object),
					dataType : 'json',
					timeout : 10000,
					success : function(data) {
						if (data === 200) {
							$("#myModal").modal('toggle');
							$.getJSON("../customer/" + id + "", function(data,
									err) {
								showAnalyticLastUpdate(data, idTr);
							});
							messageNotify("Cập nhật thành công.", "success");
						} else if (data === 500) {
							messageNotify("Cập nhật không thành công!",
									"danger");
						}
					},
					error : function(e) {
						console.log("danger", e);
					}
				});
			}
		}
	} else {
		alert("Tuổi phải là số thuộc từ 0 đến 100!!!");
	}
}

function showAnalyticLastUpdate(item, id) {
	$(id).empty();
	var strVar = "";
	strVar += " <td><input type=\"checkbox\" name=\"checkAll\" class=\"checkSingle\" onclick=\"clickCheckBox()\" value=\""
			+ item.id + "\" \/><\/td>";
	strVar += "															<td>";
	strVar += "																<div>";
	strVar += "																	<div>";
	strVar += "<img src=\"" + item.image_new + "\" style=\" height: 90px;\">";
	strVar += "																	<\/div>";
	strVar += "																	<div>" + convertTime(item.datetime_new)
			+ "<\/div>";
	strVar += "																<\/div>";
	strVar += "															<\/td>";
	strVar += "<td>" + item.name + "<\/td>";
	strVar += "															<td>" + item.gender + "<\/td>";
	strVar += "															<td>" + item.age + "-" + (item.age + 5)
			+ "<\/td>";
	strVar += "															<td>" + item.emotion + "<\/td>";
	strVar += "															<td>";
	strVar += "																<div>";
	strVar += "																	<div>";
	strVar += "<img src=\"" + item.image_old + "\" style=\" height: 90px;\">";
	strVar += "																	<\/div>";
	strVar += "																	<div>" + convertTime(item.datetime_old)
			+ "<\/div>";
	strVar += "																<\/div>";
	strVar += "															<\/td>";
	strVar += "<td>" + item.similarity + "<\/td>";
	strVar += "															<td class=\"row\" style=\"width:12%;\">";
	strVar += "<div style=\"width: 55%;float: left;\">";
	strVar += "<button onclick=\"clickShowPop("
			+ item.id
			+ ")\" class=\"btn btn-block btn-info btn-lg\"><i>Edit<\/i><\/button>";
	strVar += "<\/div><div style=\"width: 40%;float: right;\">";
	strVar += "<button onclick=\"deleteCustomer(" + item.id
			+ ")\" class=\"btn btn-block btn-warning btn-lg\">Lọc bỏ<\/button>";
	strVar += "<\/div><\/td>";
	$(id).append(strVar);
}

function clickShowPop(id) {
	$.getJSON("../customer/" + id + "", function(data, err) {
		showDetailAnalytic("#showDetailAnalytic", data);
	});
	$("#myModal").modal();
}

function showDetailAnalytic(id, data) {
	$(id).empty();
	var strVar = "";
	strVar += "<div class=\"\" style=\"font-size: 15px;\">";
	strVar += "				<div class=\"box\">";
	// strVar += " <form method=\"post\"";
	// strVar += " action=\"..\/user\/update\/image=" + data.id + "\">";
	strVar += "						<div class=\"box-body \">";
	strVar += "							<div class=\"col=md-12 row\">";
	strVar += "								<div class=\"col-md-1\"><\/div>";
	strVar += "								<div class=\"col-md-5\">";
	strVar += "									<div>";
	strVar += "										<img src=\"" + data.image_new + "\" ";
	strVar += "											style=\"width: 250px;\" \/>";
	strVar += "									<\/div>";
	strVar += "									<div>";
	strVar += "										<strong>Ngày chụp: " + convertTime(data.datetime_new)
			+ "<\/strong>";
	strVar += "									<\/div>";
	strVar += "								<\/div>";
	strVar += "								<div class=\"col-md-1\"><\/div>";
	strVar += "								<div class=\"col-md-5\">";
	strVar += "									<div>";
	strVar += "										<img src=\"" + data.image_old + "\" ";
	strVar += "											style=\"width: 250px;\" \/>";
	strVar += "									<\/div>";
	if (data.datetime_old != null) {
		strVar += "									<div>";
		strVar += "										<strong>Ngày chụp: ";
		strVar += convertTime(data.datetime_old);
		strVar += "<\/strong><\/div>";
	}
	strVar += "								<\/div>";
	strVar += "							<\/div>";
	strVar += "							<div class=\"col=md-12 row\" style=\"\">";
	strVar += "								<div class=\"col-md-6\">";
	strVar += "									<div class=\"row\" style=\"margin-top: 15px;\">";
	strVar += "										<div class=\"col-md-1\"><\/div>";
	strVar += "										<div class=\"col-md-4\">";
	strVar += "											<strong>Giới tính<\/strong>";
	strVar += "										<\/div>";
	strVar += "										<div class=\"col-md-6\">";
	if (data.gender === "Male") {
		strVar += "												<select id=\"genderAnalytic\" name=\"gender\" style=\"width: 195px;\">";
		strVar += "													<option value=\"Male\" selected>Male<\/option>";
		strVar += "													<option value=\"Female\">Female<\/option>";
		strVar += "												<\/select>";
	} else if (data.gender === "Female") {
		strVar += "												<select id=\"genderAnalytic\" name=\"gender\" style=\"width: 195px;\">";
		strVar += "													<option value=\"Male\">Male<\/option>";
		strVar += "													<option value=\"Female\" selected>Female<\/option>";
		strVar += "												<\/select>";
	}
	strVar += "										<\/div>";
	strVar += "									<\/div>";
	strVar += "									<div class=\"row\" style=\"margin-top: 15px;\">";
	strVar += "										<div class=\"col-md-1\"><\/div>";
	strVar += "										<div class=\"col-md-4\">";
	strVar += "											<strong>Tuổi<\/strong>";
	strVar += "										<\/div>";
	strVar += "										<div class=\"col-md-6\">";
	strVar += "<input id=\"ageAnalytic\"name=\"age\" value=\""
			+ data.age
			+ "\" min=\"0\" max=\"100\" type=\"number\" style=\"width: 195px;\"\/>";
	strVar += "										<\/div>";
	strVar += "									<\/div>";
	strVar += "									<div class=\"row\" style=\"margin-top: 15px;\">";
	strVar += "										<div class=\"col-md-1\"><\/div>";
	strVar += "										<div class=\"col-md-4\">";
	strVar += "											<strong>Họ và tên<\/strong>";
	strVar += "										<\/div>";
	strVar += "										<div class=\"col-md-6\">";
	if (data.name == "" || data.name == null) {
		strVar += "											<input id=\"nameAnalytic\" type=\"text\" name=\"nameImage\"";
		strVar += "												value=\"\" placeholder=\"Nhập tên\"\/>";
	} else {
		strVar += "											<input id=\"nameAnalytic\" type=\"text\" name=\"nameImage\"";
		strVar += "												value=\"" + data.name + "\" \/>";
	}
	strVar += "										<\/div>";
	strVar += "									<\/div>";
	strVar += "								<\/div>";
	strVar += "								<div class=\"col-md-6\">";
	if (data.status === -1) {
		strVar += "											<div class=\"row\" style=\"margin-top:15px;\">";
		strVar += "<div class=\"col-md-1\"><\/div>";
		strVar += "<div class=\"col-md-2\">";
		strVar += "												<strong>Đúng<\/strong>";
		strVar += "<\/div><div class=\"col-md-1\">";
		strVar += "												<input type=\"radio\" name=\"cbxStatus\" value=\"1\"  \/>";
		strVar += "<\/div>";
		strVar += "											<\/div>";
		strVar += "											<div class=\"row\" style=\"margin-top:15px;\">";
		strVar += "<div class=\"col-md-1\"><\/div>";
		strVar += "<div class=\"col-md-2\">";
		strVar += "												<strong>Sai<\/strong>";
		strVar += "<\/div><div class=\"col-md-1\">";
		strVar += "												<input type=\"radio\" name=\"cbxStatus\" value=\"-1\" checked\/>";
		strVar += "<\/div>";
		strVar += "											<\/div>";
	} else if (data.status === 0 || data.status === 1) {
		strVar += "											<div class=\"row\" style=\"margin-top:15px;\">";
		strVar += "<div class=\"col-md-1\"><\/div>";
		strVar += "<div class=\"col-md-2\">";
		strVar += "												<strong>Đúng<\/strong>";
		strVar += "<\/div><div class=\"col-md-1\">";
		strVar += "												<input type=\"radio\" name=\"cbxStatus\" value=\"1\" checked \/>";
		strVar += "<\/div>";
		strVar += "											<\/div>";
		strVar += "											<div class=\"row\" style=\"margin-top:15px;\">";
		strVar += "<div class=\"col-md-1\"><\/div>";
		strVar += "<div class=\"col-md-2\">";
		strVar += "												<strong>Sai<\/strong>";
		strVar += "<\/div><div class=\"col-md-1\">";
		strVar += "												<input type=\"radio\" name=\"cbxStatus\" value=\"-1\" \/>";
		strVar += "<\/div>";
		strVar += "											<\/div>";
	}
	strVar += "									<div  style=\"margin-top: 30px;\">";
	strVar += "										<input class=\"btn btn-block btn-info btn-lg\" onclick=\"clickUpdate("
			+ data.id
			+ ")\" type=\"submit\" value=\"Cập nhật\" id=\"btnUpdate\" style=\"margin-left: 10%; width: 50%;\"\/>";
	strVar += "									<\/div>";
	strVar += "								<\/div>";
	strVar += "							<\/div>";
	strVar += "						<\/div>";
	// strVar += " <\/form>";
	strVar += "				<\/div>";
	strVar += "			<\/div>";
	$(id).append(strVar);
}

function showHtmlList(data, strId) {
	var timeNew = new Date();
	$(strId).empty();
	var strVar = "";
	var i = 0;
	$
			.each(
					data,
					function(index, item) {
						var idTr = "lastUpdate" + item.id;
						i += 1;
						strVar += "													<tr id=\"" + idTr + "\">";
						strVar += " <td><input type=\"checkbox\" name=\"checkAll\" class=\"checkSingle\" value=\""
								+ item.id
								+ "\" onclick=\"clickCheckBox()\"\/><\/td>";
						strVar += "															<td class=\"\">";
						var txtTitle;
						if (timeNew.getDate() == convertDate(item.datetime_new)
								.getDate()) {
							txtTitle = minusTwoDate(timeNew,
									convertDate(item.datetime_new));
						} else {
							txtTitle = "";
						}
						strVar += "<a href=\"#\" title=\"" + txtTitle
								+ "\" style=\"color:#000;\">";
						strVar += "																<div>";
						strVar += "																	<div>";
						strVar += "<img src=\"" + item.image_new
								+ "\" style=\" height: 90px;\">";
						strVar += "																	<\/div>";
						strVar += "																	<div class=\"\">";
						strVar += convertTime(item.datetime_new);
						strVar += "<\/div>";
						strVar += "<\/div>";
						strVar += "<\/a>";
						strVar += "															<\/td>";
						strVar += "<td>" + item.name + "<\/td>";
						strVar += "															<td>" + item.gender
								+ "<\/td>";
						strVar += "															<td>" + item.age + "-"
								+ (item.age + 5) + "<\/td>";
						strVar += "															<td>" + item.emotion
								+ "<\/td>";
						strVar += "															<td>";
						strVar += "																<div>";
						strVar += "																	<div>";
						strVar += "<img src=\"" + item.image_old
								+ "\" style=\" height: 90px;\">";
						strVar += "																	<\/div>";
						strVar += "																	<div>";
						if (item.datetime_old != null) {
							strVar += convertTime(item.datetime_old);
						}
						;
						strVar += "	<\/div><\/div>";
						strVar += "															<\/td>";
						strVar += "<td>" + item.similarity + "<\/td>";
						strVar += "															<td class=\"row\" style=\"width:12%;\">";
						strVar += "<div style=\"width: 55%;float: left;\">";
						strVar += "<button onclick=\"clickShowPop("
								+ item.id
								+ ")\" class=\"btn btn-block btn-info btn-lg\" style=\"font-size: 100%;\">Chi tiết<\/button>";
						strVar += "<\/div><div style=\"width: 40%;float: right;\" >";
						strVar += "<button onclick=\"deleteCustomer("
								+ item.id
								+ ")\"  class=\"btn btn-block btn-warning btn-lg\" style=\"font-size: 100%;\">Lọc bỏ<\/button>";
						strVar += "<\/div><\/td>";
						strVar += "														<\/tr>";
					});
	// strVar += "<\/tbody>";
	$(strId).append(strVar);
}
/* Xóa bản ghi */
function deleteCustomer(id) {
	showViewDelete("#deleteAna", id);
	$("#deleteAnalytic").modal();
}

function deleteAnalytic(id) {
	var idTr = "#lastUpdate" + id;
	$.getJSON("../user/delete/id=" + id + "", function(data, err) {
		if (data == true) {
			messageNotify("Lọc bỏ bản ghi thành công.", "success");
			loadLastDeleteCus(idTr);
		} else if (data == false) {
			messageNotify("Lọc không thành công!", "danger");
		}
	});
}

function showViewDelete(strId, id) {
	$(strId).empty();
	var strVar = "";
	strVar += "<div class=\"col-md-6\">";
	strVar += "									<button type=\"button\" onclick=\"deleteAnalytic(" + id
			+ ")\"  data-dismiss=\"modal\"";
	strVar += "										class=\"btn btn-block btn-warning btn-lg\">Đồng ý<\/button>";
	strVar += "								<\/div>";
	strVar += "								<div class=\"col-md-6\">";
	strVar += "									<button  type=\"button\" data-dismiss=\"modal\"";
	strVar += "										class=\"btn btn-block  btn-lg\">Bỏ qua<\/button>";
	strVar += "								<\/div>";
	$(strId).append(strVar);
}

function loadLastDeleteCus(id) {
	$(id).empty();
	var strVar = "";
	strVar += "															<td><\/td>";
	strVar += "															<td>";
	strVar += "															<\/td>";
	strVar += "<td><\/td>";
	strVar += "															<td><\/td>";
	strVar += "															<td><\/td>";
	strVar += "															<td><\/td>";
	strVar += "															<td>";
	strVar += "															<\/td>";
	strVar += "<td><\/td>";
	strVar += "															<td>";
	strVar += "<\/td>";
	$(id).append(strVar);
}
/* Xóa bản ghi */

function filterData(data, filter) {
	var dataSet = new Array();
	if ($.isNumeric(filter)) {
		if (filter > 0 && filter <= 100) {
			$.each(data, function(index, item) {
				if (item.similarity >= filter) {
					dataSet.push(item);
				}
			});
		} else {
			dataSet = data;
		}
	} else {
		dataSet = data;
	}
	return dataSet;
}

function getListAnalytic(strDate, filter) {
	var size = $("#pagSize").val();
	getListAnalyticHome(strDate, filter, size);
	getListAnalyticByShopAndDate(strDate, filter, size);
	getListAnalyticByCameraAndDate(strDate, filter, size);
	$('select').on('change', function(e) {
		var val = this.value;
		$("#checkedAll").prop("checked", false);
		document.getElementById("deleteAll").disabled = true;
		getListAnalyticHome(strDate, filter, val);
		getListAnalyticByShopAndDate(strDate, filter, val);
		getListAnalyticByCameraAndDate(strDate, filter, val);
	});
}

function getListAnalyticHome(strDate, filter, size) {
	var shop_id = $("#keyShopId").val();
	if (shop_id == null || shop_id == "") {
		showHtmlStatisSum(strDate);
		var url = "../user/home/date=" + strDate;
		$.getJSON(url, function(data, err) {
			paginationShow(data, size);
		});
	}
};

function getListAnalyticByShopAndDate(strDate, filter, size) {
	var shop_id = $("#keyShopId").val();
	var n = shop_id.includes("-");
	if (shop_id == "" || shop_id == null || n == true) {
	} else {
		showHtmlStatisSum(strDate);
		var url = "../user/shop=" + shop_id + "/date=" + strDate;
		$.getJSON(url, function(data, err) {
			paginationShow(data, size);
		});
	}
};

function getListAnalyticByCameraAndDate(strDate, filter, size) {
	var shop_id = $("#keyShopId").val();
	var n = shop_id.includes("-");
	if (shop_id == "" || shop_id == null || n == false) {
	} else {
		var url = "../user/shop_camera=" + shop_id + "/date=" + strDate;
		showHtmlStatisSumCam(shop_id, strDate);
		$.getJSON(url, function(data, err) {
			paginationShow(data, size);
		});
	}
};

function paginationShow(data, size) {
	$('.showlistAnalytic').pagination({
		dataSource : data,
		totalNumber : data.length,
		pageSize : size,
		ajax : {
			beforeSend : function() {
				container.prev().html('Loading data from flickr.com ...');
			}
		},
		callback : function(data, pagination) {
			showHtmlList(data, '#tableAnalytic > tbody');
		}
	});
}
// convert datetime
function convertTime(date) {
	var d = new Date(date)
	return ("0" + d.getDate()).slice(-2) + "-"
			+ ("0" + (d.getMonth() + 1)).slice(-2) + "-" + d.getFullYear()
			+ " " + ("0" + d.getHours()).slice(-2) + ":"
			+ ("0" + d.getMinutes()).slice(-2) + ":"
			+ ("0" + d.getSeconds()).slice(-2);
};

function convertDate(date) {
	return new Date(date);
};

function minusTwoDate(date1, date2) {
	var diff = Math.abs(date1 - date2);
	var t = parseInt(diff / 1000);
	var h = parseInt(t / 3600);
	var m = parseInt((t - h * 3600) / 60);
	var s = t - h * 3600 - m * 60;
	return ("0" + h).slice(-2) + " giờ " + ("0" + m).slice(-2) + " phút "
			+ ("0" + s).slice(-2) + "giây trước";
};
/*
 * Thông báo kết quả
 * 
 */
function messageNotify(mess, typeS) {
	$
			.notify(
					{
						// options
						icon : '',
						title : '',
						message : '' + mess + '',
						url : '',
						target : ''
					},
					{
						// settings
						element : 'body',
						position : null,
						type : "" + typeS + "",
						allow_dismiss : true,
						newest_on_top : false,
						showProgressbar : false,
						placement : {
							from : "top",
							align : "right"
						},
						offset : 20,
						spacing : 10,
						z_index : 1031,
						delay : 2000,
						timer : 1000,
						url_target : '_blank',
						mouse_over : null,
						animate : {
							enter : 'animated fadeInDown',
							exit : 'animated fadeOutUp'
						},
						onShow : null,
						onShown : null,
						onClose : null,
						onClosed : null,
						icon_type : 'class',
						template : '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">'
								+ '<span data-notify="title">{1}</span> '
								+ '<span data-notify="message">{2}</span>'
								+ '<div class="progress" data-notify="progressbar">'
								+ '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>'
								+ '</div>'
								+ '<a href="{3}" target="{4}" data-notify="url"></a>'
								+ '</div>'
					});
};

function clickDeleteAll() {
	$(".checkSingle:checked").each(function() {
		id = $(this).val();
		var idTr = "#lastUpdate" + id;
		$.getJSON("../user/delete/id=" + id + "", function(data, err) {
			if (data == true) {
				messageNotify("Lọc bỏ thành công.", "success");
				loadLastDeleteCus(idTr);
			} else if (data == false) {
				messageNotify("Lọc không thành công!", "danger");
			}
		});
		// chkArray.push($(this).val());
	});
}

function deleteCustomerAll(strId) {
	$(strId).empty();
	var strVar = "";
	strVar += "<div class=\"col-md-6\">";
	strVar += "									<button type=\"button\" onclick=\"clickDeleteAll()\" data-dismiss=\"modal\"";
	strVar += "										class=\"btn btn-block btn-warning btn-lg\">Đồng ý<\/button>";
	strVar += "								<\/div>";
	strVar += "								<div class=\"col-md-6\">";
	strVar += "									<button  type=\"button\" data-dismiss=\"modal\"";
	strVar += "										class=\"btn btn-block  btn-lg\">Bỏ qua<\/button>";
	strVar += "								<\/div>";
	$(strId).append(strVar);
}