/**
 * 
 */

$(document).ready(function() {
	var size = $("#pagSizeShop").val();
	getListShow(size);
	$("#btnCreateShop").click(function() {
		createShop();
	});
});

function updateShop(shop_id) {
	var object = new Object();
	object.shop_id = shop_id;
	object.name = $("#unameShop").val();
	object.phone = $("#uphoneShop").val();
	object.address = $("#uaddressShop").val();
	console.log(object);
	var itTr="#lastUpdateShop"+shop_id;
	var r = confirm("Bạn muốn lưu lại không?");
	if (r === true) {
		$.ajax({
			type : "POST",
			contentType : "application/json",
			url : "../admin/update/shop="+shop_id+"",
			data : JSON.stringify(object),
			dataType : 'json',
			timeout : 10000,
			success : function(data) {
				if (data === 200) {
					alert("Cập nhật thành công!!!");
					$.getJSON("../admin/shop/" + shop_id + "", function(data, err) {
						showHtmlLastUpdate(data, itTr);
					});
				} else if (data === 500) {
					alert("Không thành công!!!");
				}
			},
			error : function(e) {
				console.log("error", e);
			}
		})
	}
};

function showHtmlLastUpdate(item, id) {
	$(id).empty();
	var strVar="";
	strVar += "															<td><\/td>";
	strVar += "<td>" + item.shop_id + "<\/td>";
	strVar += "															<td>" + item.name + "<\/td>";
	strVar += "															<td> " + item.phone
			+ "<\/td>";
	strVar += "															<td>" + item.address
			+ "<\/td>";
	strVar += "															<td>";
	strVar += "<button onclick=\"clickDetailShop("
			+ item.shop_id
			+ ")\" type=\"button\" class=\"btn btn-info btn-lg\"><span class=\"glyphicon glyphicon-edit\"><\/span><\/button><\/td>";
	$(id).append(strVar);
	
};

function clickDetailShop(shop_id) {
	$.getJSON("../admin/shop/" + shop_id + "", function(data, err) {
		showDetailHtmlShop("#showDetailShop", data);
	});
	$("#myModal").modal();
};

function showDetailHtmlShop(id, data) {
	$(id).empty();
	var strVar = "";
	strVar += "<table class=\"table table-bordered\" style=\"margin-top: 30px;\">";
	strVar += "												<tr>";
	strVar += "<td><strong>Shop_id:<\/strong><\/td>";
	strVar += "<td><input id=\"idShop\" value=\"" + data.shop_id
			+ "\" \/><\/td>";
	strVar += "													<td><strong>Name:<\/strong><\/td>";
	strVar += "													<td><input id=\"unameShop\" value=\"" + data.name
			+ "\" \/><\/td>";
	strVar += "												<\/tr>";
	strVar += "												<tr>";
	strVar += "													<td><strong>Phone<\/strong><\/td>";
	strVar += "													<td><input id=\"uphoneShop\" value=\"" + data.phone
			+ "\" \/><\/td>";
	strVar += "													<td><strong>Address<\/strong><\/td>";
	strVar += "													<td><input id=\"uaddressShop\" value=\""
			+ data.address + "\" \/><\/td>";
	strVar += "												<\/tr>";
	strVar += "<tr><td><\/td>";
	strVar += "													<td><\/td>";
	strVar += "<td><\/td>";
	strVar += "													<td><button onclick=\"updateShop("
			+ data.shop_id + ")\"";
	strVar += "															class=\"btn btn-block btn-info\" style=\"width: 80px;\">Save<\/button><\/td>";
	strVar += "<\/tr>";
	strVar += "											<\/table>";
	$(id).append(strVar);
};

function showHtmlListShop(data, pagination, strId) {
	var strVar = "";
	strVar += "<thead>";
	strVar += "													<tr>";
	strVar += "														<th><\/th>";
	strVar += "														<th>Shop_id<\/th>";
	strVar += "<th>Name<\/th>";
	strVar += "														<th>Phone<\/th>";
	strVar += "														<th>Address<\/th>";
	strVar += "														<th><\/th>";
	strVar += "													<\/tr>";
	strVar += "												<\/thead>";
	strVar += "												<tbody>";
	$
			.each(
					data,
					function(index, item) {
						strVar += "													<tr id=\"lastUpdateShop"+item.shop_id+"\">";
						strVar += "															<td><\/td>";
						strVar += "<td>" + item.shop_id + "<\/td>";
						strVar += "															<td>" + item.name + "<\/td>";
						strVar += "															<td> " + item.phone
								+ "<\/td>";
						strVar += "															<td>" + item.address
								+ "<\/td>";
						strVar += "															<td>";
						strVar += "<button onclick=\"clickDetailShop("
								+ item.shop_id
								+ ")\" type=\"button\" class=\"btn btn-info btn-lg\"><span class=\"glyphicon glyphicon-edit\"><\/span><\/button><\/td>";
						strVar += "														<\/tr>";
					});
	strVar += "<\/tbody>";
	$(strId).prev().html(strVar);
}

function getListShow(size) {
	var url = "../admin/getListShop";
	$.getJSON(url, function(data, err) {
		$("#listShop").pagination({
			dataSource : data,
			totalNumber : data.length,
			pageSize : size,
			ajax : {
				beforeSend : function() {
					container.prev().html('Loading data ...');
				}
			},
			callback : function(data, pagination) {
				showHtmlListShop(data, pagination, "#listShop");
			}
		});
	});
}

function createShop() {
	var object = new Object();
	object.name = $("#nameShop").val();
	object.address = $("#addressShop").val();
	object.phone = $("#phoneShop").val();
	if (object.name != "" && object.address != "" && object.phone != "") {
		var r = confirm("Bạn có muốn lưu lại không?");
		if (r == true) {
			$.ajax({
				type : "POST",
				contentType : "application/json",
				url : "../admin/createshop",
				data : JSON.stringify(object),
				dataType : 'json',
				timeout : 10000,
				success : function(data) {
					if (data === 200) {
						alert("Cập nhật thành công!!!");
						window.location.reload();
					} else if (data === 500) {
						alert("Không thành công!!!");
					}
				},
				error : function(e) {
					console.log("error", e);
				}
			});
		}
	} else {
		alert("Kiểm tra nhập liệu!");
	}
}
