/**
 * 
 */

$(document).ready(function() {
	$(document).ajaxStart(function() {
		$("#loadingDibLast").css("display", "none");
		$("#loadingDiv").css("display", "block");
	});
	$(document).ajaxStop(function() {
		$("#loadingDiv").css("display", "none");
		$("#loadingDibLast").css("display", "block");
	});
	
	getListTrash();

})

/*
 * function settingDateRangPicker() { var start = moment(); function cd(start) {
 * $('#getDateTrash span').html(start.format('DD/MM/YYYY')); } ;
 * $('#getDateTrash').daterangepicker({ singleDatePicker : true, showDropdowns :
 * true }, cd); cd(start); };
 */

function undoCustomer(id) {
	var object = new Object();
	object.id = id;
	var idTr = "#lastUpdateTrash" + id;
	$.ajax({
		type : "POST",
		contentType : "application/json",
		url : "../user/undo/id=" + id + "",
		data : JSON.stringify(object),
		dataType : 'json',
		timeout : 10000,
		success : function(data) {
			if (data === true) {
				messageNotify("Khôi phục thành công.", "warning");
				loadLastDeleteTrash(idTr)
			} else if (data === false) {
				messageNotify("Khôi phục không thành công!", "danger");
			}
		},
		error : function(e) {
			console.log("danger", e);
		}
	});
}

function loadLastDeleteTrash(id) {
	$(id).empty();
	var strVar = "";
	strVar += "															<td><\/td>";
	strVar += "															<td>";
	strVar += "															<\/td>";
	strVar += "<td><\/td>";
	strVar += "															<td><\/td>";
	strVar += "															<td><\/td>";
	strVar += "															<td><\/td>";
	strVar += "															<td>";
	strVar += "															<\/td>";
	strVar += "<td><\/td>";
	strVar += "															<td>";
	strVar += "<\/td>";
	$(id).append(strVar);
}

function getListTrash() {
	var url = "../user/trash" ;
	var size = $("#pagSize").val();
	$.getJSON(url, function(data, err) {
		paginationShow(data,size);
	});
	$('select').on('change', function(e) {
		var val = this.value;
		$.getJSON(url, function(data, err) {
			paginationShow(data,val);
		});
	});
};

function showHtmlList(data, strId) {
	$(strId).empty();
	var strVar = "";
	var i = 0;
	$
			.each(
					data,
					function(index, item) {
						var idTr = "lastUpdateTrash" + item.id;
						i += 1;
						strVar += "													<tr id=\"" + idTr + "\">";
						strVar += "<td>" + convertTime(item.datedel)
								+ "<\/td>";
						strVar += "															<td>";
						strVar += "																<div>";
						strVar += "																	<div>";
						strVar += "<img src=\"" + item.image_new
								+ "\" style=\" height: 90px;\">";
						strVar += "																	<\/div>";
						strVar += "																	<div class=\"\">";
						strVar += convertTime(item.datetime_new);
						strVar += "<\/div>";
						strVar += "<\/div>";
						strVar += "															<\/td>";
						strVar += "<td>" + item.name + "<\/td>";
						strVar += "															<td>" + item.gender
								+ "<\/td>";
						strVar += "															<td>" + item.age + "-"
								+ (item.age + 5) + "<\/td>";
						strVar += "															<td>" + item.emotion
								+ "<\/td>";
						strVar += "															<td>";
						strVar += "																<div>";
						strVar += "																	<div>";
						strVar += "<img src=\"" + item.image_old
								+ "\" style=\" height: 90px;\">";
						strVar += "																	<\/div>";
						strVar += "																	<div>";
						if (item.datetime_old != null) {
							strVar += convertTime(item.datetime_old);
						}
						;
						strVar += "	<\/div><\/div>";
						strVar += "															<\/td>";
						strVar += "<td>" + item.similarity + "<\/td>";
						strVar += "															<td class=\"row\" style=\"width: 8%;\">";
						strVar += "<button onclick=\"undoCustomer("
								+ item.id
								+ ")\" class=\"btn btn-block btn-success btn-lg\" style=\"font-size: 100%;\">Khôi phục<\/button>";
						strVar += "<\/td>";
						strVar += "														<\/tr>";
					});
	$(strId).append(strVar);
}

function paginationShow(data,size) {
	$('.showlistTrash').pagination({
		dataSource : data,
		totalNumber : data.length,
		pageSize : size,
		ajax : {
			beforeSend : function() {
				container.prev().html('Loading data from flickr.com ...');
			}
		},
		callback : function(data, pagination) {
			showHtmlList(data, '#tableTrash > tbody');
		}
	});
}

function convertTime(date) {
	var d = new Date(date)
	return ("0" + d.getDate()).slice(-2) + "-"
			+ ("0" + (d.getMonth() + 1)).slice(-2) + "-" + d.getFullYear()
			+ " " + ("0" + d.getHours()).slice(-2) + ":"
			+ ("0" + d.getMinutes()).slice(-2) + ":"
			+ ("0" + d.getSeconds()).slice(-2);
};

function convertDate(date) {
	return new Date(date);
};

function minusTwoDate(date1, date2) {
	var diff = Math.abs(date1 - date2);
	var t = parseInt(diff / 1000);
	var h = parseInt(t / 3600);
	var m = parseInt((t - h * 3600) / 60);
	var s = t - h * 3600 - m * 60;
	return ("0" + h).slice(-2) + " giờ " + ("0" + m).slice(-2) + " phút "
			+ ("0" + s).slice(-2) + "giây trước";
};

/*
 * Thông báo kết quả
 * 
 */
function messageNotify(mess, typeS) {
	$
			.notify(
					{
						// options
						icon : '',
						title : '',
						message : '' + mess + '',
						url : '',
						target : ''
					},
					{
						// settings
						element : 'body',
						position : null,
						type : "" + typeS + "",
						allow_dismiss : true,
						newest_on_top : false,
						showProgressbar : false,
						placement : {
							from : "top",
							align : "right"
						},
						offset : 20,
						spacing : 10,
						z_index : 1031,
						delay : 2000,
						timer : 1000,
						url_target : '_blank',
						mouse_over : null,
						animate : {
							enter : 'animated fadeInDown',
							exit : 'animated fadeOutUp'
						},
						onShow : null,
						onShown : null,
						onClose : null,
						onClosed : null,
						icon_type : 'class',
						template : '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">'
								+ '<span data-notify="title">{1}</span> '
								+ '<span data-notify="message">{2}</span>'
								+ '<div class="progress" data-notify="progressbar">'
								+ '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>'
								+ '</div>'
								+ '<a href="{3}" target="{4}" data-notify="url"></a>'
								+ '</div>'
					});
};
